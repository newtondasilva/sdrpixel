#!/usr/bin/env bash

cd /site/sdrpixel
sudo git pull origin php8
docker exec -it drpixel.apache /var/www/html/sdrpixel/bin/console doctrine:schema:update --force
docker exec -it drpixel.apache /var/www/html/sdrpixel/bin/console cache:clear --env=prod
docker exec -it drpixel.apache /var/www/html/sdrpixel/bin/console assets:install web
#docker-compose -f /site/docker-drpixel/docker-compose.yml exec -T web php /var/www/html/sdrpixel/bin/console doctrine:schema:update
#docker-compose -f /site/docker-drpixel/docker-compose.yml exec -T web php /var/www/html/sdrpixel/bin/console cache:clear --env=prod
#docker-compose -f /site/docker-drpixel/docker-compose.yml exec -T web php /var/www/html/sdrpixel/bin/console assets:install web
sudo cp -r /site/fosckeditor /site/sdrpixel/web/bundles
#docker-compose -f /site/docker-symfony/docker-compose.yml exec -T apache /var/www/html/sdrpixel yarn encore production
#docker-compose -f /site/docker-drpixel/docker-compose.yml exec -T drpixel.apache chown newton:newton -R /var/www/html/sdrpixel
#docker-compose -f /site/docker-symfony/docker-compose.yml exec -T apache /var/www/html/adrpixel ng build --prod

docker exec -it drpixel.apache /var/www/html/sdrpixel/bin/console ckeditor:install --tag=4.22.x /site/sdrpixel/web/bundles/fosckeditor
sudo cp -rf /site/sdrpixel/assets/fmelfinder /site/sdrpixel/web/bundles/
sudo yarn encore production
sudo chown apache:apache -R *
