<?php

namespace ContentBundle\Service;

use ContentBundle\Entity\Content;
use ContentBundle\Entity\ContentTag;
use ContentBundle\Entity\Counter;
use ContentBundle\Entity\Tag;
use Doctrine\ORM\EntityManager;


class CounterService {

    /** @var  EntityManager */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Incrementa conteudo
     * @param Content $content
     */
    public function incrementContent(Content $content){
        $counter = $this->em->getRepository(Counter::class)->findOneBy(['content' => $content->getId()]);

        if(!$counter){
            /** @var \ContentBundle\Entity\Counter $counter */
            $counter = new Counter();
            $counter->setContent($content);
        }

        /** @var ContentTag $contentTag */
        foreach ($content->getContentTags() as $contentTag) {
            if($contentTag->getTag()){
                $this->incrementTag($contentTag->getTag());
            }

        }

        $this->increment($counter);
        $this->em->persist($counter);
        $this->em->flush();
    }

    /**
     * @param Counter $counter
     */
    private function increment(Counter &$counter){
        $today = new \DateTime();


        $diffYear = $counter->getLastYear()->diff($today);
        $diffMonth = $counter->getLastMonth()->diff($today);
        $diffWeek = $counter->getLastWeek()->diff($today);

        //Compare year
        if($diffYear->y > 0 and $diffYear->invert == 0){
            $counter->setLastYear($today);
            $counter->setYearCounter(1);
        } else {
            $counter->setYearCounter($counter->getYearCounter() + 1);
        }


        //Compare month
        if($diffMonth->m > 0 and $diffMonth->invert == 0){
            $counter->setLastMonth($today);
            $counter->setMonthCounter(1);
        } else {
            $counter->setMonthCounter($counter->getMonthCounter() + 1);
        }

        //Compare week
        if($diffWeek->d > 7 and $diffWeek->invert == 0){
            $counter->setLastWeek($today);
            $counter->setWeekCounter(1);
        } else {
            $counter->setWeekCounter($counter->getWeekCounter() + 1);
        }

        $counter->setTotalCounter($counter->getTotalCounter() + 1);
    }

    private function incrementTag(Tag $tag){
        $counterTag = $this->em->getRepository(Counter::class)->findOneBy(['tag' => $tag->getId()]);

        if(!$counterTag) {
            $counterTag = new Counter();
            $counterTag->setTag($tag);
        }
        $this->increment($counterTag);
        $this->em->persist($counterTag);

    }
}
