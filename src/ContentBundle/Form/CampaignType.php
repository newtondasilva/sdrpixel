<?php

namespace ContentBundle\Form;

use ContentBundle\Entity\Content;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CampaignType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Título',
            ])
            ->add('redirect', TextType::class, [
                'label' => 'Redirecionamento',
            ])

            ->add('published',CheckboxType::class,[
            'label' => 'Publicado?',
            'required' => false,
            ])
            ->add('slideshow',VichFileType::class,[
                'label' => 'Slideshow (450px de largura por 220px de altura)',
                'required' => false,
            ])
            ->add('lateral',VichFileType::class,[
                'label' => 'Lateral (200px largura)',
                'required' => false,
            ])
            ->add('topo',VichFileType::class,[
                'label' => 'Banner topo (728px largura por 90px altura)',
                'required' => false,
            ])
            ->add('modal',VichFileType::class,[
                'label' => 'Banner Modal (tamanho livre)',
                'required' => false,
            ])
            ;

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContentBundle\Entity\Campaign'
        ));
    }


}
