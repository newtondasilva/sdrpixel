<?php

namespace ContentBundle\Form;

use ContentBundle\Entity\Course;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CourseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class, [
            'label' => 'Título',
            ])

            ->add('published',CheckboxType::class,[
                'required' =>false,'label' => 'Publicado?'])
            ->add('termo',CKEditorType::class,[
                'label' => 'Termo (termo que o usuário deve aceitar ao iniciar o curso)',
                'required' => false,
            ])
            ->add('summary',CKEditorType::class,[
                'label' => 'Chamada do curso (exibido na home)',
            ])
            ->add('resumo',CKEditorType::class,[
                'label' => 'Resumo do curso (exibido na página do curso)',
            ])
            ->add('certificado',CKEditorType::class,[
                'label' => 'Texto do certificado (texto que irá gerar o certificado ao usuário)',
                'required' => true,
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Imagem de chamada',
                'required' => false,
            ])
            ->add('visibility',ChoiceType::class,[
                'label' => 'Visibilidade',
                'choices' => Course::getVisibilityChoices(),
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContentBundle\Entity\Course'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'contentbundle_course';
    }


}
