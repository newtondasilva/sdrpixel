<?php

namespace ContentBundle\Form;

use ContentBundle\Entity\Author;
use ContentBundle\Entity\Content;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ContentAuthorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', EntityType::class,[
                'class' => Author::class,
                'choice_label' => 'name'
            ]);
//            ->add('author', Select2EntityType::class,[
//                'label' => 'Autor',
//                'class' => Author::class,
//                'class' => 'ContentBundle\Entity\Author',
//                'remote_route' => 'ajax_author',
//                'primary_key' => 'id',
//                'text_property' => 'name',
//                'minimum_input_length' => 2,
//                'page_limit' => 20,
//                'allow_clear' => true,
//                'required' => true,
//                'delay' => 250,
//                'cache' => true,
//                'cache_timeout' => 60000, // if 'cache' is true
//                'language' => 'pt-br',
//                'placeholder' => 'Informe o autor',
//            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContentBundle\Entity\ContentAuthor',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ContentAuthorType';
    }


}
