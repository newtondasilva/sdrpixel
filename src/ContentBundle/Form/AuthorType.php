<?php

namespace ContentBundle\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class AuthorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',TextType::class, [
            'label' => 'Nome'
        ])
            ->add('imageFile', VichFileType::class, [
                'label' => 'Foto de perfil',
                'required' => false,
            ])
            ->add('curriculum', CKEditorType::class, [
                'label' => 'Curriculum',
            ])
            ->add('citationName',TextType::class, [
                'label' => 'Nome para citações (SILVA, João.)'
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContentBundle\Entity\Author'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'contentbundle_author';
    }


}
