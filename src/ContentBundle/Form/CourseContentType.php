<?php

namespace ContentBundle\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class CourseContentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('delta', NumberType::class,['label' => 'Ordem'])
//            ->add('chamada', CKEditorType::class,[
//                'label' => 'Texto de chamada',
//                'required' => false,
//            ])
            ->add('title',TextType::class,['label' => 'Título'])
            ->add('quiz',Select2EntityType::class,[
                'label' => 'Teste associado (informe o título do teste)',
                'class' => 'QuizBundle\Entity\Quiz',
                'remote_route' => 'ajax_quiz',
                'primary_key' => 'id',
                'text_property' => 'title',
                'minimum_input_length' => 2,
                'page_limit' => 20,
                'allow_clear' => true,
                'required' => false,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'pt-br',
                'placeholder' => 'Informe o teste',
            ])
            ->add('content',Select2EntityType::class,[
                'label' => 'Conteúdo associado (informe o título do conteúdo)',
                'class' => 'ContentBundle\Entity\Content',
                'remote_route' => 'ajax_content',
                'primary_key' => 'id',
                'text_property' => 'titulo',
                'minimum_input_length' => 2,
                'page_limit' => 20,
                'allow_clear' => true,
                'required' => false,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'pt-br',
                'placeholder' => 'Informe o conteúdo',
            ])
            ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContentBundle\Entity\CourseContent'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'contentbundle_coursecontent';
    }


}
