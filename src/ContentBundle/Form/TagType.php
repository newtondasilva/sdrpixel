<?php

namespace ContentBundle\Form;

use ContentBundle\ContentBundle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TagType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('children',EntityType::class,[
                'label' => 'Tag Pai',
                'class' => \ContentBundle\Entity\Tag::class,
                'choice_label' => 'name',
                'required' => false,
                'choices' => $options['tags'],
            ])
            ->add('path', TextType::class,[
                'label' => 'Caminho',
                'required' => false,
            ])
            ->add('tagType',EntityType::class,[
                'label' => 'Tipo',
                'class' => \ContentBundle\Entity\TagType::class,
                'choice_label' => 'name',
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContentBundle\Entity\Tag',
            'tags' => array(),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'contentbundle_tag';
    }


}
