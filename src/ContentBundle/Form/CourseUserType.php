<?php

namespace ContentBundle\Form;


use ContentBundle\Entity\CourseUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class CourseUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('type', ChoiceType::class,[
                'label' => 'Tipo de usuário',
                'choices' => CourseUser::getTypeChoices(),
                'expanded' => true,
                'disabled' => false,
                'multiple' => false,
            ])
            ->add('status', ChoiceType::class,[
                'label' => 'Andamento',
                'choices' => CourseUser::getStatusChoices(),
                'expanded' => true,
                'required' => false,
            ])
            ->add('imageFile', VichFileType::class, [
                'label' => 'Certificado',
                'allow_delete' => true,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CourseUser::class,
        ));
    }
}