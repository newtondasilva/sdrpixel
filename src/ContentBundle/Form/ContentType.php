<?php

namespace ContentBundle\Form;

use ContentBundle\Entity\Content;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ContentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('path', TextType::class, [
                'label' => 'Endereço (deixe em branco para preenchimento automático)',
                'required' => false,
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Tipo',
                'choices' => Content::getTypeChoices()
            ])
            ->add('published', CheckboxType::class,[
                'label' => 'Publicado? Se publicado conteúdo será listado no site, caso contrário ficará oculto',
                'required' => false,
            ])
            ->add('body', CKEditorType::class, [
                'label' => 'Corpo',
                'config_name' => 'content',
            ])
            ->add('contentAuthors',CollectionType::class,[
                'label' => 'Autores',
                'entry_type'   => ContentAuthorType::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'attr' => [
                    'class' => 'collection-author'
                ],
            ])
            ->add('urlVideo',TextType::class,[
                'label' => 'Url do vídeo no youtube. Ex: https://www.youtube.com/embed/hzp_snTNwc0',
                'required' => false,
            ])
            ->add('video', VichFileType::class, [
                'label' => 'Vídeo em MP4 para o e-drpixel',
                'delete_label' => 'Excluir arquivo',
                'required' => false,
            ])
            ->add('file', VichFileType::class, [
                'label' => 'Imagem de chamada',
                'delete_label' => 'Excluir arquivo',
                'required' => false,
            ])
            ;

        /** @var \ContentBundle\Entity\TagType $tagType */
        foreach ($options['tagTypes'] as $tagType){
            $builder->add('tagType' . $tagType->getId(), Select2EntityType::class, [
                'label' => $tagType->getName(),
                'multiple' => true,
                'mapped' => false,
                'data' => isset($options['tags'][$tagType->getId()]) ? $options['tags'][$tagType->getId()] : null,
                'class' => 'ContentBundle\Entity\Tag',
                'remote_route' => 'ajax_tag',
                'remote_params' => ['tagType' => $tagType->getId()],
                'primary_key' => 'id',
                'text_property' => 'name',
                'minimum_input_length' => 2,
                'page_limit' => 20,
                'allow_clear' => true,
                'required' => false,
                'delay' => 250,
                'cache' => true,
                'allow_add' => [
                    'enabled' => true,
                    'tag_separators' => '/',
                    'new_tag_prefix' => '__',
                    'new_tag_text' => ' (incluir)',
                ],
                'attr' => [
                    'class' => 'collection-tagType' . $tagType->getId()
                ],
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'pt-br',
                'placeholder' => 'Informe as tags',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContentBundle\Entity\Content',
            'tags' => null,
            'tagTypes' => array(),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'quizbundle_content';
    }


}
