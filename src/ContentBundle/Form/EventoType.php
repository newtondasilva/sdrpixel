<?php

namespace ContentBundle\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class EventoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Título',
            ])
            ->add('local')
            ->add('published',CheckboxType::class, [
                'label' => 'Publicado?',
                'required' => false,
            ])
            ->add('file', VichFileType::class, [
                'label' => 'Imagem chamada',
                'required' => false,
            ])

            ->add('dataInicio')
            ->add('dataFim')
            ->add('body',CKEditorType::class,[
                'label' => 'Corpo',
            ])


        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ContentBundle\Entity\Evento'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'contentbundle_evento';
    }


}
