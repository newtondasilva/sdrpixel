<?php

namespace ContentBundle\Command;

use ContentBundle\Entity\Author;
use ContentBundle\Entity\Content;
use ContentBundle\Entity\ContentAuthor;
use ContentBundle\Entity\ContentTag;
use ContentBundle\Entity\Tag;
use ContentBundle\Entity\TagType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ContentFixtagCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('content:fixtag')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $contents = $em->getRepository(Content::class)->findAll();

        /** @var Content $content */
        foreach ($contents as $content){
            $content->setTitulo(ucfirst(mb_strtolower($content->getTitulo())));
            $em->persist($content);
        }


        $em->flush();

        $output->writeln('Command result.');
    }

}
