<?php

namespace ContentBundle\Command;

use ContentBundle\Entity\Content;
use ContentBundle\Entity\ContentTag;
use Doctrine\DBAL\Driver\Mysqli\MysqliConnection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class ContentImportCommand extends ContainerAwareCommand
{
    /** @var  MysqliConnection */
    public $conn;

    /** @var  EntityManager */
    public $em;

    protected function configure()
    {
        $this
            ->setName('content:import')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->conn = mysqli_connect("localhost", "newton", "newton", "fcmDrPixel");

        $this->em = $this->getContainer()->get('doctrine')->getManager();

        //$this->importBancoImagem();
        $this->fixPath();
        $output->writeln('Command result.');
    }

    public function fixPath(){
        $contents = $this->em->getRepository(Content::class)->findAll();

        /** @var Content $content */
        foreach ($contents as $content){
            $content->setPath($content->getPath());
            $this->em->persist($content);
        }

        $this->em->flush();
    }

    public function importBancoImagem(){
        $query = $this->conn->query("Select * from node a where type IN ('banco_de_imagem','discussao_casos','metodos_de_imagem')");

        while($row = $query->fetch_assoc()) {
            $created = new \DateTime();
            $created->setTimestamp($row['created']);

            $content = new Content();

            $imagemQuery = $this->conn->query('select b.uri from fcmDrPixel.field_data_field_imagem a
join fcmDrPixel.file_managed b on a.field_imagem_fid = b.fid
where a.entity_id = ' . $row['nid']);

            while($fileImagem = $imagemQuery->fetch_assoc()) {
                $content->setImagem($fileImagem['uri']);
            }

            $bodyQuery = $this->conn->query('select body_value from fcmDrPixel.field_data_body where entity_id = ' . $row['nid']);
            while($body = $bodyQuery->fetch_assoc()) {
                $content->setBody($body['body_value']);
            }

            $qPath = $this->conn->query("select alias from fcmDrPixel.url_alias a where a.source = 'node/".$row['nid']."'");
            while($path = $qPath->fetch_assoc()) {
                $content->setPath($path['alias']);
            }

            $qvideo = $this->conn->query("select field_video_chamada_video_url from fcmDrPixel.field_data_field_video_chamada where entity_id = ".$row['nid']);
            while($video = $qvideo->fetch_assoc()) {
                $content->setUrlVideo($video['field_video_chamada_video_url']);
            }

            $content->setCreated($created);
            $content->setPublished(true);
            $content->setType($row['type']);
            $content->setLanguage($row['language']);
            $content->setTitulo($row['title']);
            $content->setNid($row['nid']);

            $this->saveTag(1,$row['nid'], $content);
            $this->saveTag(2,$row['nid'], $content);
            $this->saveTag(3,$row['nid'], $content);

            $this->em->persist($content);

        }

        $this->em->flush();
    }

    /**
     * types
     * 1 - tags
     * 3 - especialidades
     * 4 - tipos de imagem
     * 5 - autor
     * @param $tipo
     * @param $nid
     */
    public function saveTag($type, $nid,Content &$content){
        switch ($type){
            case 1: $table = 'field_data_field_palavras_chave'; $field = 'field_palavras_chave_tid'; break;
            case 2: $table = 'field_data_field_especialidade'; $field = 'field_especialidade_tid'; break;
            case 3: $table = 'field_data_field_tipo_de_imagem'; $field = 'field_tipo_de_imagem_tid'; break;
        }


        $query = $this->conn->query('select b.name from fcmDrPixel.'.$table.' a
join fcmDrPixel.taxonomy_term_data b on a.'.$field.' = b.tid
where a.entity_id = ' . $nid);

        while($row = $query->fetch_assoc()){
            $tags = $this->em->getRepository(Tag::class)->findBy(['name' => $row['name']]);

            foreach ($tags as $tag){
                $contentTag = new ContentTag();
                $contentTag->setContent($content);
                $contentTag->setTag($tag);
                $content->addContentTag($contentTag);
            }
        }

    }
}
