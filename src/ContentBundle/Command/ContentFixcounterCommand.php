<?php

namespace ContentBundle\Command;

use ContentBundle\Entity\Content;
use ContentBundle\Entity\ContentTag;
use ContentBundle\Entity\Counter;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ContentFixcounterCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('content:fixcounter')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $contents = $em->getRepository(Content::class)->findAll();

        $conn = mysqli_connect("localhost", "newton", "newton", "fcmDrPixel");

        /** @var Content $content */
        foreach ($contents as $content){
            $nid = $content->getNid();
            $query = $conn->query("Select * from node_counter a where nid = " . $nid);

            while($row = $query->fetch_assoc()) {

                $counter = new Counter();
                $counter->setContent($content);
                $counter->setYearCounter($row['yearcount']);
                $counter->setMonthCounter($row['monthcount']);
                $counter->setWeekCounter($row['weekcount']);
                $counter->setTotalCounter($row['totalcount']);

                $em->persist($counter);
                /** @var ContentTag $contentTag */
                foreach ($content->getContentTags() as $contentTag){

                    $counterT = new Counter();
                    $counterT->setTag($contentTag->getTag());
                    $counterT->setYearCounter($row['yearcount']);
                    $counterT->setMonthCounter($row['monthcount']);
                    $counterT->setWeekCounter($row['weekcount']);
                    $counterT->setTotalCounter($row['totalcount']);
                    $em->persist($counterT);
                }
            }
        }


        $em->flush();
        $output->writeln('Command result.');
    }

}
