<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Course;
use ContentBundle\Entity\CourseContent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Coursecontent controller.
 *
 * @Route("admin/course/{course}/content")
 */
class CourseContentController extends Controller
{
    /**
     * Lists all courseContent entities.
     *
     * @Route("/", name="admin_course_content_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $courseContents = $em->getRepository('ContentBundle:CourseContent')->findAll();

        return $this->render('coursecontent/index.html.twig', array(
            'courseContents' => $courseContents,
        ));
    }

    /**
     * Creates a new courseContent entity.
     *
     * @Route("/new", name="admin_course_content_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Course $course)
    {
        $em = $this->getDoctrine()->getManager();
        $courseContent = new Coursecontent();

        if($courseSectionId = $request->get('courseSection')){
            $courseSection = $em->getRepository('ContentBundle:CourseSection')->find($courseSectionId);
            $courseContent->setCourseSection($courseSection);
        }

        if($contentId = $request->get('content')){
            $content = $this->getDoctrine()->getRepository(Content::class)->find($contentId);
            $courseContent->setContent($content);
        }

        $courseContent->setDelta($course->getCourseContents()->count() + 1);

        $courseContent->setCourse($course);
        $form = $this->createForm('ContentBundle\Form\CourseContentType', $courseContent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($courseContent);
            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_course_show', array(
                'id' => $course->getId(),
                'tab' => 1,
            ));
        }

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('admin_course_show', array('id' => $course->getId()));
        }

        return $this->render('coursecontent/edit.html.twig', array(
            'page_title' => 'Incluir aula em: ' . $courseSection->getNome(),
            'back' => $back,
            'courseContent' => $courseContent,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing courseContent entity.
     *
     * @Route("/{id}/edit", name="admin_course_content_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CourseContent $courseContent, Course $course)
    {

        $editForm = $this->createForm('ContentBundle\Form\CourseContentType', $courseContent);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('course_show', array('id' => $course->getId()));
        }

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('admin_course_show',['id' => $course->getId()]);
        }

        return $this->render('coursecontent/edit.html.twig', array(
            'page_title' => 'Editar aula: ' . $courseContent->getNome(),
            'courseContent' => $courseContent,
            'actions' => [
                'delete' => [
                    'label' => 'Excluir',
                    'path' => $this->generateUrl('admin_course_content_delete',[
                        'id' => $courseContent->getId(),
                        'course' => $courseContent->getCourseSection()->getCourse()->getId()])
                ],
            ],
            'form' => $editForm->createView(),
            'back' => $back,

        ));
    }

    /**
     * Deletes a courseContent entity.
     *
     * @Route("/{id}/delete", name="admin_course_content_delete")
     * @Method({"GET","POST"})
     */
    public function deleteAction(Request $request, CourseContent $courseContent, Course $course)
    {
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($courseContent);
            $em->flush();

            $this->addFlash('success', 'Exclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_course_show',['id' => $course->getId()]);
        }

        return $this->render(':default:delete.html.twig',[
            'form' => $form->createView(),
            'page_title' => 'Confirmar exclusão do módulo "'.
                $courseContent->getNome() . '" no curso ' . $course->getTitle(),
            'back' => $this->generateUrl('admin_course_show',['id' => $course->getId()]),
        ]);

    }


}
