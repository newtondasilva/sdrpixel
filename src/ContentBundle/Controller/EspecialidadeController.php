<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Caso;
use ContentBundle\Entity\Content;
use ContentBundle\Entity\TagType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Caso controller.
 *
 * @Route("")
 */
class EspecialidadeController extends Controller
{

    /**
     * Finds and displays a caso entity.
     *
     * @Route("/especialidade", name="especialidade_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('home');
        }

        $em = $this->getDoctrine()->getManager();

        $especialidades = $em->getRepository(Tag::class)->createQueryBuilder('a')
            ->join('a.tagType','b')
            ->andWhere('b.name = :name')->setParameter('name','Especialidades')
            ->andwhere('a.children is null')
            ->orderBy('a.name')->getQuery()->getResult();

        return $this->render('Content/Especialidade/index.html.twig', array(
            'page_title' => 'Especialidades',
            'actions' => [
                'new' => [
                    'label' => 'Incluir especialidade',
                    'path' => $this->generateUrl('admin_tag_new',['type' => 3])
                ]
            ],
            'tags' => $especialidades,
            'back' => $back,
        ));
    }

    /**
     * Finds and displays a caso entity.
     *
     * @Route("/especialidade/{path}", name="especialidade_show")
     * @Method("GET")
     */
    public function showAction(Request $request, $path)
    {

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('home');
        }

        $em = $this->getDoctrine()->getManager();

        $tag = $em->getRepository(Tag::class)->findOneBy(['path' => $path]);

        if(!$tag){
            return $this->redirectToRoute('busca',['q' => $path]);
        }

        return $this->render('Content/Tag/show.html.twig', array(
            'page_title' => $tag->getName(),
            'tag' => $tag,
            'actions' => [
                'edit' => [
                    'label' => 'Editar especialidade',
                    'path' => $this->generateUrl('admin_tag_edit',['id' => $tag->getId()])
                ],
            ],
            'back' => $back,
        ));
    }
}
