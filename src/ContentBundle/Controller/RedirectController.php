<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Content;
use ContentBundle\Entity\ContentTag;
use ContentBundle\Entity\Redirect;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Redirect controller.
 *
 * @Route("admin/redirect")
 */
class RedirectController extends Controller
{
    /**
     * Lists all redirect entities.
     *
     * @Route("/", name="admin_redirect_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $redirects = $em->getRepository(Redirect::class)->findAll();



        return $this->render('redirect/index.html.twig', array(
            'page_title' => 'Redirecionamentos',
            'actions' => [
                1 => [
                    'label' => 'Gerar novamente',
                    'path' => $this->generateUrl('admin_redirect_generate'),
                ],
                0 => [
                    'label' => 'Incluir',
                    'path' => $this->generateUrl('admin_redirect_new')
                ]
            ],
            'redirects' => $redirects,
        ));
    }

    /**
     * Creates a new redirect entity.
     *
     * @Route("/new", name="admin_redirect_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        return $this->editAction($request, new Redirect());
    }

    /**
     * Deletes a redirect entity.
     *
     * @Route("/generate", name="admin_redirect_generate")

     */
    public function generateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $contents = $em->getRepository(Content::class)->findAll();

        /**
         * MODELOS DE RECIRECIONAMENTO
         * /{tag}/{content}
         * /conteudo/{content}
         * /{tipo}/{content}
         */


        foreach ($contents as $content){

            // {tag}/{content}
            /** @var ContentTag $contentTag */
            foreach ($content->getContentTags() as $contentTag){
                $url = '/' . $contentTag->getTag()->getPath().'/'. $content->getPath();
                if(!$redirect = $this->getRedirectByUrl($url)){
                    $redirect = new Redirect();
                    $redirect->setContent($content);
                    $redirect->setTarget($url);
                    $em->persist($redirect);
                }
            }

            // tipo/{content}
            $url = '/' . Content::getTypeSlug($content->getType()) . '/' . $content->getPath();
            if(!$redirect = $this->getRedirectByUrl($url)){
                $redirect = new Redirect();
                $redirect->setContent($content);
                $redirect->setTarget($url);
                $em->persist($redirect);
            }


        }

        $em->flush();
        return $this->redirectToRoute('admin_redirect_index');
    }

    /**
     * Finds and displays a redirect entity.
     *
     * @Route("/{id}", name="admin_redirect_show")
     * @Method("GET")
     */
    public function showAction(Redirect $redirect)
    {
        $deleteForm = $this->createDeleteForm($redirect);

        return $this->render('redirect/show.html.twig', array(
            'redirect' => $redirect,
            'back' => $this->generateUrl('admin_redirect_index'),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing redirect entity.
     *
     * @Route("/{id}/edit", name="admin_redirect_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Redirect $redirect)
    {
        $editForm = $this->createForm('ContentBundle\Form\RedirectType', $redirect);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_redirect_index', array('id' => $redirect->getId()));
        }

        return $this->render('redirect/edit.html.twig', array(
            'page_title' => 'Editar redirecionamento',
            'redirect' => $redirect,
            'form' => $editForm->createView(),
        ));
    }



    protected function getRedirectByUrl($url){
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository(Redirect::class)->findOneBy(['target' => $url]);
    }

    /**
     * Deletes a redirect entity.
     *
     * @Route("/{id}", name="admin_redirect_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Redirect $redirect)
    {
        $form = $this->createDeleteForm($redirect);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($redirect);
            $em->flush();
        }

        return $this->redirectToRoute('admin_redirect_index');
    }

    /**
     * Creates a form to delete a redirect entity.
     *
     * @param Redirect $redirect The redirect entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Redirect $redirect)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_redirect_delete', array('id' => $redirect->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Lists all redirect entities.
     *
     * @Route("/{arg1}/{arg2}", name="match2args")
     */
    public function match2Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $redirect  = $em->getRepository(Redirect::class)->findOneBy(['target' => $request->getPathInfo()]);

        if($redirect){
            return $this->redirectToRoute('content_show',['id' => $redirect->getContent()->getId()]);
        }
    }
}
