<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Content;
use ContentBundle\Entity\Slideshow;
use ContentBundle\Form\ContentSlideshowType;
use ContentBundle\Form\ContentType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Slideshow controller.
 *
 * @Route("")
 */
class SlideshowController extends Controller
{
    /**
     * Lists all slideshow entities.
     *
     * @Route("/content/{content}/slideshow/ajax", name="content_slideshow_ajax", options={"expose"=true})
     * @Method({"GET","POST"})
     */
    public function ajaxAction(Request $request, Content $content)
    {
        $content = $this->render('Content/Slideshow/ajax.html.twig',[
            'content' => $content,
        ]);
        return new JsonResponse($content->getContent());
    }


    /**
     * Lists all slideshow entities.
     *
     * @Route("/admin/content/{content}/slideshow", name="admin_content_slideshow_index")
     * @Method({"GET","POST"})
     */
    public function indexAction(Request $request, Content $content)
    {
        $em = $this->getDoctrine()->getManager();

        $orig = new ArrayCollection();

        /** @var Slideshow $slideshow */
        foreach ($content->getSlideshows() as $slideshow){
            $orig->add($slideshow);
        }

        $form = $this->createForm(ContentSlideshowType::class, $content);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            /** @var Slideshow $slideshow */
            foreach ($content->getSlideshows() as $slideshow){
                if($slideshow->getFile()){
                    $slideshow->setUpdatedAt(new \DateTime());
                    $slideshow->setContent($content);
                    $em->persist($slideshow);
                }
            }

            $em->flush();

            $this->addFlash('success','Alteração efetuada com sucesso');

        }

        return $this->render('ContentBundle:Slideshow:index.html.twig', array(
            'page_title' => 'Editar ' . $content->getTitulo() . ' - Slideshow',
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_content_edit', ['id' => $content->getId()])
        ));
    }




}