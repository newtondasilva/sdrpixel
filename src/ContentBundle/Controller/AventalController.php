<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Avental;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Avental controller.
 *
 * @Route("admin/avental")
 */
class AventalController extends Controller
{

    /**
     * Lists all Avental entities.
     *
     * @Route("/", name="admin_avental_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aventals = $em->getRepository(Avental::class)
            ->findBy(array(),['name' => 'ASC']);

        return $this->render('avental/index.html.twig', array(
            'page_title' => 'Aventais',
            'actions' => [
                'new' => [
                    'label' => 'Incluir',
                    'path' => $this->generateUrl('admin_avental_new'),
                    'role' => 'ROLE_ADMIN',
                ],
            ],
            'aventals' => $aventals,
        ));
    }

    /**
     * Creates a new Avental entity.
     *
     * @Route("/new", name="admin_avental_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request)
    {
        $avental = new Avental();
        return $this->editAction($request, $avental);
    }

    /**
     * Finds and displays a Avental entity.
     *
     * @Route("/{id}", name="avental_show")
     * @Method("GET")
     */
    public function showAction(Avental $avental)
    {
        return $this->render('@Content/Avental/show.html.twig', array(
            'avental' => $avental
        ));
    }

    /**
     * Displays a form to edit an existing Avental entity.
     *
     * @Route("/{id}/edit", name="admin_avental_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Avental $avental)
    {
        $editForm = $this->createForm('ContentBundle\Form\AventalType', $avental);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $avental->setCreated(new \DateTime());

            $em->persist($avental);
            $em->flush();


            $this->addFlash('success','Alteração efetuada com sucesso');
            return $this->redirectToRoute('admin_avental_index', array('id' => $avental->getId()));
        }

        if($avental->getId()){
            $actions = [
                'new' => [
                    'label' => 'Excluir',
                    'path' => $this->generateUrl('admin_avental_delete',['id' => $avental->getId()]),
                    'role' => 'ROLE_ADMIN',
                ],
            ];
        } else $actions = array();


        return $this->render('avental/edit.html.twig', array(
            'page_title' => 'Editar avental',
            'back' => $this->generateUrl('admin_avental_index'),
            'avental' => $avental,
            'actions' => $actions,
            'form' => $editForm->createView(),

        ));
    }

    /**
     * Deletes a Avental entity.
     *
     * @Route("/{id}/delete", name="admin_avental_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Avental $avental)
    {
        $form = $this->createFormBuilder($avental)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($avental);
            $em->flush();
            $this->addFlash('success','Exclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_avental_index');
        }

        return $this->render(':default:delete.html.twig', array(
            'page_title' => 'Excluir autor ' . $avental->getName(),
            'back' => $this->generateUrl('admin_avental_index'),
            'form' => $form->createView(),

        ));
    }
}
