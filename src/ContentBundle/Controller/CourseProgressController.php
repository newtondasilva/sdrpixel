<?php

namespace ContentBundle\Controller;

use AppBundle\Entity\User;
use ContentBundle\Entity\Course;
use ContentBundle\Entity\CourseContent;
use ContentBundle\Entity\CourseProgress;
use Doctrine\Common\Collections\ArrayCollection;
use QuizBundle\Entity\Answer;
use QuizBundle\Entity\AnswerItem;
use QuizBundle\Entity\Question;
use QuizBundle\Entity\QuestionItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Courseprogress controller.
 *
 * @Route("course/{course}/progress")
 */
class CourseProgressController extends Controller
{
    /**
     * O progresso do curso eh criado quando o usuario se increve
     * Lists all courseProgress entities.
     * Inscreve usuário no curso
     *
     * @Route("/start", name="course_progress_start")
     * @Method({"GET","POST"})
     */
    public function startAction(Request $request, Course $course)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted()){
            foreach ($course->getCourseContents() as $courseContent){
                $courseProgresses = new CourseProgress();
                $courseProgresses->setCourseContent($courseContent);
                $courseProgresses->setCreated(new \DateTime());
                $courseProgresses->setCreatedBy($this->getUser());
                $courseProgresses->setStatus(CourseProgress::STATUS_PENDENTE);

                $em->persist($courseProgresses);

            }

            $em->flush();
            $this->addFlash('success','Inscrição efetuada com sucesso');
            return $this->redirectToRoute('course_show',['id' => $course->getId()]);
        }

        return $this->render('courseprogress/start.html.twig', array(
            'page_title' => 'Confirma inscrição para o curso: ' . $course->getTitle(),
            'back' => $this->generateUrl('course_show',['id' => $course->getId()]),
            'form' => $form->createView(),
            'course' => $course,
        ));
    }

    /**
     * Displays a form to edit an existing courseProgress entity.
     * Encontra próximo módulo que o usuario deve executar
     *
     * @Route("/next", name="course_progress_next")
     * @Method({"GET", "POST"})
     */
    public function nextAction(Request $request, Course $course)
    {
        $em = $this->getDoctrine()->getManager();

        $progresses = $em->getRepository('ContentBundle:CourseProgress')->createQueryBuilder('a')
            ->join('a.courseContent','b')
            ->andWhere('b.course = :course')->setParameter('course',$course->getId())
            ->andWhere('a.createdBy = :user')->setParameter('user',$this->getUser())
            ->orderBy('b.delta','ASC')
            ->getQuery()->getResult();

        if($progresses){
            $count = count($progresses);
            $nextProgress = $progresses[0];
            /** @var CourseProgress $previousProgress */
            $previousProgress = $progresses[0];
            $lastProgress = $progresses[$count-1];

            /**
             * @var  $key
             * @var CourseProgress $progress
             */
            foreach ($progresses as $key => $progress){
                // Verifica se o proximo nao foi iniciado ou esta pendente
                if($previousProgress->getStatus() == CourseProgress::STATUS_CONCLUIDO and
                    ($progress->getStatus() == CourseProgress::STATUS_INICIADO or $progress->getStatus() == CourseProgress::STATUS_PENDENTE)){
                    $nextProgress = $progress;
                }

                $previousProgress = $progress;
            }
        }

        return $this->redirectToRoute('course_progress_show',['course' => $course->getId(),'id' => $nextProgress->getId()]);
    }

    /**
     * Finds and displays a courseProgress entity.
     *
     * @Route("/{id}", name="course_progress_show")
     * @Method({"GET","POST"})
     */
    public function showAction(Request $request, CourseProgress $courseProgress)
    {
        $related = new ArrayCollection();

        if(!$content = $courseProgress->getCourseContent()->getContent()){
            return $this->redirectToRoute('course_progress_quiz',[
                'id' => $courseProgress->getId(),'course' => $courseProgress->getCourseContent()->getCourse()->getId()
            ]);
        }

        if($courseProgress->getStatus() == 'P'){
            $courseProgress->setStatus(CourseProgress::STATUS_INICIADO);
        }

        $tags = array();

        $em = $this->getDoctrine()->getManager();

        /** @var Tag $tag */
        foreach ($tags as $tag){
            /** @var ContentTag $contentTag */
            foreach ($tag->getContentTags() as $contentTag){
                if($contentTag->getContent()->getId() != $content->getId()){
                    $related->add($contentTag->getContent());
                }
            }
        }

        $rate = null;

        if($this->getUser()){
            $rate = $this->getDoctrine()->getRepository(Rate::class)
                ->findOneBy(['user' => $this->getUser()->getId(),'content' => $content->getId()]);
        }

        if($quiz = $content->getQuiz()){
            $question = $quiz->getQuestions()->first();
            $form = $this->createFormBuilder()->add('answer',ChoiceType::class,[
                'label' => 'Resposta',
                'choices' => $question->getItens(),
                'choice_label' => 'text',
                'choice_value' => 'id',
                'expanded' => true,

            ])->add('time',TextType::class)
                ->getForm();
        } else {
            $form = $this->createFormBuilder()->getForm();
        }

        $form->handleRequest($request);

        if($form->isSubmitted()){
            $courseProgress->setStatus(CourseProgress::STATUS_CONCLUIDO);
            $em->persist($courseProgress);
            $em->flush();
            $this->addFlash('success','Módulo concluído com sucesso');

            return $this->redirectToRoute('course_show',[
                'id' => $courseProgress->getCourseContent()->getCourse()->getId()
            ]);
        }

        return $this->render('@Content/Content/show.html.twig', array(
            'page_title' => $courseProgress->getCourseContent()->getCourse()->getTitle() .': '
                . $courseProgress->getCourseContent()->getTitle(),
            'courseProgress' => $courseProgress,
            'related' => $related,
            'form' => $form->createView(),
            'rate' => $rate,
            'actions' => [
                'edit' => [
                    'path' => $this->generateUrl('admin_content_edit', ['id' => $content->getId()]),
                    'label' => 'Editar',
                    'role' => 'ROLE_ADMIN'
                ],
            ],
            'back' => $this->generateUrl('course_show',['id' => $courseProgress->getCourseContent()->getCourse()->getId()]),
            'content' => $content,

        ));
    }

    /**
     * Lists all courseContent entities.
     *
     * @Route("/", name="admin_course_progress_index")
     */
    public function indexAction(Course $course)
    {
        $em = $this->getDoctrine();

        $users = array();

        $query = $em->getRepository(User::class)->createQueryBuilder('a')
        ->join('a.courseProgresses','cp')
        ->join('cp.courseContent','cc')
        ->join('cc.course','c')
        ->andWhere("c.id = :course")->setParameter('course',$course->getId())
        ->orderBy('a.name')
        ->getQuery()->getResult();



        /**
        * @var  $key
        * @var User $user
        */
        foreach ($query as $key => $user) {
            $users[$user->getId()]['user'] = $user;

            $progressed = $em->getRepository('ContentBundle:CourseProgress')->createQueryBuilder('a')
                ->join('a.courseContent','cc')
                ->join('cc.course','c')
                ->andWhere("c.id = :course")->setParameter('course',$course->getId())
                ->andWhere('a.createdBy = :user')->setParameter('user',$user->getId())
                ->getQuery()->getResult();

            /** @var CourseProgress $progress */
            foreach ($progressed as $progress){
                $users[$user->getId()]['progress'][$progress->getCourseContent()->getId()] = $progress;
                $userArr[$user->getId()] = $user;
            }
        }

        $byContent = array();

        /** @var CourseContent $courseContent */
        foreach ($course->getCourseContents() as $courseContent) {
            if(!$courseContent->getQuiz() or $courseContent->getQuiz()->getFeedback() == true) continue;

            $byContent[$courseContent->getId()]['courseContent'] = $courseContent;

            if ($courseContent->getQuiz()) {
                /** @var QuestionItem $question */
                foreach ($courseContent->getQuiz()->getQuestions() as $question) {
                    $byContent[$courseContent->getId()]['questions'][$question->getId()]['question']= $question;
                    foreach ($userArr as $item){
                        $byContent[$courseContent->getId()]['questions'][$question->getId()]['answerItem'][$item->getId()] = null;
                    }


                }
            }
        }


        /** @var CourseContent $courseContent */
        foreach ($course->getCourseContents() as $courseContent){
            if(!$courseContent->getQuiz() or $courseContent->getQuiz()->getFeedback() == true) continue;

            $byContent[$courseContent->getId()]['courseContent'] = $courseContent;

            if($courseContent->getQuiz()){
                /** @var Answer $answer */
                foreach ($courseContent->getQuiz()->getAnswers() as $answer){

                    /** @var AnswerItem $item */
                    foreach ($answer->getItems() as $item){

                    }

                }
            }

        }

      return $this->render('courseprogress/index.html.twig',[
        'page_title' => 'Inscritos',
        'course' => $course,
        'users' => $users,
        'byContent' => $byContent,
        'back' => $this->redirectToRoute('course_show',['id' => $course->getId()])
      ]);
    }

    /**
     * Finds and displays a courseProgress entity.
     * Exibe todas as perguntas no formatado de prova
     *
     * @Route("/{id}/quiz", name="course_progress_quiz")
     * @Method({"GET","POST"})
     */
    public function quizAction(Request $request, CourseProgress $courseProgress)
    {
        //Prepara redirecionamento para perguntas do Quiz
        $quiz = $courseProgress->getCourseContent()->getQuiz();

        $em = $this->getDoctrine()->getManager();

        // Altera o status para iniciado
        if($courseProgress->getStatus() == CourseProgress::STATUS_PENDENTE){
            $courseProgress->setStatus(CourseProgress::STATUS_INICIADO);
            $em->persist($courseProgress);
            $em->flush();
        }


        // Verifica se ja possui resposta
        $answer = $em->getRepository('QuizBundle:Answer')->createQueryBuilder('a')
            ->andWhere('a.courseProgress = :courseProgressId')
            ->setParameter('courseProgressId',$courseProgress->getId())
            ->andWhere('a.user = :user')->setParameter('user',$this->getUser()->getId())
            ->andWhere('a.quiz = :quiz')->setParameter('quiz',$quiz->getId())
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        if(!$answer){
            /** @var Answer $answer */
            $answer = new Answer();
            $answer->setCreated(new \DateTime());
            $answer->setUser($this->getUser());
            $answer->setQuiz($quiz);
            $answer->setCourseProgress($courseProgress);
            $this->getDoctrine()->getManager()->persist($answer);
            $this->getDoctrine()->getManager()->flush();
        }

        // Quiz ja respondido
        if($courseProgress->getStatus() == CourseProgress::STATUS_CONCLUIDO){
            // Exibe respostas somente se o quiz foi respondido em todas as etapas
            if($courseProgress->getCourseContent()->allQuizFromCourseCompleted($courseProgress)){
                return $this->redirectToRoute('quiz_answer_finish',['answer' => $answer->getId(),
                    'quiz' => $answer->getQuiz()->getId(),
                    'back' => $this->generateUrl('course_show',['id' => $courseProgress->getCourseContent()->getCourse()->getId()])]);
            } else {
                $this->addFlash('success','Respostas do teste será liberado somente o próximo teste for concluído');
                return $this->redirectToRoute('course_show',['id' => $courseProgress->getCourseContent()->getCourse()->getId()]);
            }

        }


        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $courseProgress->setStatus(CourseProgress::STATUS_CONCLUIDO);
            $em->persist($courseProgress);
            $em->flush();
            $this->addFlash('success','Teste realizado com sucesso');
            return $this->redirectToRoute('course_show',['id' => $courseProgress->getCourseContent()->getCourse()->getId()]);
        }



        return $this->render('Quiz/Quiz/show.full.html.twig',[
            'page_title' => $courseProgress->getCourseContent()->getCourse()->getTitle() .': '
            . $courseProgress->getCourseContent()->getTitle(),
            'quiz' => $quiz,
            'form' => $form->createView(),
            'back' => $this->generateUrl('course_show',['id' => $courseProgress->getCourseContent()->getCourse()->getId()]),
            'answer' => $answer,
            'courseProgress' => $courseProgress,
        ]);
    }




}
