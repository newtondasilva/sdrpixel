<?php

namespace ContentBundle\Controller;

use AppBundle\Entity\User;
use ContentBundle\Entity\Course;
use ContentBundle\Entity\CourseProgress;
use ContentBundle\Entity\CourseUser;
use Mpdf\Mpdf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Course controller.
 *
 * @Route("")
 */
class CourseController extends Controller
{

    /**
     * Lists all course entities.
     *
     * @Route("/admin/course", name="admin_course_index")
     * @Method("GET")
     */
    public function indexAdminAction()
    {
        $em = $this->getDoctrine()->getManager();

        $courses = $em->getRepository(Course::class)->findAll();

        return $this->render('course/index.admin.html.twig', array(
            'page_title' => 'Cursos',
            'actions' => [
                'new' => [
                    'label' => 'Incluir curso',
                    'role' => 'ROLE_ADMIN',
                    'path' => $this->generateUrl('admin_course_new'),
                ],

            ],
            'courses' => $courses,
        ));
    }
    /**
     * Lists all course entities.
     *
     * @Route("/course", name="course_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $courses = $em->getRepository(Course::class)->findAll();

        return $this->render('course/index.html.twig', array(
            'page_title' => 'Cursos',
            'back' => $this->generateUrl('home'),
            'actions' => [
                'new' => [
                    'label' => 'Incluir curso',
                    'role' => 'ROLE_ADMIN',
                    'path' => $this->generateUrl('admin_course_new'),
                ],
            ],
            'courses' => $courses,
        ));
    }

    /**
     * Lists all course entities.
     *
     * @Route("/course/{id}/certificate", name="course_certificate_show")
     * @Method("GET")
     */
    public function certificateAction(Request $request, Course $course)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $this->getUser();

        $corpo = $course->getCertificado();

        $replace_arr = [
            '[nome]' => $user->getName(),
            '[curso]' => $course->getTitle(),
        ];

        // Substitui as tags pelas respostas
        foreach($replace_arr as $pergunta => $row){

            $posPerguntaCorpo = strpos($corpo, $pergunta);

            // Se pergunta contem no corpo do certificado, caracter anterior eh virgula e nao
            // possui resposta: retira virgula
            if($posPerguntaCorpo and substr($corpo,$posPerguntaCorpo -2,1) == ';'
                and !isset($replace_arr[$pergunta])){
                $corpo = substr_replace($corpo,'',$posPerguntaCorpo -2,1);

            }

            $corpo = str_replace($pergunta, $row, $corpo);
        }

        $fundo = '';

        $html =  $this->renderView(':course:certificate.html.twig', array(
            'fundo' => $fundo,
            'corpo' => $corpo,
        ));

        include_once('/site/sdrpixel/vendor/mpdf/mpdf/src/Mpdf.php');
        $mpdf = new Mpdf(['mode' => 'utf-8','orientation' => 'L']);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;


    }

    /**
     * Creates a new course entity.
     *
     * @Route("/admin/course/new", name="admin_course_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $course = new Course();
        $form = $this->createForm('ContentBundle\Form\CourseType', $course);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $course->setCreated(new \DateTime());
            $course->setCreatedBy($this->getUser());

            $em->persist($course);
            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_course_show', array('id' => $course->getId()));
        }

        return $this->render('course/edit.html.twig', array(
            'page_title' => 'Incluir curso',
            'back' => $this->generateUrl('course_index'),
            'course' => $course,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a course entity.
     *
     * @Route("/cursos-online", name="adrpixel_index")
     * @Method("GET")
     */
    public function gotoAllAction()
    {
        return new RedirectResponse($this->getParameter('adrpixel_root'));
    }

    /**
     * Finds and displays a course entity.
     *
     * @Route("/adrpixel/course/{id}", name="adrpixel_course_show")
     * @Method("GET")
     */
    public function gotoAction(Course $course)
    {
        if($course->getVisibility() == Course::VIS_EDRPIXEL){
            return new RedirectResponse($this->getParameter('adrpixel_root').'#/course/'.$course->getId());
        }
    }





        /**
     * Finds and displays a course entity.
     *
     * @Route("/course/{id}", name="course_show")
     * @Method("GET")
     */
    public function showAction(Course $course)
    {
        $em = $this->getDoctrine()->getManager();
        $nextProgress = null;
        $lastProgress = null;
        $actions = array();

        $progresses = $em->getRepository('ContentBundle:CourseProgress')->createQueryBuilder('a')
            ->join('a.courseContent','b')
            ->andWhere('b.course = :course')->setParameter('course',$course->getId())
            ->andWhere('a.createdBy = :user')->setParameter('user',$this->getUser())
            ->orderBy('b.delta','ASC')
            ->getQuery()->getResult();

        if($progresses){
            $count = count($progresses);
            $nextProgress = $progresses[0];
            /** @var CourseProgress $previousProgress */
            $previousProgress = $progresses[0];
            $lastProgress = $progresses[$count-1];

            /**
             * @var  $key
             * @var CourseProgress $progress
             */
            foreach ($progresses as $key => $progress){
                // Verifica se o proximo nao foi iniciado ou esta pendente
                if($previousProgress->getStatus() == CourseProgress::STATUS_CONCLUIDO and
                    ($progress->getStatus() == CourseProgress::STATUS_INICIADO or $progress->getStatus() == CourseProgress::STATUS_PENDENTE)){
                    $nextProgress = $progress;
                }

                $previousProgress = $progress;
            }

            $actions['reset'] = [
                'label' => 'Reiniciar curso',
                'role' => 'ROLE_ADMIN',
                'path' => $this->generateUrl('admin_course_reset',['id' => $course->getId()])
            ];
        }



        return $this->render('course/show.html.twig', array(
            'page_title' => 'Curso: ' . $course->getTitle(),
            'lastProgress' => $lastProgress,
            'nextProgress' => $nextProgress,
            'actions' => $actions,
            'back' => $this->generateUrl('course_index'),
            'course' => $course,
        ));
    }

    /**
     * Finds and displays a course entity.
     *
     * @Route("/admin/course/{id}", name="admin_course_show")
     * @Method("GET")
     */
    public function showAdminAction(Request $request,Course $course)
    {
        $em = $this->getDoctrine()->getManager();
        $nextProgress = null;
        $lastProgress = null;

        if($tab = $request->get('tab')){
            $request->getSession()->set('tab',$request->get('tab'));
        } else {
            $request->getSession()->set('tab',0);
        }
        
        return $this->render('course/show.admin.html.twig', array(
            'page_title' => 'Curso: ' . $course->getTitle(),
            'lastProgress' => $lastProgress,
            'nextProgress' => $nextProgress,
            'back' => $this->generateUrl('admin'),
            'course' => $course,
        ));
    }

    /**
     * Finds and displays a course entity.
     *
     * @Route("/admin/course/{id}/inscritos", name="admin_course_inscritos")
     * @Method("GET")
     */
    public function inscritosAction(Request $request,Course $course)
    {
        $em = $this->getDoctrine()->getManager();
        $courseUsers = $course->getCourseUsers();
        $result = [];

        /** @var CourseUser $courseUser */
        foreach ($course->getCourseUsers() as $courseUser){
            $status = $courseUser->getCourseStatus();
            $progress = ($status[CourseProgress::STATUS_CONCLUIDO] / $course->getCourseContents()->count()) * 100;
            $result[] = [
                'courseUser' => $courseUser,
                'progress' => $progress,
            ];
        }

        return $this->render('course/inscritos.admin.html.twig', array(
            'page_title' => 'Curso: ' . $course->getTitle(),
            'result' => $result,
            'back' => $this->generateUrl('course_index'),
            'course' => $course,
        ));
    }

    /**
     * Displays a form to edit an existing course entity.
     *
     * @Route("/admin/course/progress/{id}", name="admin_course_reset")
     */
    public function resetAction(Request $request, Course $course)
    {
        $em = $this->getDoctrine()->getManager();

        $progresses = $em->getRepository('ContentBundle:CourseProgress')->createQueryBuilder('a')
            ->join('a.courseContent','b')
            ->andWhere('b.course = :course')->setParameter('course',$course->getId())
            ->andWhere('a.createdBy = :user')->setParameter('user',$this->getUser())
            ->orderBy('b.delta','ASC')
            ->getQuery()->getResult();

        /** @var CourseProgress $progress */
        foreach ($progresses as $progress){
            // Se possui quiz associado ao conteudo preenchido
            if($quiz = $progress->getCourseContent()->getQuiz()){

                if($answer = $quiz->getAnswerUser($this->getUser())){

                    foreach ($answer->getItems() as $item){
                        $em->remove($item);
                    }

                    $em->remove($answer);
                }

            }
            $em->remove($progress);
        }


        $em->flush();

        $this->addFlash('success','Curso reiniciado com sucesso');
        return $this->redirectToRoute('course_show',['id' => $course->getId()]);
    }

    /**
     * Displays a form to edit an existing course entity.
     *
     * @Route("/admin/course/{id}/edit", name="admin_course_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Course $course)
    {

        $editForm = $this->createForm('ContentBundle\Form\CourseType', $course);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Alteração efetuada com sucesso');
            return $this->redirectToRoute('admin_course_show',['id' => $course->getId()]);
        }

        return $this->render('course/edit.html.twig', array(
            'page_title' => 'Editar curso ' . $course->getTitle(),
            'back' => $this->generateUrl('admin_course_show',['id' => $course->getId()]),
            'course' => $course,
            'form' => $editForm->createView(),

        ));
    }



}
