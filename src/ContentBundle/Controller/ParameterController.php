<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Parameter;
use ContentBundle\Entity\Tag;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tag controller.
 *
 * @Route("")
 */
class ParameterController extends Controller
{
    /**
     * Lists all tag entities.
     *
     * @Route("/admin/parameter/{parameter}/edit", name="admin_parameter_edit")
     * @Method({"GET","POST"})
     */
    public function editAction(Request $request, Parameter $parameter)
    {
        $form = $this->createFormBuilder()
            ->add('value',CKEditorType::class, [
                'label' => 'Corpo',
                'data' => $parameter->getValue(),
            ])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted()){

            $parameter->setValue($form->get('value')->getViewData());
            $this->getDoctrine()->getManager()->persist($parameter);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Salvo com sucesso');
            return new RedirectResponse($request->get('back'));
        }

        return $this->render('Content/Parameter/edit.html.twig',[
            'page_title' => 'Editar ' . $parameter->getName(),
            'back' => $request->get('back'),
            'form' => $form->createView(),
        ]);
    }



}
