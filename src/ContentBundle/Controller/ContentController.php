<?php

namespace ContentBundle\Controller;


use ContentBundle\Entity\Content;
use ContentBundle\Entity\ContentAuthor;
use ContentBundle\Entity\ContentTag;

use ContentBundle\Entity\Counter;
use ContentBundle\Entity\CourseContent;
use ContentBundle\Entity\Rate;
use ContentBundle\Entity\Tag;
use ContentBundle\Entity\TagType;
use ContentBundle\Form\ContentType;
use ContentBundle\Service\CounterService;
use Doctrine\Common\Collections\ArrayCollection;
use QuizBundle\Entity\Answer;
use QuizBundle\Entity\AnswerItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ContentController extends AbstractController
{
    public $counter;



    public function __construct()
    {

    }

    /**
     * Lists all tag entities.
     *
     * @Route("/ajax/content", name="ajax_content", options={"expose=true"})
     */
    public function ajaxContentAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository(Content::class)->createQueryBuilder('a');
        $tags = $query->andWhere($query->expr()->like('a.titulo',':name'))
            ->setParameter('name','%'.$request->get('q').'%')
            ->getQuery()->getResult();

        $result = array();

        /** @var Content $tag */
        foreach ($tags as $tag){
            $result['results'][] = [
                'id' => $tag->getId(),
                'text' => $tag->getTitulo(),
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * Lists all tag entities.
     *
     * @Route("/ajax/content/full", name="ajax_content_full", options={"expose=true"})
     */
    public function ajaxContentFullAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository(Content::class)->createQueryBuilder('a');
        $rows = $query->andWhere('a.type = :type')->setParameter('type',$request->get('type'))
            ->getQuery()->getResult();



        $rows = array_slice($rows,$request->get('step'),5);

        $result = $this->render('@Content/Content/ajax.index.html.twig', ['contents' => $rows]);

        return new JsonResponse($result->getContent());
    }

    /**
     * Lists all content entities.
     *
     * @Route("/content/{content}/favorite", name="content_favorite_ajax", options={"expose"=true})
     */
    public function ajaxFavoriteAction(Request $request, Content $content){
        $user = $this->getUser();

        $rate = $this->getDoctrine()->getRepository(Rate::class)
            ->findOneBy(['user' => $user->getId(),'content' => $content->getId()]);

        if(!$rate){
            $rate = new Rate();
            $rate->setContent($content);
            $rate->setUser($user);
            $rate->setCreated(new \DateTime());
        }

        if($rate->getFavorite()){
            $rate->setFavorite(false);
        } else {
            $rate->setFavorite(true);
        }

        $this->getDoctrine()->getManager()->persist($rate);
        $this->getDoctrine()->getManager()->flush();

        $return = $this->render(':Content:favorite.html.twig',['rate' => $rate,'content' => $content]);

        return new JsonResponse($return->getContent());
    }

    /**
     * Lists all content entities.
     *
     * @Route("/aula/ajaxLoad", name="aula_ajaxLoad", options={"exposed=true"})
     */
    public function ajaxLoad(Request $request){
        $resp = array();

        $search = $request->get('search')['value'];

        return new JsonResponse($resp);
    }

    /**
     * Lists all content entities.
     *
     * @Route("/aula", name="aula_index")
     */
    public function indexAulaAction(Request $request)
    {
        return $this->indexAction($request, Content::TYPE_AULA, 'Aulas');
    }

    /**
     * Lists all content entities.
     *
     * @Route("/aula2", name="aula2_index")
     */
    public function indexAula2Action(Request $request)
    {
        return $this->render('@Content/Content/aula.html.twig', array(
            'page_title' => 'Aula',
            'back' => $this->generateUrl('home')
        ));
    }

    /**
     * Lists all content entities.
     *
     * @Route("/banco-de-imagem", name="bancodeimagem_index")
     */
    public function indexBancoDeImagemAction(Request $request)
    {
        return $this->indexAction($request, Content::TYPE_BANCOIMAGEM);
    }

    /**
     * Lists all content entities.
     *
     * @Route("/caso", name="discussaodecaso_index")
     */
    public function indexDiscussaoDeCasosAction(Request $request)
    {
        return $this->indexAction($request, Content::TYPE_DISCUSSAOCASO, 'Casos');
    }

    /**
     * Lists all content entities.
     * tipo de buscas:
     * Aula
     * Casos
     * Recente
     * Mais acessados
     * @Route("/conteudo", name="content_index")
     */
    public function indexAction(Request $request, $type = null, $page_title = null)
    {
        $em = $this->getDoctrine()->getManager();
        $contents = array();
        $tagSearch = null;

        $query = $em->getRepository(Content::class)
            ->createQueryBuilder('a')
            ->join('a.contentTags','b')
            ->join('b.tag','c')
            ->addOrderBy('a.created', 'DESC');

        if($type){
            $query->andWhere('a.type = :type')->setParameter('type',$type);
        }

        // Busca por TAG
        if($tagId = $request->get('tag')){
            $tagSearch = $em->getRepository(Tag::class)->find($tagId);
        }

        $tags = $this->getDoctrine()->getRepository(Counter::class)->getTopTag(20,$type);

        $form = $this->createFormBuilder()
            ->add('search',TextType::class, [
                'label' => 'Título',
                'required' => false,
                ]);

        $tagTypes = $em->getRepository(TagType::class)->findAll();

        /** @var TagType $tagType */
        foreach ($tagTypes as $tagType){
            $choices = array();

            /** @var Tag $tag */
            foreach ($tagType->getTags() as $tag){
                $choices[$tag->getName()] = $tag->getId();
            }

            // Busca por TAG e TYPE contem tag
            if($tagSearch and $tagType->getTags()->contains($tagSearch)){
                $data = $tagSearch->getId();
            } else {
                $data = null;
            }

            $form->add($tagType->getId(),ChoiceType::class, [
                'label' => $tagType->getName(),
                'choices' => $choices,
                'data' => $data,
                'required' => false,
            ]);
        }

        $form = $form->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted()){
            foreach ($form->getNormData() as $key => $val){
                if(is_numeric($key) and $val){
                    $query->andWhere('c.id = :i')->setParameter('i',$val);
                }
            }
            // BY TEXT
            if($search = $request->get('form')['search']){
                $query->andWhere($query->expr()->like('a.titulo',':search'))
                    ->setParameter('search','%'.$search.'%');
            }

            $contents = $query->getQuery()->getResult();

        } else {
            // busca RECENT
            if($request->get('type') == 'recent'){
                $contents = $em->getRepository(Content::class)->findBy(array(),['created' => 'DESC']);
            }

            // busca TOP
            if($request->get('type') == 'top'){
                $contents = $em->getRepository(Counter::class)->getTopContent(30);
            }

            // busca TYPE
            if($type){
                $contents = $em->getRepository(Content::class)->findBy(['type' => $type],['created' => 'DESC']);
            }

            if($tagSearch){
                $contents = $em->getRepository(Content::class)->createQueryBuilder('a')
                    ->join('a.contentTags','b')
                    ->join('b.tag','c')
                    ->andWhere('c.id = :id')->setParameter('id',$tagSearch->getId())->getQuery()->getResult();
            }

        }

        if(!$page_title){
            $page_title = 'Pesquisa';
        }



        return $this->render('@Content/Content/index.html.twig', array(
            'page_title' => $page_title,
            'tagTypes' => $em->getRepository(TagType::class)->findAll(),
            'form' => $form->createView(),
            'tags' => $tags,
            'contents' => $contents,
            'back' => $this->generateUrl('home')
        ));
    }

    /**
     * Creates a new content entity.
     *
     * @Route("/admin/content/new", name="admin_content_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $content = new Content();
        $content->setCreatedBy($this->getUser());

        if($tagName = $request->get('tag')){
            $tag = $this->getDoctrine()->getRepository(Tag::class)
                ->findOneBy(['path' => $tagName]);

            $contentTag = new ContentTag();

            $contentTag->setTag($tag);
            $content->addContentTag($contentTag);
        }

        return $this->editAction($request, $content);
    }



    /**
     * Finds and displays a content entity.
     *
     * @Route("/pt-br/{tipo}/{path}", name="content_path_old")
     */
    public function oldAction(Request $request, $tipo, $path)
    {
        $em = $this->getDoctrine()->getManager();

        if($tipo == 'tags'){
            return $this->redirectToRoute('tag_path',['path' => $path]);
        }

        $content = $this->getDoctrine()->getManager()->getRepository(Content::class)->findOneBy(['path' => $path]);

        if($content instanceof Content){
            return $this->showAction($request, $content);
        } else {
            return new RedirectResponse($this->generateUrl('home'));
        }


    }

    /**
     * Finds and displays a content entity.
     *
     * @Route("/es/especialidades/{path}", name="content_es_especialidade_old")
     */
    public function esEspecialidadeOldAction(Request $request, $path)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->redirectToRoute('especialidade_show',['path' => $path]);

    }

    /**
     * Finds and displays a content entity.
     *
     * @Route("/es/node/{node}", name="content_path_node")
     */
    public function oldNodeAction(Request $request)
    {
        $node = $request->get('node');

        $content = $this->getDoctrine()->getManager()->getRepository(Content::class)->findOneBy(['nid' => $node]);

        if($content instanceof Content){
            return $this->showAction($request, $content);
        } else {
            return new RedirectResponse($this->generateUrl('home'));
        }
    }

    /**
     * Finds and displays a content entity.
     *
     * @Route("/en/node/{node}", name="content_path_node_EN")
     */
    public function oldNodeEnAction(Request $request)
    {
        $node = $request->get('node');

        $content = $this->getDoctrine()->getManager()->getRepository(Content::class)->findOneBy(['nid' => $node]);

        if($content instanceof Content){
            return $this->showAction($request, $content);
        } else {
            return new RedirectResponse($this->generateUrl('home'));
        }
    }


    /**
     * Finds and displays a content entity.
     *
     * @Route("/conteudo/{path}", name="content_path", methods={"GET","POST"})
     */
    public function pathAction(Request $request, $path)
    {
        $content = $this->getDoctrine()->getManager()->getRepository(Content::class)->findOneBy(['path' => $path]);

        if($content instanceof Content){
            return $this->showAction($request, $content);
        } else {
            return new RedirectResponse($this->generateUrl('busca',['q' => $path]));
        }
    }

    /**
     * Finds and displays a content entity.
     *
     * @Route("/content/{id}", name="content_show")
     */
    public function showAction(Request $request, Content $content)
    {
        $related = array();

        $tags = array();

//        $this->counter->incrementContent($content);
        $em = $this->getDoctrine()->getManager();

        /** @var Tag $tag */
        foreach ($content->getContentTags() as $contentTag){
            /** @var ContentTag $contentTag */
            foreach ($contentTag->getTag()->getContentTags() as $contentTag){
                if($contentTag->getContent() and $contentTag->getContent()->getId() != $content->getId()){
                    $related[$contentTag->getContent()->getId()] = $contentTag->getContent();
                }
            }
        }

        $related = array_slice($related,0,6);

        $rate = null;

        if($this->getUser()){
            $rate = $this->getDoctrine()->getRepository(Rate::class)
                ->findOneBy(['user' => $this->getUser()->getId(),'content' => $content->getId()]);
        }



        if($quiz = $content->getQuiz()){
            $question = $quiz->getQuestions()->first();
            $form = $this->createFormBuilder()->add('answer',ChoiceType::class,[
                'label' => 'Resposta',
                'choices' => $question->getItens(),
                'choice_label' => 'text',
                'choice_value' => 'id',
                'expanded' => true,

            ])->add('time',TextType::class)
                ->getForm();
        } else {
            $form = $this->createFormBuilder()->getForm();
        }

        $form->handleRequest($request);

        if($form->isSubmitted()){
            $answer = new Answer();
            $answer->setQuiz($content->getQuiz());
            $answer->setCreated(new \DateTime());

            if($user = $this->getUser()){
                $answer->setUser($user);
            }

            $answer->setQuiz($quiz);
            $em->persist($answer);

            $answerItem = new AnswerItem();
            $answerItem->setAnswer($answer);

            $questionItemId = $request->get('form')['answer'];
            $time = $request->get('form')['time'];

            $questionItem = $this->getDoctrine()->getRepository('QuizBundle:QuestionItem')->find($questionItemId);
            $answerItem->setQuestionItem($questionItem);
            $answerItem->setCreated(new \DateTime());
            $answerItem->setType(AnswerItem::T_NORMAL);
            $answerItem->setTime($time);
            $em->persist($answerItem);

            $answer->addItem($answerItem);

            $em->flush($answer);
            $em->flush($answerItem);

            if($nextQuestion = $answer->getNextQuestion()){
                // Proxima questao
                return $this->redirectToRoute('quiz_answer_question', [
                    'answer'=> $answer->getId(),
                    'question' =>$nextQuestion->getId(),
                    'quiz' => $quiz->getId(),
                    'back' => $request->getRequestUri(),
                ]);
            } else {
                $this->addFlash('success', 'Suas questões foram salvas com sucesso');
                return $this->redirectToRoute('quiz_answer_feedback',[
                    'quiz' => $quiz->getId(),
                    'answer' => $answer->getId(),
                ]);
            }
        }

        return $this->render('@Content/Content/show.html.twig', array(
            'page_title' => $content->getTitulo(),
            'related' => $related,
            'form' => $form->createView(),
            'rate' => $rate,
            'actions' => [
                'edit' => [
                    'path' => $this->generateUrl('admin_content_edit', ['id' => $content->getId()]),
                    'label' => 'Editar',
                    'role' => 'ROLE_ADMIN'
                ],
            ],
            'content' => $content,

        ));
    }

    /**
     * Lists all content entities.
     * tipo de buscas:
     * Aula
     * Casos
     * Recente
     * Mais acessados
     * @Route("/admin/conteudo", name="admin_content_index")
     */
    public function adminShowAction(Request $request, $type = null)
    {
        $em = $this->getDoctrine()->getManager();

        $contents = null;
        $tagSearch = null;

        // Busca por TAG
        if($tagId = $request->get('tag')){
            $tagSearch = $em->getRepository(Tag::class)->find($tagId);
        }

        $tags = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(20,$type);

        $form = $this->createFormBuilder()
            ->add('search',TextType::class, [
                'label' => 'Título',
                'required' => false,
            ]);

        $tagTypes = $em->getRepository(TagType::class)->findAll();

        /** @var TagType $tagType */
        foreach ($tagTypes as $tagType){
            $choices = array();

            /** @var Tag $tag */
            foreach ($tagType->getTags() as $tag) $choices[$tag->getName()] = $tag->getId();

            // Busca por TAG e TYPE contem tag
            if($tagSearch and $tagType->getTags()->contains($tagSearch)){
                $data = $tagSearch->getId();
            } else {
                $data = null;
            }

            $form->add($tagType->getId(),ChoiceType::class, [
                'label' => $tagType->getName(),
                'choices' => $choices,
                'data' => $data,
                'required' => false,
            ]);
        }

        $form = $form->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $search = $request->get('form')['search'];
            $em = $this->getDoctrine()->getManager();

            $query = $em->getRepository(Content::class)
                ->createQueryBuilder('a')
                ->join('a.contentTags','b')
                ->join('b.tag','c')
                ->addOrderBy('a.created', 'DESC');

            if($type){
                $query->andWhere('a.type = :type')->setParameter('type',$type);
            }

            foreach ($form->getNormData() as $key => $val){
                if(is_numeric($key) and $val){
                    $query->andWhere('c.id = :i')->setParameter('i',$val);
                }
            }

            if($search){
                $query->andWhere($query->expr()->like('a.titulo',':search'))
                    ->setParameter('search','%'.$search.'%');
            }

            $contents = $query->getQuery()->getResult();


        } else {

            // busca TYPE
            if($type){
                $contents = $em->getRepository(Content::class)
                    ->createQueryBuilder('a')
                    ->join('a.contentTags','b')
                    ->join('b.tag','c')
                    ->andWhere('a.type = :type')->setParameter('type',$type)
                    ->addOrderBy('a.created', 'DESC')
                    ->getQuery()->getResult();

            }

            if($tagSearch){
                $contents = $em->getRepository(Content::class)->createQueryBuilder('a')
                    ->join('a.contentTags','b')
                    ->join('b.tag','c')
                    ->addOrderBy('a.created', 'DESC')
                    ->andWhere('c.id = :id')->setParameter('id',$tagSearch->getId())->getQuery()
                    ->getQuery()->getResult();
            }

            if(!$contents) {
                $contents = $em->getRepository(Content::class)->createQueryBuilder('a')
                    ->join('a.contentTags','b')
                    ->join('b.tag','c')
                    ->addOrderBy('a.created', 'DESC')
                    ->getQuery()->getResult();
            }
        }

        $actions = [];

        if($this->isGranted('ROLE_ADMIN')) {
            $actions[] =  [
                'label' => 'Incluir',
                'path' => $this->generateUrl('admin_content_new', ['back' => $this->generateUrl('admin_content_index')]),
            ];
        }

        $contents = array_slice($contents,0,20);

        return $this->render('@Content/Content/index.admin.html.twig', array(
            'page_title' => 'Conteúdos',
            'tagTypes' => $em->getRepository(TagType::class)->findAll(),
            'form' => $form->createView(),
            'actions' => $actions,
            'tags' => $tags,
            'contents' => $contents,
            'back' => $this->generateUrl('home')
        ));
    }

    /**
     * Displays a form to edit an existing content entity.
     *
     * @Route("/admin/content/{id}/edit", name="admin_content_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Content $content)
    {
        $tags = array();

        $em = $this->getDoctrine()->getManager();

        $tagTypes = $em->getRepository(TagType::class)->findBy(['status' => 1]);

        /** @var ContentTag $contentTag */
        foreach ($content->getContentTags() as $contentTag){
            if($contentTag->getTag()){
                $tags[$contentTag->getTag()->getTagType()->getId()][] = $contentTag->getTag();
            }
        }

        $editForm = $this->createForm('ContentBundle\Form\ContentType', $content, [
            'tags' => $tags, 'tagTypes' => $tagTypes,
        ]);

        if(!$back = $request->get('back') and $content->getId()){
            $back = $this->generateUrl('content_show', ['id' => $content->getId()]);
        }

        $authorOriginal = new ArrayCollection();

        if($content->getContentAuthors()){
            foreach ($content->getContentAuthors() as $contentAuthor){
                $authorOriginal->add($contentAuthor);
            }
        }


        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            if(!$content->getId()){
                $content->setPath($this->urlSlug($content->getTitulo()));
                $content->setCreated(new \DateTime());
            }

            /** @var ContentAuthor $item */
            foreach ($authorOriginal as &$item){
                if(false === $content->getContentAuthors()->contains($item)){
                    $content->getContentAuthors()->removeElement($item);
                    $em->persist($item);
                    $em->remove($item);
                }
            }

            foreach ($content->getContentTags() as &$contentTag){
                $this->getDoctrine()->getManager()->remove($contentTag);
                $this->getDoctrine()->getManager()->flush();
            }

            // Varre por tagtypes
            /** @var TagType $tagType */
            foreach ($tagTypes as $tagType){

                if(isset($request->get('quizbundle_content')['tagType' . $tagType->getId()])
                and $tagsIds = $request->get('quizbundle_content')['tagType' . $tagType->getId()]){
                    foreach ($tagsIds as $tagId){

                        // Nova tag
                        if(substr($tagId,0,2) == '__'){

                            $tag = new Tag();
                            $tag->setTagType($tagType);
                            $tag->setName(str_replace('__','',$tagId));
                            $tag->setPath($this->urlSlug($tag->getName()));
                            $em->persist($tag);
                        } else {
                            $tag = $this->getDoctrine()->getManager()->getRepository(Tag::class)->find($tagId);
                        }

                        $contentTag = new ContentTag();
                        $contentTag->setContent($content);
                        $contentTag->setTag($tag);
                        $this->getDoctrine()->getManager()->persist($contentTag);
                    }
                }
            }

            /** @var ContentAuthor $contentAuthor */
            if($content->getContentAuthors()){
                foreach ($content->getContentAuthors() as &$contentAuthor){
                    $contentAuthor->setContent($content);
                    $em->persist($contentAuthor);
                }
            }



            $em->persist($content);
            $em->flush();

            $this->addFlash('success', 'Alteração efetuada com sucesso');
            return new RedirectResponse($back);
        }

        $actions = [];
        if($content->getId()){
            $actions = [
                'delete' => [
                    'label' => 'Excluir',
                    'path' => $this->generateUrl('admin_content_delete', ['id' => $content->getId()]),
                ],
                'caso' => [
                    'label' => 'Caso',
                    'path' => $this->generateUrl('admin_content_caso_edit',['content' => $content->getId()])
                ],
            ];
        }


        return $this->render('@Content/Content/edit.html.twig', array(
            'page_title' => 'Editar: ' . $content->getTitulo(),
            'tagTypes' => $tagTypes,
            'actions' => $actions,
            'content' => $content,
            'back' => $back,
            'form' => $editForm->createView(),

        ));
    }
    /**
     * Displays a form to edit an existing content entity.
     *
     * @Route("/admin/content/{id}/delete", name="admin_content_delete", methods={"GET", "POST"})
     */
    public function deleteAction(Request $request, Content $content)
    {
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($content);
            $em->flush();

            $this->addFlash('success','Exclusão efetuada com sucesso');
            return $this->redirectToRoute('home');
        }

        return $this->render('default/delete.html.twig',[
            'page_title' => 'Excluir ' . $content->getTitulo() .'?',
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_content_edit', ['id' => $content->getId()]),
        ]);
    }

    public function urlSlug($str, $options = array()) {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

        $defaults = array(
            'delimiter' => '-',
            'limit' => null,
            'lowercase' => true,
            'replacements' => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);

        $char_map = array(
            // Latin
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
            'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
            'ß' => 'ss', 'Ç' => 'c', 'ç' => 'c',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
            'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
            'ÿ' => 'y',
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);

        return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }
}
