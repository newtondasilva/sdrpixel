<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Author;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Author controller.
 *
 * @Route("autor")
 */
class AuthorController extends Controller
{

    /**
     * Lists all tag entities.
     *
     * @Route("/ajax/author", name="ajax_author", options={"expose=true"})
     * @Method("GET")
     */
    public function ajaxAuthorAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository(Author::class)->createQueryBuilder('a');
        $tags = $query->andWhere($query->expr()->like('a.name',':name'))
            ->setParameter('name','%'.$request->get('q').'%')
            ->getQuery()->getResult();

        $result = array();

        /** @var Author $tag */
        foreach ($tags as $tag){
            $result[] = [
                'id' => $tag->getId(),
                'text' => $tag->getName(),
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * Lists all Author entities.
     *
     * @Route("/", name="author_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $authors = $em->getRepository(Author::class)
            ->findBy(array(),['name' => 'ASC']);

        return $this->render('@Content/Author/index.html.twig', array(
            'page_title' => 'Autores',
            'actions' => [
                'new' => [
                    'label' => 'Incluir',
                    'path' => $this->generateUrl('author_new'),
                    'role' => 'ROLE_ADMIN',
                ],
            ],
            'authors' => $authors,
        ));
    }

    /**
     * Creates a new Author entity.
     *
     * @Route("/new", name="author_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request)
    {
        $author = new Author();
        return $this->editAction($request, $author);
    }

    /**
     * Finds and displays a Author entity.
     *
     * @Route("/{id}", name="author_show")
     * @Method("GET")
     */
    public function showAction(Author $author)
    {
        return $this->render('@Content/Author/show.html.twig', array(
            'author' => $author
        ));
    }

    /**
     * Displays a form to edit an existing Author entity.
     *
     * @Route("/{id}/edit", name="author_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Author $author)
    {
        $editForm = $this->createForm('ContentBundle\Form\AuthorType', $author);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($author);
            $em->flush();


            $this->addFlash('success','Alteração efetuada com sucesso');
            return $this->redirectToRoute('author_index', array('id' => $author->getId()));
        }

        if($author->getId()){
            $actions = [
                'new' => [
                    'label' => 'Excluir',
                    'path' => $this->generateUrl('author_delete',['id' => $author->getId()]),
                    'role' => 'ROLE_ADMIN',
                ],
            ];
        } else $actions = array();


        return $this->render('@Content/Author/edit.html.twig', array(
            'page_title' => 'Editar autor',
            'back' => $this->generateUrl('author_index'),
            'Author' => $author,
            'actions' => $actions,
            'form' => $editForm->createView(),

        ));
    }

    /**
     * Deletes a Author entity.
     *
     * @Route("/{id}/delete", name="author_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Author $author)
    {
        $form = $this->createFormBuilder($author)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($author);
            $em->flush();
            $this->addFlash('success','Exclusão efetuada com sucesso');
            return $this->redirectToRoute('author_index');
        }

        return $this->render(':default:delete.html.twig', array(
            'page_title' => 'Excluir autor ' . $author->getName(),
            'back' => $this->generateUrl('author_index'),
            'form' => $form->createView(),

        ));
    }
}
