<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Evento;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Evento controller.
 *
 * @Route("")
 */
class EventoController extends AbstractController
{


    /**
     * Lists all evento entities.
     *
     * @Route("/evento/sidebar", name="evento_sidebar")
     * @Method("GET")
     */
    public function sidebarAction()
    {
        $em = $this->getDoctrine()->getManager();

        $eventos = $em->getRepository(Evento::class)->findBy(['published' => true],['id' => 'DESC']);

        return $this->render('@App/HomeNew/block.eventos.html.twig', array(
            'eventos' => $eventos,
        ));
    }

    /**
     * Lists all evento entities.
     *
     * @Route("/evento", name="evento_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            $filter = array();
        } else {
            $filter = ['published' => true];
        }

        $eventos = $em->getRepository(Evento::class)->findBy($filter,['id' => 'DESC']);


        return $this->render('ContentBundle:Evento:index.html.twig', array(
            'page_title' => 'Cursos Presenciais',
            'back' => $this->generateUrl('home'),
            'actions' => [
                'new' => [
                    'label' => 'Incluir',
                    'path' => $this->generateUrl('admin_evento_new'),
                    'role' => 'ROLE_ADMIN',
                ],
            ],
            'eventos' => $eventos,
        ));
    }

    /**
     * Lists all evento entities.
     *
     * @Route("/admin/evento", name="admin_evento_index")
     * @Method("GET")
     */
    public function adminIndexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            $filter = array();
        } else {
            $filter = ['published' => true];
        }

        $eventos = $em->getRepository(Evento::class)->findBy($filter,['id' => 'DESC']);


        return $this->render('ContentBundle:Evento:index.html.twig', array(
            'page_title' => 'Cursos Presenciais',
            'back' => $this->generateUrl('home'),
            'actions' => [
                'new' => [
                    'label' => 'Incluir',
                    'path' => $this->generateUrl('admin_evento_new'),
                    'role' => 'ROLE_ADMIN',
                ],
            ],
            'eventos' => $eventos,
        ));
    }

    /**
     * Creates a new evento entity.
     *
     * @Route("/admin/evento/new", name="admin_evento_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $evento = new Evento();
        $evento->setPublished(true);

        $form = $this->createForm('ContentBundle\Form\EventoType', $evento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $evento->setCreated(new \DateTime());
            $evento->setCreatedBy($this->getUser());

            $em->persist($evento);
            $em->flush();
            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('evento_show', array('id' => $evento->getId()));
        }

        return $this->render('ContentBundle:Evento:edit.html.twig', array(
            'page_title' => 'Incluir evento',
            'back' => $this->generateUrl('evento_index'),
            'evento' => $evento,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a evento entity.
     *
     * @Route("/evento/{id}", name="evento_show")
     * @Method("GET")
     */
    public function showAction(Evento $evento)
    {


        return $this->render('ContentBundle:Evento:show.html.twig', array(
            'back' => $this->generateUrl('evento_index'),
            'page_title' => 'Curso presencial: ' . $evento->getTitle(),
            'evento' => $evento,
            'actions' => [
                'edit' => [
                    'label' => 'Editar',
                    'path' => $this->generateUrl('admin_evento_edit',['id' => $evento->getId()]),
                    'role' => 'ROLE_ADMIN',
                ],
                'delete' => [
                    'label' => 'Excluir',
                    'path' => $this->generateUrl('admin_evento_delete',['id' => $evento->getId()]),
                    'role' => 'ROLE_ADMIN',
                ],
            ],

        ));
    }

    /**
     * Displays a form to edit an existing evento entity.
     *
     * @Route("/{id}/edit", name="admin_evento_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Evento $evento)
    {

        $editForm = $this->createForm('ContentBundle\Form\EventoType', $evento);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('evento_show', array('id' => $evento->getId()));
        }

        return $this->render('ContentBundle:Evento:edit.html.twig', array(
            'page_title' => 'Editar ' . $evento->getTitle(),
            'evento' => $evento,
            'back' => $this->generateUrl('evento_show', ['id' => $evento->getId()]),
            'form' => $editForm->createView(),

        ));
    }

    /**
     * Deletes a evento entity.
     *
     * @Route("/evento/{id}/delete", name="admin_evento_delete")

     */
    public function deleteAction(Request $request, Evento $evento)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($evento);
        $em->flush();
        $this->addFlash('success', 'Exclusão efetuada com sucesso');

        return $this->redirectToRoute('admin_evento_index');
    }


}
