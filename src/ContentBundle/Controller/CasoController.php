<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Caso;
use ContentBundle\Entity\Content;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Caso controller.
 *
 * @Route("")
 */
class CasoController extends Controller
{

    /**
     * Finds and displays a caso entity.
     *
     * @Route("/content/{content}/caso", name="content_caso_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Content $content)
    {

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('home');
        }

        return $this->render('Content/Caso/show.html.twig', array(
            'page_title' => $content->getCaso()->getResume(),
            'actions' => [
                'edit' => [
                    'url' => $this->generateUrl('admin_content_caso_edit',['content' => $content->getId()]),
                    'label' => 'Editar caso',
                    'ROLE' => 'ROLE_ADMIN',
                ],
            ],
            'caso' => $content->getCaso(),
            'back' => $back,
        ));
    }

    /**
     * Displays a form to edit an existing caso entity.
     *
     * @Route("admin/content/{content}/caso/edit", name="admin_content_caso_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Content $content)
    {
        $em = $this->getDoctrine()->getManager();

        if(!$caso = $content->getCaso()){
            $caso = new Caso();
            $caso->setContent($content);
        }

        $deleteForm = $this->createDeleteForm($caso);
        $editForm = $this->createForm('ContentBundle\Form\CasoType', $caso);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $content->setCaso($caso);
            $em->persist($content);
            $em->persist($caso);
            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('content_caso_show', array(
                'content' => $caso->getContent()->getId(),
                'back' => $request->getRequestUri(),
            ));
        }

        return $this->render('Content/Caso/edit.html.twig', array(
            'page_title' => $content->getTitulo() . ' - Caso',
            'back' => $this->generateUrl('admin_content_edit',['id' => $content->getId()]),
            'caso' => $caso,
            'content' => $content,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a caso entity.
     *
     * @Route("admin/content/{content}/caso/delete", name="admin_content_caso_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Content $content)
    {
        $caso = $content->getCaso();
        $contentId = $content->getId();
        $form = $this->createDeleteForm($caso);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $content->setCaso();
            $em->remove($caso);
            $em->flush();
            $this->addFlash('success', 'Exclusão efetuada com sucesso');
        }

        return $this->redirectToRoute('admin_content_edit',['id' => $contentId]);
    }

    /**
     * Creates a form to delete a caso entity.
     *
     * @param Caso $caso The caso entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Caso $caso)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_content_caso_delete', array('content' => $caso->getContent()->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
