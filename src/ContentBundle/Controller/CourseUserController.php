<?php

namespace ContentBundle\Controller;

use ContentBundle\Form\CourseUserType;
use ContentBundle\Entity\CourseUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CourseUserController extends AbstractController
{


    /**
     * Finds and displays a course entity.
     *
     * @Route("/admin/courseUser/{courseUser}", name="admin_courseUser_show")
     * @Method({"GET","POST"})
     */
    public function inscritosAction(Request $request,CourseUser $courseUser)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CourseUserType::class, $courseUser);

        $form->handleRequest($request);

        if($form->isSubmitted()){
            $em->persist($courseUser);
            $em->flush();
            $this->addFlash('success','Alterações realizadas com sucesso');
        }


        return $this->render('courseUser/show.html.twig', array(
            'page_title' => 'Aluno: ' . $courseUser->getUser()->getName() .
        ' - Curso: ' . $courseUser->getCourse()->getTitle(),
            'form' => $form->createView(),
            'courseUser' => $courseUser,
            'back' => $this->generateUrl('user_show',[
                'id' => $courseUser->getUser()->getId(),
            ])
        ));
    }

}
