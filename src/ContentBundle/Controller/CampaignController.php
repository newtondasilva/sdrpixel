<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Campaign controller.
 *
 * @Route("")
 */
class CampaignController extends Controller
{
    /**
     * Lists all campaign entities.
     *
     * @Route("/admin/campaign", name="admin_campaign_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $campaigns = $em->getRepository(Campaign::class)->findBy([],['id' => 'DESC']);

        return $this->render('campaign/index.html.twig', array(
            'page_title' => 'Campanhas',
            'actions' => [
                'new' => [
                    'label' => 'Incluir campanha',
                    'path' => $this->generateUrl('admin_campaign_new')
                ]
            ],
            'back' => $this->generateUrl('home'),
            'campaigns' => $campaigns,
        ));
    }

    /**
     * Creates a new campaign entity.
     *
     * @Route("/admin/campaign/new", name="admin_campaign_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $campaign = new Campaign();
        $form = $this->createForm('ContentBundle\Form\CampaignType', $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $campaign->setUpdatedAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($campaign);
            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_campaign_index');
        }

        return $this->render('campaign/edit.html.twig', array(
            'page_title' => 'Incluir campanha',
            'back' => $this->generateUrl('admin_campaign_index'),
            'campaign' => $campaign,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a campaign entity.
     *
     * @Route("/campaign/sidebar", name="campaign_sidebar")
     * @Method("GET")
     */
    public function sidebarAction()
    {
        $campaign = $this->getDoctrine()->getRepository(Campaign::class)
            ->createQueryBuilder('campaign')
            ->andWhere('campaign.published = :published')->setParameter('published',true)
            ->andWhere('campaign.lateralName is not null')
            ->getQuery()->getOneOrNullResult();

        return $this->render('campaign/sidebar.html.twig', array(
            'campaign' => $campaign,
        ));
    }

    /**
     * Finds and displays a campaign entity.
     *
     * @Route("/campaign/topo", name="campaign_topo")
     * @Method("GET")
     */
    public function topoAction()
    {
        $campaign = $this->getDoctrine()->getRepository(Campaign::class)
            ->createQueryBuilder('campaign')
            ->andWhere('campaign.published = :published')->setParameter('published',true)
            ->andWhere('campaign.topoName is not null')
            ->getQuery()->getResult();


        return $this->render('campaign/topo.html.twig', array(
            'campaigns' => $campaign,
        ));
    }

    /**
     * Finds and displays a campaign entity.
     *
     * @Route("/campaign/modal", name="campaign_modal")
     * @Method("GET")
     */
    public function modalAction()
    {
        $campaign = $this->getDoctrine()->getRepository(Campaign::class)
            ->createQueryBuilder('campaign')
            ->andWhere('campaign.published = :published')->setParameter('published',true)
            ->andWhere('campaign.modalName is not null')
            ->getQuery()->getOneOrNullResult();

        return $this->render('campaign/modal.html.twig', array(
            'campaign' => $campaign,
        ));
    }

    /**
     * Displays a form to edit an existing campaign entity.
     *
     * @Route("/admin/campaign/{id}/edit", name="admin_campaign_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Campaign $campaign)
    {

        $editForm = $this->createForm('ContentBundle\Form\CampaignType', $campaign);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Alteração efetuada com sucesso');
            return $this->redirectToRoute('admin_campaign_index');
        }

        return $this->render('campaign/edit.html.twig', array(
            'page_title' => 'Campanha ' . $campaign->getId(),
            'back' => $this->generateUrl('admin_campaign_index'),
            'campaign' => $campaign,
            'actions' => [
                'delete' => [
                    'label' => 'Excluir',
                    'path' => $this->generateUrl('admin_campaign_delete', ['id' => $campaign->getId()]),
                ],
            ],
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a campaign entity.
     *
     * @Route("/admin/campaign/{id}/delete", name="admin_campaign_delete")
     */
    public function deleteAction(Campaign $campaign)
    {
        $em = $this->getDoctrine()->getManager();

        $this->addFlash('success','Exclusão efetuada com sucesso');
        $em->remove($campaign);
        $em->flush();

        return $this->redirectToRoute('admin_campaign_index');
    }

    /**
     * Creates a form to delete a campaign entity.
     *
     * @param Campaign $campaign The campaign entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Campaign $campaign)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_campaign_delete', array('id' => $campaign->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
