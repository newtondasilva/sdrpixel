<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Course;
use ContentBundle\Entity\CourseSection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Coursesection controller.
 *
 * @Route("admin/course/{course}/section")
 */
class CourseSectionController extends Controller
{
    /**
     * Lists all courseSection entities.
     *
     * @Route("/", name="admin_course_section_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $courseSections = $em->getRepository('ContentBundle:CourseSection')->findAll();

        return $this->render('coursesection/index.html.twig', array(
            'courseSections' => $courseSections,
        ));
    }

    /**
     * Creates a new courseSection entity.
     *
     * @Route("/new", name="admin_course_section_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Course $course)
    {
        $nextDelta = 0;

        $courseSection = new Coursesection();
        $courseSection->setCourse($course);
        return $this->editAction($request, $courseSection);
    }

    /**
     * Finds and displays a courseSection entity.
     *
     * @Route("/{id}", name="admin_course_section_show")
     * @Method("GET")
     */
    public function showAction(CourseSection $courseSection)
    {
        $deleteForm = $this->createDeleteForm($courseSection);

        return $this->render('coursesection/show.html.twig', array(
            'courseSection' => $courseSection,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing courseSection entity.
     *
     * @Route("/{id}/edit", name="admin_course_section_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CourseSection $courseSection)
    {
        $editForm = $this->createForm('ContentBundle\Form\CourseSectionType', $courseSection);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($courseSection);
            $em->flush();
            $this->addFlash('success','Alteração efetuada com sucesso');
            return $this->redirectToRoute('admin_course_show', array(
                'id' => $courseSection->getCourse()->getId(),
                'tab' => 1,
            ));
        }

        return $this->render('coursesection/edit.html.twig', array(
            'page_title' => 'Seção: ' . $courseSection->getTitle(),
            'back' => $this->generateUrl('admin_course_show', ['id' => $courseSection->getCourse()->getId()]),
            'courseSection' => $courseSection,
            'form' => $editForm->createView(),

        ));
    }

    /**
     * Deletes a courseSection entity.
     *
     * @Route("/{id}", name="admin_course_section_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CourseSection $courseSection)
    {
        $form = $this->createDeleteForm($courseSection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($courseSection);
            $em->flush();
        }

        return $this->redirectToRoute('admin_course_section_index');
    }

    /**
     * Creates a form to delete a courseSection entity.
     *
     * @param CourseSection $courseSection The courseSection entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CourseSection $courseSection)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_course_section_delete', array('id' => $courseSection->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
