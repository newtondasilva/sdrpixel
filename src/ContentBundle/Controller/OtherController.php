<?php

namespace ContentBundle\Controller;

use ContentBundle\Entity\Parameter;
use ContentBundle\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tag controller.
 *
 * @Route("")
 */
class OtherController extends Controller
{
    /**
     * Lists all tag entities.
     *
     * @Route("/contato", name="other_contato")
     * @Method({"GET","POST"})
     */
    public function contactAction(Request $request)
    {
        $this->addFlash('success','Página desativada');
        return $this->redirectToRoute('home');

        $form = $this->createFormBuilder()
            ->add('nome',TextType::class, [
                'label' => 'Nome'
            ])
            ->add('email', EmailType::class,[
                'label' => 'E-mail'
            ])
            ->add('message', TextareaType::class,[
                'label' => 'Mensagem'
            ])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted()){

            $message = (new \Swift_Message('[DrPixel] Contato'))
                ->setFrom($this->getParameter('from'))
                ->setTo($form->getNormData()['email'])
                ->setCc('newton@fcm.unicamp.br')
                ->setBody(
                    $this->renderView(
                        'Emails/contact.html.twig',
                        array('message' => $form->getNormData()['message'],'nome' => $form->getNormData()['nome'])
                    ),
                    'text/html'
                );

            //$this->get('mailer')->send($message);

            $this->addFlash('success','Mensagem enviado com sucesso');
        }

        return $this->render('Content/Other/contact.html.twig',[
            'page_title' => 'Contato',
            'back' => $this->generateUrl('home'),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Lists all tag entities.
     *
     * @Route("/sobre", name="other_sobre")
     * @Method({"GET","POST"})
     */
    public function aboutAction(Request $request)
    {
        $sobre = $this->getDoctrine()->getRepository('ContentBundle:Parameter')
            ->findOneBy(['name' => 'page_sobre']);

        $aventals = $this->getDoctrine()->getRepository(Avental::class)
            ->findBy([],['name' => 'ASC']);

        if(!$sobre){
            $sobre = new Parameter();
            $sobre->setName('page_sobre');
            $sobre->setValue('');
            $this->getDoctrine()->getManager()->persist($sobre);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render(':Content/Other:about.html.twig',[
            'page_title' => 'Sobre',
            'aventals' => $aventals,
            'actions' => [
                'edit' => [
                    'label' => 'Editar',
                    'path' => $this->generateUrl('admin_parameter_edit', ['parameter' => $sobre->getId(),
                        'back' => $request->getUri()]),

                ],
            ],
            'sobre' => $sobre,
            'back' => $this->generateUrl('home'),
        ]);
    }

    /**
     * Lists all tag entities.
     *
     * @Route("/font", name="other_font")
     */
    public function fontAction(Request $request)
    {
        return $this->render(':Content/Other:font.html.twig',[
            'page_title' => 'Fonte',
            'back' => $this->generateUrl('home'),
        ]);
    }

    /**
     * Lists all tag entities.
     *
     * @Route("/minicex", name="other_minicex")
     */
    public function minicexAction(Request $request)
    {
        return $this->redirect('https://drpixel.fcm.unicamp.br/minicex/index.html');
    }

}
