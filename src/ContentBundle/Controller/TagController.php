<?php

namespace ContentBundle\Controller;

use AppBundle\Service\Tasker;
use ContentBundle\Entity\Content;
use ContentBundle\Entity\Tag;
use QuizBundle\Entity\Quiz;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tag controller.
 *
 */
class TagController extends AbstractController
{
    /** @var Tasker */
    public $tasker;

    public function __construct(){

    }

    /**
     * Lists all tag entities.
     *
     * @Route("/ajax/tag", name="ajax_tag", options={"expose=true"})
     */
    public function ajaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository(Tag::class)->createQueryBuilder('a');
        $tags = $query->andWhere($query->expr()->like('a.name',':name'))
            ->andWhere('a.tagType = :type')->setParameter('type', $request->get('tagType'))
            ->setParameter('name','%'.$request->get('q').'%')
            ->getQuery()->getResult();

        $result = array();

        foreach ($tags as $tag){
            $result['results'][] = [
                'id' => $tag->getId(),
                'text' => $tag->getName(),
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * Lists all tag entities.
     *
     * @Route("/tag/tipo/{tipo}", name="tag_tipo")

     */
    public function tipoAction(Request $request, $tipo)
    {
        $em = $this->getDoctrine()->getManager();


        if(!$tagType = $em->getRepository(TagType::class)->findOneBy(['path' => $tipo])){
            return $this->redirectToRoute('home');
        }

        $actions = [
            'add' => [
                'label' => 'Criar ' . $tagType->getName(),
                'path' => $this->generateUrl('admin_tag_new',['type' => $tagType->getId()]),
                'role' => 'ROLE_ADMIN'
            ],
        ];

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('home');
        }

        $em = $this->getDoctrine()->getManager();

        $tags = $em->getRepository(Tag::class)->createQueryBuilder('a')
            ->andWhere('a.tagType = :type')->setParameter('type',$tagType->getId())
            ->andwhere('a.children is null')
            ->orderBy('a.name')->getQuery()->getResult();



        return $this->render('Content/Especialidade/index.html.twig', array(
            'page_title' => $tagType->getName(),
            'actions' => $actions,
            'tags' => $tags,
            'back' => $back,
        ));
    }

    /**
     * Lists all tag entities.
     *
     * @Route("/admin/tag", name="admin_tag_index")

     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tags = $em->getRepository(Tag::class)->findAll();

        $tagTypes = $em->getRepository(TagType::class)->findAll();
        $actions = [];
        foreach ($tagTypes as $tagType){
            $actions[$tagType->getId()] = [
                'label' => 'Incluir ' .$tagType->getName(),
                'url' => $this->generateUrl('admin_tag_new',['type' => $tagType->getId()])
            ];
        }


        return $this->render(':Content/Tag:index.html.twig', array(
            'page_title' => 'Tags',
            'actions' => $actions,
            'tagTypes' => $tagTypes,
            'back' => $this->generateUrl('home'),
            'tags' => $tags,
        ));
    }

    /**
     * Creates a new tag entity.
     *
     * @Route("/admin/tag/new", name="admin_tag_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tag = new Tag();

        $em = $this->getDoctrine()->getManager();

        if($type = $request->get('type')){
            $tagType = $em->getRepository(TagType::class)->find($type);
            $tag->setTagType($tagType);
        }

        $form = $this->createForm('ContentBundle\Form\TagType', $tag,['tags' => $tagType->getTags()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tag->setPath(Quiz::url_Slug($tag->getName()));
            $em->persist($tag);
            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_tag_index');
        }

        return $this->render('Content/Tag/edit.html.twig', array(
            'page_title' => 'Incluir tag',
            'back' => $this->generateUrl('home'),
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tag entity.
     *
     * @Route("/tag/{path}", name="tag_path")
     */
    public function showAction(Request $request, $path)
    {
        if(!$back = $request->get('back')){
            $back = $this->generateUrl('home');
        }

        $em = $this->getDoctrine()->getManager();

        $tag = $em->getRepository(Tag::class)->findOneBy(['path' => $path]);


        if(!$tag) {
            $this->addFlash('info','Página não encontrada');
            return $this->redirectToRoute('busca',['q' => $path]);
        }

        $contents = $em->getRepository(Content::class)->getByTag($tag->getId());

        return $this->render('Content/Tag/show.html.twig', array(
            'page_title' => $tag->getName(),
            'contents' => $contents,
            'back' => $back,
        ));
    }

    /**
     * Displays a form to edit an existing tag entity.
     *
     * @Route("/admin/tag/{id}/edit", name="admin_tag_edit", methods={"GET", "POST"})

     */
    public function editAction(Request $request, Tag $tag)
    {
        $editForm = $this->createForm('ContentBundle\Form\TagType', $tag, ['tags' => $tag->getTagType()->getTags()]);
        $editForm->handleRequest($request);

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('admin_tag_index');
        }

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Alteração efetuada com sucesso');
            return new RedirectResponse($back);
        }

        return $this->render('Content/Tag/edit.html.twig', array(
            'page_title' => 'Editar tag ' . $tag->getName(),
            'back' => $back,
            'tag' => $tag,
            'actions' => [
                'delete' => [
                    'label' => 'Excluir',
                    'path' => $this->generateUrl('admin_tag_delete',['id' => $tag->getId()])
                ],
            ],
            'form' => $editForm->createView(),

        ));
    }

    /**
     * Displays a form to edit an existing tag entity.
     *
     * @Route("/admin/tag/{id}/delete", name="admin_tag_delete", methods={"GET", "POST"})

     */
    public function deleteAction(Request $request, Tag $tag)
    {
        if(!$back = $request->get('back')){
            $back = $this->generateUrl('admin_tag_index');
        }

        $this->getDoctrine()->getManager()->remove($tag);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success','Exclusão efetuada com sucesso');
        return new RedirectResponse($back);
    }


}
