<?php

namespace ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Tag
 *
 * @ORM\Table(name="cont_tag")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\TagRepository")
 * @UniqueEntity(
 *      fields={"tagType","name"},
 *     errorPath="name",
 *     message="Tag já existente para o tipo"
 * )
 */
class Tag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __toString()
    {
        return $this->getName();
    }



    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\ContentTag", mappedBy="tag", cascade={"persist","remove"})
     */
    private $contentTags;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Counter", mappedBy="tag", cascade={"persist","remove"})
     */
    private $counters;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Tag", mappedBy="children", cascade={"persist","remove"})
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Tag", inversedBy="parent")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\TagType", inversedBy="tags")
     * @ORM\JoinColumn(name="tag_type_id", referencedColumnName="id")
     */
    private $tagType;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contentTags = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * Add contentTag
     *
     * @param \ContentBundle\Entity\ContentTag $contentTag
     *
     * @return Tag
     */
    public function addContentTag(\ContentBundle\Entity\ContentTag $contentTag)
    {
        $this->contentTags[] = $contentTag;

        return $this;
    }

    /**
     * Remove contentTag
     *
     * @param \ContentBundle\Entity\ContentTag $contentTag
     */
    public function removeContentTag(\ContentBundle\Entity\ContentTag $contentTag)
    {
        $this->contentTags->removeElement($contentTag);
    }

    /**
     * Get contentTags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContentTags()
    {
        return $this->contentTags;
    }

    /**
     * Set tagType
     *
     * @param \ContentBundle\Entity\TagType $tagType
     *
     * @return Tag
     */
    public function setTagType(\ContentBundle\Entity\TagType $tagType = null)
    {
        $this->tagType = $tagType;

        return $this;
    }

    /**
     * Get tagType
     *
     * @return \ContentBundle\Entity\TagType
     */
    public function getTagType()
    {
        return $this->tagType;
    }

    /**
     * Add counter
     *
     * @param \ContentBundle\Entity\Counter $counter
     *
     * @return Tag
     */
    public function addCounter(\ContentBundle\Entity\Counter $counter)
    {
        $this->counters[] = $counter;

        return $this;
    }

    /**
     * Remove counter
     *
     * @param \ContentBundle\Entity\Counter $counter
     */
    public function removeCounter(\ContentBundle\Entity\Counter $counter)
    {
        $this->counters->removeElement($counter);
    }

    /**
     * Get counters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCounters()
    {
        return $this->counters;
    }

    /**
     * Add parent.
     *
     * @param \ContentBundle\Entity\Tag $parent
     *
     * @return Tag
     */
    public function addParent(\ContentBundle\Entity\Tag $parent)
    {
        $this->parent[] = $parent;

        return $this;
    }

    /**
     * Remove parent.
     *
     * @param \ContentBundle\Entity\Tag $parent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeParent(\ContentBundle\Entity\Tag $parent)
    {
        return $this->parent->removeElement($parent);
    }

    /**
     * Get parent.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set children.
     *
     * @param \ContentBundle\Entity\Tag|null $children
     *
     * @return Tag
     */
    public function setChildren(\ContentBundle\Entity\Tag $children = null)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children.
     *
     * @return \ContentBundle\Entity\Tag|null
     */
    public function getChildren()
    {
        return $this->children;
    }
}
