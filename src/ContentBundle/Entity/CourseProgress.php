<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CourseProgress
 *
 * @ORM\Table(name="cont_course_progress")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\CourseProgressRepository")
 */
class CourseProgress
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseContent","courseUser","courseProgress"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\CourseContent", inversedBy="courseProgresses")
     * @ORM\JoinColumn(name="course_content_id", referencedColumnName="id")
     * @Serializer\Groups({"default","list","show","courseContent","courseUser"})
     * @Serializer\SerializedName("courseContent")
     */
    private $courseContent;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\CourseUser", inversedBy="courseProgresses")
     * @ORM\JoinColumn(name="course_user_id", referencedColumnName="id")
     * @Serializer\Groups({"default","list","show","courseContent","courseUser"})
     * @Serializer\SerializedName("courseUser")
     */
    private $courseUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="courseProgresses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\Answer", mappedBy="courseProgress",cascade={"remove","persist"})
     * @Serializer\Groups({"default","list","show","courseContent","courseProgress"})
     *
     */
    private $answers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Serializer\Groups({"default","list","show"})
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     * @Serializer\Groups({"default","list","show","courseContent","courseUser"})
     */
    private $status;

    const STATUS_PENDENTE = 'P';
    const STATUS_INICIADO = 'I';
    const STATUS_CONCLUIDO = 'C';
    const STATUS_PENDENTE_LABEL = 'Pendente';
    const STATUS_INICIADO_LABEL = 'Iniciado';
    const STATUS_CONCLUIDO_LABEL = 'Concluído';

    public function getStatusLabel(){
        switch ($this->getStatus()){
            case CourseProgress::STATUS_PENDENTE: return CourseProgress::STATUS_PENDENTE_LABEL;
            case CourseProgress::STATUS_INICIADO: return CourseProgress::STATUS_INICIADO_LABEL;
            case CourseProgress::STATUS_CONCLUIDO: return CourseProgress::STATUS_CONCLUIDO_LABEL;
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return CourseProgress
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return CourseProgress
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent|null $courseContent
     *
     * @return CourseProgress
     */
    public function setCourseContent(\ContentBundle\Entity\CourseContent $courseContent = null)
    {
        $this->courseContent = $courseContent;

        return $this;
    }

    /**
     * Get courseContent.
     *
     * @return \ContentBundle\Entity\CourseContent|null
     */
    public function getCourseContent()
    {
        return $this->courseContent;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return CourseProgress
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add answer.
     *
     * @param \QuizBundle\Entity\Answer $answer
     *
     * @return CourseProgress
     */
    public function addAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer.
     *
     * @param \QuizBundle\Entity\Answer $answer
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAnswer(\QuizBundle\Entity\Answer $answer)
    {
        return $this->answers->removeElement($answer);
    }

    /**
     * Get answers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add courseUser.
     *
     * @param \ContentBundle\Entity\CourseUser $courseUser
     *
     * @return CourseProgress
     */
    public function addCourseUser(\ContentBundle\Entity\CourseUser $courseUser)
    {
        $this->courseUser[] = $courseUser;

        return $this;
    }

    /**
     * Remove courseUser.
     *
     * @param \ContentBundle\Entity\CourseUser $courseUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseUser(\ContentBundle\Entity\CourseUser $courseUser)
    {
        return $this->courseUser->removeElement($courseUser);
    }

    /**
     * Get courseUser.
     *
     * @return CourseUser
     */
    public function getCourseUser()
    {
        return $this->courseUser;
    }

    /**
     * Set course.
     *
     * @param \ContentBundle\Entity\CourseUser|null $courseUser
     *
     * @return CourseProgress
     */
    public function setCourseUser(\ContentBundle\Entity\CourseUser $courseUser = null)
    {
        $this->courseUser = $courseUser;

        return $this;
    }

}
