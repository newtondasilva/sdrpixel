<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Campaign
 *
 * @ORM\Table(name="content_campaign")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\CampaignRepository")
 * @Vich\Uploadable
 */
class Campaign
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published;



    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;



    /**
     * @var string|null
     *
     * @ORM\Column(name="redirect", type="text", nullable=true)
     */
    private $redirect;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set redirect.
     *
     * @param string|null $redirect
     *
     * @return Campaing
     */
    public function setRedirect($redirect = null)
    {
        $this->redirect = $redirect;

        return $this;
    }

    /**
     * Get redirect.
     *
     * @return string|null
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Campaing
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }



    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Campaign
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return bool
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Campaign
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="lateralname")
     *
     * @var Lateral
     */
    private $lateral;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $lateralName;

    /**
     * If manually uploading a lateral (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedLateral' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'Lateral' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $lateral
     *
     * @return PlanesPageSlideshow
     */
    public function setLateral(\Symfony\Component\HttpFoundation\File\File $lateral = null)
    {
        $this->lateral = $lateral;

        if ($lateral) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the lateral is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getLateral()
    {
        return $this->lateral;
    }

    /**
     * @param string $lateralName
     *
     * @return PlanesPageSlideshow
     */
    public function setLateralName($lateralName)
    {
        $this->lateralName = $lateralName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLateralName()
    {
        return $this->lateralName;
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="toponame")
     *
     * @var Topo
     */
    private $topo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $topoName;

    /**
     * If manually uploading a topo (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedTopo' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'Topo' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $topo
     *
     * @return PlanesPageSlideshow
     */
    public function setTopo(\Symfony\Component\HttpFoundation\File\File $topo = null)
    {
        $this->topo = $topo;

        if ($topo) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the topo is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getTopo()
    {
        return $this->topo;
    }

    /**
     * @param string $topoName
     *
     * @return PlanesPageSlideshow
     */
    public function setTopoName($topoName)
    {
        $this->topoName = $topoName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTopoName()
    {
        return $this->topoName;
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="modalname")
     *
     * @var Modal
     */
    private $modal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $modalName;

    /**
     * If manually uploading a modal (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedModal' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'Modal' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $modal
     *
     * @return PlanesPageSlideshow
     */
    public function setModal(\Symfony\Component\HttpFoundation\File\File $modal = null)
    {
        $this->modal = $modal;

        if ($modal) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the modal is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getModal()
    {
        return $this->modal;
    }

    /**
     * @param string $modalName
     *
     * @return PlanesPageSlideshow
     */
    public function setModalName($modalName)
    {
        $this->modalName = $modalName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getModalName()
    {
        return $this->modalName;
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="slideshowname")
     *
     * @var Slideshow
     */
    private $slideshow;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $slideshowName;

    /**
     * If manually uploading a slideshow (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedSlideshow' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'Slideshow' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $slideshow
     *
     * @return PlanesPageSlideshow
     */
    public function setSlideshow(\Symfony\Component\HttpFoundation\File\File $slideshow = null)
    {
        $this->slideshow = $slideshow;

        if ($slideshow) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the slideshow is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * @param string $slideshowName
     *
     * @return PlanesPageSlideshow
     */
    public function setSlideshowName($slideshowName)
    {
        $this->slideshowName = $slideshowName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlideshowName()
    {
        return $this->slideshowName;
    }

}
