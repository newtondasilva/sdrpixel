<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Author
 *
 * @ORM\Table(name="cont_author")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\AuthorRepository")
 * @Vich\Uploadable
 */
class Author
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\ContentAuthor", mappedBy="author", cascade={"persist","remove"})
     */
    private $contentAuthors;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="curriculum", type="text", nullable=true)
     */
    private $curriculum;



    /**
     * @var string
     *
     * @ORM\Column(name="citationName", type="string", length=255, unique=true)
     */
    private $citationName;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="imageName")

     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $imageSize;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Author
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set citationName.
     *
     * @param string $citationName
     *
     * @return Author
     */
    public function setCitationName($citationName)
    {
        $this->citationName = $citationName;

        return $this;
    }

    /**
     * Get citationName.
     *
     * @return string
     */
    public function getCitationName()
    {
        return $this->citationName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contentAuthors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add contentAuthor.
     *
     * @param \ContentBundle\Entity\ContentAuthor $contentAuthor
     *
     * @return Author
     */
    public function addContentAuthor(\ContentBundle\Entity\ContentAuthor $contentAuthor)
    {
        $this->contentAuthors[] = $contentAuthor;

        return $this;
    }

    /**
     * Remove contentAuthor.
     *
     * @param \ContentBundle\Entity\ContentAuthor $contentAuthor
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContentAuthor(\ContentBundle\Entity\ContentAuthor $contentAuthor)
    {
        return $this->contentAuthors->removeElement($contentAuthor);
    }

    /**
     * Get contentAuthors.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContentAuthors()
    {
        return $this->contentAuthors;
    }

    /**
     * @return string
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }

    /**
     * @param string $curriculum
     */
    public function setCurriculum(string $curriculum): void
    {
        $this->curriculum = $curriculum;
    }

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }


}
