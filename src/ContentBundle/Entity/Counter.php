<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Counter
 *
 * @ORM\Table(name="cont_counter")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\CounterRepository")
 * @Vich\Uploadable()
 */
class Counter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __construct()
    {
        $this->start = new \DateTime();
        $this->lastMonth = new \DateTime();
        $this->lastWeek = new \DateTime();
        $this->lastYear = new \DateTime();
        $this->totalCounter = 0;
        $this->monthCounter = 0;
        $this->yearCounter = 0;
        $this->weekCounter = 0;

    }

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Tag", inversedBy="counters")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id", nullable=true)
     */
    private $tag;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Content", inversedBy="counters")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id", nullable=true)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_week", type="datetime")
     */
    private $lastWeek;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_month", type="datetime")
     */
    private $lastMonth;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_year", type="datetime")
     */
    private $lastYear;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="year_counter", type="integer")
     */
    private $yearCounter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="month_counter", type="integer")
     */
    private $monthCounter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="week_counter", type="integer")
     */
    private $weekCounter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="total_counter", type="integer")
     */
    private $totalCounter;

    /**
     * @return \DateTime
     */
    public function getLastMonth()
    {
        return $this->lastMonth;
    }

    /**
     * @param \DateTime $lastMonth
     */
    public function setLastMonth($lastMonth)
    {
        $this->lastMonth = $lastMonth;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getLastYear()
    {
        return $this->lastYear;
    }

    /**
     * @param \DateTime $lastYear
     */
    public function setLastYear($lastYear)
    {
        $this->lastYear = $lastYear;
    }

    /**
     * @return integer
     */
    public function getYearCounter()
    {
        return $this->yearCounter;
    }

    /**
     * @param integer $yearCounter
     */
    public function setYearCounter($yearCounter)
    {
        $this->yearCounter = $yearCounter;
    }

    /**
     * @return integer
     */
    public function getMonthCounter()
    {
        return $this->monthCounter;
    }

    /**
     * @param integer $monthCounter
     */
    public function setMonthCounter($monthCounter)
    {
        $this->monthCounter = $monthCounter;
    }

    /**
     * @return integer
     */
    public function getWeekCounter()
    {
        return $this->weekCounter;
    }

    /**
     * @param integer $weekCounter
     */
    public function setWeekCounter($weekCounter)
    {
        $this->weekCounter = $weekCounter;
    }

    /**
     * @return integer
     */
    public function getTotalCounter()
    {
        return $this->totalCounter;
    }

    /**
     * @param integer $totalCounter
     */
    public function setTotalCounter($totalCounter)
    {
        $this->totalCounter = $totalCounter;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param integer $num
     *
     * @return Counter
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return int
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set created
     *
     * @param \DateTime $lastWeek
     *
     * @return Counter
     */
    public function setLastWeek($lastWeek)
    {
        $this->lastWeek = $lastWeek;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getLastWeek()
    {
        return $this->lastWeek;
    }

    /**
     * Set tag
     *
     * @param \ContentBundle\Entity\Tag $tag
     *
     * @return Counter
     */
    public function setTag(\ContentBundle\Entity\Tag $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \ContentBundle\Entity\Tag
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set content
     *
     * @param \ContentBundle\Entity\Content $content
     *
     * @return Counter
     */
    public function setContent(\ContentBundle\Entity\Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return \ContentBundle\Entity\Content
     */
    public function getContent()
    {
        return $this->content;
    }
}
