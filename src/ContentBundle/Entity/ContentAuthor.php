<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Author
 *
 * @ORM\Table(name="cont_content_author")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\AuthorRepository")
 */
class ContentAuthor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Content", inversedBy="contentAuthors")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Author", inversedBy="contentAuthors")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->getAuthor()->getCitationName();
    }

    /**
     * Set content.
     *
     * @param \ContentBundle\Entity\Content|null $content
     *
     * @return ContentAuthor
     */
    public function setContent(\ContentBundle\Entity\Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return \ContentBundle\Entity\Content|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set Author.
     *
     * @param \ContentBundle\Entity\Author|null $author
     *
     * @return ContentAuthor
     */
    public function setAuthor(\ContentBundle\Entity\Author $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get Author.
     *
     * @return \ContentBundle\Entity\Author|null
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
