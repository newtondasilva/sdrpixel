<?php

namespace ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;


/**
 * Content
 *
 * @ORM\Table(name="cont_content")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\ContentRepository")
 * @Vich\Uploadable
 */
class Content
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","courseContent","show"})
     */
    private $id;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->slideshows = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contentTags = new ArrayCollection();
        $this->rates = new ArrayCollection();

    }

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseContent", mappedBy="content", cascade={"persist","remove"})
     */
    private $courseContents;

    /**
    * @ORM\OneToMany(targetEntity="ContentBundle\Entity\ContentAuthor", mappedBy="content", cascade={"persist","remove"})
    */
    private $contentAuthors;

    /**
     * @ORM\OneToOne(targetEntity="ContentBundle\Entity\Caso")
     */
    private $caso;


    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\ContentTag", mappedBy="content", cascade={"persist","remove"})
     */
    private $contentTags;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Rate", mappedBy="content", cascade={"persist","remove"})
     */
    private $rates;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="contents")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Serializer\Groups({"default","list"})
     */
    private $createdBy;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Counter", mappedBy="content", cascade={"persist","remove"})
     */
    private $counters;

    /**
     * @ORM\OneToOne(targetEntity="QuizBundle\Entity\Quiz", mappedBy="content")
     */
    private $quiz;


    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Slideshow", mappedBy="content", cascade={"persist","remove"})
     */
    private $slideshows;

    /**
     * @var string
     *
     * @ORM\Column(name="imagem", type="string", length=255, nullable=true)
     * @Serializer\Groups({"default","list"})
     */
    private $imagem;

    const TYPE_AULA = 'aula';
    const TYPE_OUTROS = 'outros';
    const TYPE_IMAGENS = 'imagens_didaticas';
    const TYPE_DISCUSSAOCASO = 'discussao_caso';
    const TYPE_BANCOIMAGEM = 'banco_de_imagem';
    const TYPE_PIXELCRP = 'pixel_crp';

    const TYPE_AULA_LABEL = 'Aula';
    const TYPE_OUTROS_LABEL = 'Outros';
    const TYPE_IMAGENS_LABEL = 'Imagens Didáticas';
    const TYPE_DISCUSSAOCASO_LABEL = 'Caso';
    const TYPE_BANCOIMAGEM_LABEL = 'Banco de imagem';
    const TYPE_PIXELCRPLABEL = 'Pixel CRP';

    const TYPE_AULA_SLUG = 'aula';
    const TYPE_IMAGENS_SLUG = 'imagens-didaticas';
    const TYPE_DISCUSSAOCASO_SLUG = 'caso';
    const TYPE_BANCOIMAGEM_SLUG = 'banco-de-imagem';
    const TYPE_PIXELCRP_SLUG = 'pixel-crp';

    /**
     * @return array
     */
    public static function getTypeChoices(){
        return [
            Content::TYPE_AULA_LABEL => Content::TYPE_AULA,
            Content::TYPE_DISCUSSAOCASO_LABEL => Content::TYPE_DISCUSSAOCASO,
            Content::TYPE_IMAGENS_LABEL => Content::TYPE_IMAGENS,
            Content::TYPE_PIXELCRPLABEL => Content::TYPE_PIXELCRP,
            Content::TYPE_OUTROS_LABEL => Content::TYPE_OUTROS,
        ];
    }

    /**
     * @param $type
     * @return string
     */
    public static function getTypeL($type){
        switch ($type){
            case Content::TYPE_AULA: return Content::TYPE_AULA_LABEL; break;
            case Content::TYPE_BANCOIMAGEM: return Content::TYPE_BANCOIMAGEM_LABEL; break;
            case Content::TYPE_IMAGENS: return Content::TYPE_IMAGENS_LABEL; break;
            case Content::TYPE_PIXELCRP: return Content::TYPE_PIXELCRPLABEL; break;
            case Content::TYPE_DISCUSSAOCASO: return Content::TYPE_DISCUSSAOCASO_LABEL; break;
            case Content::TYPE_OUTROS: return Content::TYPE_OUTROS_LABEL; break;
        }
    }

    public static function getTypeSlug($type){
        switch ($type){
            case Content::TYPE_AULA: return Content::TYPE_AULA_SLUG; break;
            case Content::TYPE_BANCOIMAGEM: return Content::TYPE_BANCOIMAGEM_SLUG; break;
            case Content::TYPE_IMAGENS: return Content::TYPE_IMAGENS_SLUG; break;
            case Content::TYPE_PIXELCRP: return Content::TYPE_PIXELCRP_SLUG; break;
            case Content::TYPE_DISCUSSAOCASO: return Content::TYPE_DISCUSSAOCASO_SLUG; break;
        }
    }

    /**
     * @var string
     * aula
     * discussao_caso
     * banco_imagem
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     * @Serializer\Groups({"default","list","courseContent","show"})
     * @Serializer\SerializedName("title")
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255, nullable=true)
     */
    private $language;


    /**
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     * @Serializer\Groups({"courseContent"})
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="pathVideo", type="text", nullable=true)
     * @Serializer\Groups({"courseContent"})
     * @Serializer\SerializedName("urlVideo")
     */
    private $urlVideo;

    /**
     * @var string
     *
     * @ORM\Column(name="nid", type="integer", nullable=true, unique=false)
     */
    private $nid;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="filename")
     *
     * @var File
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $fileName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt ;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="videoname")
     *
     * @var File
     */
    private $video;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"default","list","courseContent"})
     * @Serializer\SerializedName("videoPath")
     * @var string
     */
    private $videoName;



    /**
     * If manually uploading a video (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedVideo' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'Video' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return
     */
    public function setVideo(File $video = null)
    {
        $this->video = $video;

        if ($video) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @param string $videoName
     *
     * @return PlanesPageSlideshow
     */
    public function setVideoName($videoName)
    {
        $this->videoName = $videoName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVideoName()
    {
        return $this->videoName;
    }


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return PlanesPageSlideshow
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $fileName
     *
     * @return PlanesPageSlideshow
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Content
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Content
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    public function getVideoId(){
        $video = str_replace('https://www.youtube.com/embed/','',$this->urlVideo);

        return $video;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Content
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return bool
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Content
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Content
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Content
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set quiz
     *
     * @param \QuizBundle\Entity\Quiz $quiz
     *
     * @return Content
     */
    public function setQuiz(\QuizBundle\Entity\Quiz $quiz = null)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Get quiz
     *
     * @return \QuizBundle\Entity\Quiz
     */
    public function getQuiz()
    {
        return $this->quiz;
    }


    /**
     * Add slideshow
     *
     * @param \ContentBundle\Entity\Slideshow $slideshow
     *
     * @return Content
     */
    public function addSlideshow(\ContentBundle\Entity\Slideshow $slideshow)
    {
        $this->slideshows[] = $slideshow;

        return $this;
    }

    /**
     * Remove slideshow
     *
     * @param \ContentBundle\Entity\Slideshow $slideshow
     */
    public function removeSlideshow(\ContentBundle\Entity\Slideshow $slideshow)
    {
        $this->slideshows->removeElement($slideshow);
    }

    /**
     * Get slideshows
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlideshows()
    {
        return $this->slideshows;
    }

    /**
     * Add contentTag
     *
     * @param \ContentBundle\Entity\ContentTag $contentTag
     *
     * @return Content
     */
    public function addContentTag(\ContentBundle\Entity\ContentTag $contentTag)
    {
        $this->contentTags[] = $contentTag;

        return $this;
    }

    /**
     * Remove contentTag
     *
     * @param \ContentBundle\Entity\ContentTag $contentTag
     */
    public function removeContentTag(\ContentBundle\Entity\ContentTag $contentTag)
    {
        $this->contentTags->removeElement($contentTag);
    }

    /**
     * Get contentTags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContentTags()
    {
        return $this->contentTags;
    }

    /**
     * @param $type
     * @return ArrayCollection
     */
    public function getTagsByType($type){
        $result = new ArrayCollection();

        /** @var ContentTag $contentTag */
        foreach ($this->getContentTags() as $contentTag){

            if($contentTag->getTag() and $contentTag->getTag()->getTagType()->getId() == $type){
                $result->add($contentTag);
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getImagem($absolute = true)
    {
        if($this->imagem) {
            $this->imagem = str_replace('public://',
                'sites/fcm.unicamp.br.drpixel/files/',$this->imagem);
        }



        return $this->imagem;
    }

    /**
     * @param string $imagem
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getUrlVideo()
    {
        return $this->urlVideo;
    }

    /**
     * @param string $urlVideo
     */
    public function setUrlVideo($urlVideo)
    {
        $this->urlVideo = $urlVideo;
    }

    /**
     * @return string
     */
    public function getNid()
    {
        return $this->nid;
    }

    /**
     * @param string $nid
     */
    public function setNid($nid)
    {
        $this->nid = $nid;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function getTypeLabel(){
        return Content::getTypeL($this->getType());
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Add counter
     *
     * @param \ContentBundle\Entity\Counter $counter
     *
     * @return Content
     */
    public function addCounter(\ContentBundle\Entity\Counter $counter)
    {
        $this->counters[] = $counter;

        return $this;
    }

    /**
     * Remove counter
     *
     * @param \ContentBundle\Entity\Counter $counter
     */
    public function removeCounter(\ContentBundle\Entity\Counter $counter)
    {
        $this->counters->removeElement($counter);
    }

    /**
     * Get counters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCounters()
    {
        return $this->counters;
    }

    /**
     * @param $type
     * @return ArrayCollection
     */
    public function getContentTagsByType($type){
        $tags = new ArrayCollection();
        /** @var ContentTag $contentTag */
        foreach ($this->getContentTags() as $contentTag){

            if($contentTag->getTag() and  $contentTag->getTag()->getTagType()->getId() == $type){
                $tags->add($contentTag->getTag());
            }
        }

        return $tags;
    }

    /**
     * Set content.
     *
     * @param \ContentBundle\Entity\Caso|null $caso
     *
     * @return Content
     */
    public function setCaso(\ContentBundle\Entity\Caso $caso = null)
    {
        $this->caso = $caso;

        return $this;
    }

    /**
     * Get content.
     *
     * @return \ContentBundle\Entity\Caso|null
     */
    public function getCaso()
    {
        return $this->caso;
    }

    /**
     * Add rate.
     *
     * @param \ContentBundle\Entity\Rate $rate
     *
     * @return Content
     */
    public function addRate(\ContentBundle\Entity\Rate $rate)
    {
        $this->rates[] = $rate;

        return $this;
    }

    /**
     * Remove rate.
     *
     * @param \ContentBundle\Entity\Rate $rate
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRate(\ContentBundle\Entity\Rate $rate)
    {
        return $this->rates->removeElement($rate);
    }

    /**
     * Get rates.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Content
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * Set courseContents.
     *
     * @param \ContentBundle\Entity\CourseContent|null $courseContents
     *
     * @return Content
     */
    public function setCourseContents(\ContentBundle\Entity\CourseContent $courseContents = null)
    {
        $this->courseContents = $courseContents;

        return $this;
    }

    /**
     * Get courseContents.
     *
     * @return \ContentBundle\Entity\CourseContent|null
     */
    public function getCourseContents()
    {
        return $this->courseContents;
    }

    /**
     * Add courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent $courseContent
     *
     * @return Content
     */
    public function addCourseContent(\ContentBundle\Entity\CourseContent $courseContent)
    {
        $this->courseContents[] = $courseContent;

        return $this;
    }

    /**
     * Remove courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent $courseContent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseContent(\ContentBundle\Entity\CourseContent $courseContent)
    {
        return $this->courseContents->removeElement($courseContent);
    }

    /**
     * Add contentAuthor.
     *
     * @param \ContentBundle\Entity\ContentAuthor $contentAuthor
     *
     * @return Content
     */
    public function addContentAuthor(\ContentBundle\Entity\ContentAuthor $contentAuthor)
    {
        $this->contentAuthors[] = $contentAuthor;

        return $this;
    }

    /**
     * Remove contentAuthor.
     *
     * @param \ContentBundle\Entity\ContentAuthor $contentAuthor
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContentAuthor(\ContentBundle\Entity\ContentAuthor $contentAuthor)
    {
        return $this->contentAuthors->removeElement($contentAuthor);
    }

    /**
     * Get contentAuthors.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContentAuthors()
    {
        return $this->contentAuthors;
    }
}
