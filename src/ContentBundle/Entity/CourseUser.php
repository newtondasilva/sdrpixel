<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * CourseUser
 *
 * @ORM\Table(name="course_user")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\CourseUserRepository")
 * @Vich\Uploadable
 */
class CourseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseUser","courseContent"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="courseUsers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Serializer\Groups({"default","list","show","courseUser","courseContent"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Course", inversedBy="courseUsers")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * @Serializer\Groups({"default","list","show","courseUser"})
     */
    private $course;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseProgress", mappedBy="courseUser",cascade={"remove","persist"})
     * @Serializer\Groups({"default","list","show","courseUser"})
     * @Serializer\SerializedName("progresses")
     */
    private $courseProgresses;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Serializer\Groups({"default","list","show","courseUser"})
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     * @Serializer\Groups({"default"})
     *
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     * @Serializer\Groups({"default"})
     */
    private $status;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="imageName")

     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"default","list","show","courseUser","courseContent"})
     * @Serializer\SerializedName("thumb")
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    const STATUS_CANCELADO_LABEL = 'Cancelado';
    const STATUS_PENDENTE_LABEL = 'Pendente';
    const STATUS_SOLICITADO_LABEL = 'Solicitado';
    const STATUS_CONCLUIDO_LABEL = 'Concluído';
    const STATUS_CANCELADO = -1;
    const STATUS_PENDENTE = 0;
    const STATUS_SOLICITADO = 1;
    const STATUS_CONCLUIDO = 2;


    const TYPE_PADRAO = 0;
    const TYPE_EXTECAMP = 1;
    const TYPE_FCM = 3;
    const TYPE_PADRAO_LABEL = 'Padrão';
    const TYPE_EXTECAMP_LABEL = 'Extecamp';
    const TYPE_FCM_LABEL = 'FCM';

    public static function getTypeChoices(){
        return [
            self::TYPE_PADRAO_LABEL => self::TYPE_PADRAO,
            self::TYPE_EXTECAMP_LABEL => self::TYPE_EXTECAMP,
            self::TYPE_FCM_LABEL => self::TYPE_FCM,
        ];
    }

    public function getTypeLabel(){
        switch ($this->getType()){
            case self::TYPE_PADRAO: return self::TYPE_PADRAO_LABEL; break;
            case self::TYPE_EXTECAMP: return self::TYPE_EXTECAMP_LABEL; break;
            case self::TYPE_FCM: return self::TYPE_FCM_LABEL; break;

        }
    }

    public static function getStatusChoices(){
        return [
            self::STATUS_CANCELADO_LABEL => self::STATUS_CANCELADO,
            self::STATUS_PENDENTE_LABEL => self::STATUS_PENDENTE,
            self::STATUS_SOLICITADO_LABEL => self::STATUS_SOLICITADO,
            self::STATUS_CONCLUIDO_LABEL => self::STATUS_CONCLUIDO,
        ];
    }

    public function getStatusLabel(){
        switch ($this->getStatus()){
            case self::STATUS_CANCELADO: return self::STATUS_CANCELADO_LABEL; break;
            case self::STATUS_PENDENTE: return self::STATUS_PENDENTE_LABEL; break;
            case self::STATUS_CONCLUIDO: return self::STATUS_CONCLUIDO_LABEL; break;
            case self::STATUS_SOLICITADO: return self::STATUS_SOLICITADO_LABEL; break;

        }
    }



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return CourseUser
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return CourseUser
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set course.
     *
     * @param \ContentBundle\Entity\Course|null $course
     *
     * @return CourseUser
     */
    public function setCourse(\ContentBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course.
     *
     * @return \ContentBundle\Entity\Course|null
     */
    public function getCourse()
    {
        return $this->course;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->courseProgresses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add courseProgress.
     *
     * @param \ContentBundle\Entity\CourseProgress $courseProgress
     *
     * @return CourseUser
     */
    public function addCourseProgress(\ContentBundle\Entity\CourseProgress $courseProgress)
    {
        $this->courseProgresses[] = $courseProgress;

        return $this;
    }

    /**
     * Remove courseProgress.
     *
     * @param \ContentBundle\Entity\CourseProgress $courseProgress
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseProgress(\ContentBundle\Entity\CourseProgress $courseProgress)
    {
        return $this->courseProgresses->removeElement($courseProgress);
    }

    /**
     * Get courseProgresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseProgresses()
    {
        return $this->courseProgresses;
    }

    public function getCourseStatus(){
        $status = [
            CourseProgress::STATUS_INICIADO => 0,
            CourseProgress::STATUS_CONCLUIDO => 0,
            CourseProgress::STATUS_PENDENTE => 0,
        ];

        /** @var CourseProgress $courseProgress */
        foreach ($this->getCourseProgresses() as $courseProgress){
            if($courseProgress->getStatus() == 'null'){
                $statusL = CourseProgress::STATUS_INICIADO;
            } else {
                $statusL = $courseProgress->getStatus();
            }

            $status[$statusL]++;
        }

        return $status;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

}
