<?php

namespace ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Course
 *
 * @ORM\Table(name="cont_course")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\CourseRepository")
 * Serializer\ExclusionPolicy("all")
 * @Vich\Uploadable
 */
class Course
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * Serializer\Expose()
     * @Serializer\Groups({"default","list","show","courseContent","courseUser"})
     */
    private $id;



    const VIS_PUBLICO_LABEL = 'Público';
    const VIS_ALUNO_LABEL = 'Aluno';
    const VIS_PRIVADO_LABEL = 'Privado';
    const VIS_EDRPIXEL_LABEL = 'E-Drpixel';
    const VIS_PUBLICO = 0;
    const VIS_ALUNO = 1;
    const VIS_PRIVADO = 2;
    const VIS_EDRPIXEL = 3;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseSection", mappedBy="course", cascade={"persist","remove"})
     * @ORM\OrderBy({"delta" = "ASC"})
     * @Serializer\Groups({"default","show","courseUser","courseSection"})
     * @Serializer\SerializedName("sections")
     */
    private $courseSections;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseContent", mappedBy="course", cascade={"persist","remove"})
     */
    private $courseContents;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseUser", mappedBy="course", cascade={"persist","remove"})
     */
    private $courseUsers;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=false)
     * @Serializer\Groups({"default","list","show","courseUser","courseContent"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="resumo", type="text",nullable=false)
     * @Serializer\Groups({"default","show","list"})
     * @Serializer\SerializedName("body")
     *
     */
    private $resumo;

    /**
     * @var string
     *
     * @ORM\Column(name="certificado", type="text", nullable=true)
     */
    private $certificado;

    /**
     * @var string
     *
     * @ORM\Column(name="summary", type="text",nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent","courseUser"})
     * @Serializer\SerializedName("summary")
     */
    private $summary;

    /**
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param string $summary
     */
    public function setSummary($summary): void
    {
        $this->summary = $summary;
    }

    /**
     * @return string
     */
    public function getCertificado()
    {
        return $this->certificado;
    }

    /**
     * @param string $certificado
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;
    }

    /**
     * @return string
     */
    public function getResumo()
    {
        return $this->resumo;
    }

    /**
     * @param string $resumo
     */
    public function setResumo($resumo)
    {
        $this->resumo = $resumo;
    }

    /**
     * @var bool
     *
     * @ORM\Column(name="published", type="boolean")
     * @Serializer\Groups({"default","list","show"})
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="termo", type="text", nullable=true)
     */
    private $termo;

    /**
     * @var int
     *
     * @ORM\Column(name="visibility", type="smallint")
     */
    private $visibility;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="courses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="imageName", size="imageSize")

     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"default","list","show","courseUser","courseContent"})
     * @Serializer\SerializedName("thumb")
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Course
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set published.
     *
     * @param bool $published
     *
     * @return Course
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published.
     *
     * @return bool
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Course
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set termo.
     *
     * @param string $termo
     *
     * @return Course
     */
    public function setTermo($termo)
    {
        $this->termo = $termo;

        return $this;
    }

    /**
     * Get termo.
     *
     * @return string
     */
    public function getTermo()
    {
        return $this->termo;
    }

    /**
     * Set visibility.
     *
     * @param int $visibility
     *
     * @return Course
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility.
     *
     * @return int
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set courseContents.
     *
     * @param \ContentBundle\Entity\CourseContent|null $courseContents
     *
     * @return Course
     */
    public function setCourseContents(\ContentBundle\Entity\CourseContent $courseContents = null)
    {
        $this->courseContents = $courseContents;

        return $this;
    }

    /**
     * Get courseContents.
     *
     * @return ArrayCollection
     */
    public function getCourseContents()
    {
        return $this->courseContents;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->courseContents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->courseSections = new ArrayCollection();
    }

    /**
     * Add courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent $courseContent
     *
     * @return Course
     */
    public function addCourseContent(\ContentBundle\Entity\CourseContent $courseContent)
    {
        $this->courseContents[] = $courseContent;

        return $this;
    }

    /**
     * Remove courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent $courseContent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseContent(\ContentBundle\Entity\CourseContent $courseContent)
    {
        return $this->courseContents->removeElement($courseContent);
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User|null $createdBy
     *
     * @return Course
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @return array
     */
    public static function getVisibilityChoices(){
        return [
            Course::VIS_PUBLICO_LABEL => Course::VIS_PUBLICO,
            Course::VIS_ALUNO_LABEL => Course::VIS_ALUNO,
            Course::VIS_PRIVADO_LABEL => Course::VIS_PRIVADO,
            self::VIS_EDRPIXEL_LABEL => self::VIS_EDRPIXEL
        ];
    }

    public function getVisibilityLabel(){
        switch ($this->getVisibility()){
            case self::VIS_ALUNO: return self::VIS_ALUNO_LABEL; break;
            case self::VIS_EDRPIXEL: return self::VIS_EDRPIXEL_LABEL; break;
            case self::VIS_PRIVADO: return self::VIS_PRIVADO_LABEL; break;
            case self::VIS_PUBLICO: return self::VIS_PUBLICO_LABEL; break;
        }
    }

    /**
     * Add courseSection.
     *
     * @param \ContentBundle\Entity\CourseSection $courseSection
     *
     * @return Course
     */
    public function addCourseSection(\ContentBundle\Entity\CourseSection $courseSection)
    {
        $this->courseSections[] = $courseSection;

        return $this;
    }

    /**
     * Remove courseSection.
     *
     * @param \ContentBundle\Entity\CourseSection $courseSection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseSection(\ContentBundle\Entity\CourseSection $courseSection)
    {
        return $this->courseSections->removeElement($courseSection);
    }

    /**
     * Get courseSections.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseSections()
    {
        return $this->courseSections;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return Course
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add courseSection.
     *
     * @param \ContentBundle\Entity\CourseSection $courseSection
     *
     * @return Course
     */
    public function addCourseUser(CourseUser $courseUser)
    {
        $this->courseUsers[] = $courseUser;

        return $this;
    }

    /**
     * Remove courseUser.
     *
     * @param \ContentBundle\Entity\CourseUser $courseUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseUser(\ContentBundle\Entity\CourseUser $courseUser)
    {
        return $this->courseUsers->removeElement($courseUser);
    }

    /**
     * Get courseUsers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseUsers()
    {
        return $this->courseUsers;
    }
}
