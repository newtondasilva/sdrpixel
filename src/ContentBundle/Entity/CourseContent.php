<?php

namespace ContentBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CourseContent
 *
 * @ORM\Table(name="cont_course_content")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\CourseContentRepository")
 */
class CourseContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseContent","courseUser"})
     */
    private $id;

    public function __construct()
    {
        $this->courseProgresses = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseProgress", mappedBy="courseContent", cascade={"persist","remove"})
     */
    private $courseProgresses;

    /**
     * @var string
     *
     * @ORM\Column(name="delta", type="smallint")
     * @Serializer\Groups({"default","list","show","courseContentShow","courseContent","courseUser"})
     */
    private $delta;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Content", inversedBy="courseContents")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id", nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent","courseUser"})
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\CourseSection", inversedBy="courseContents")
     * @ORM\JoinColumn(name="section_id", referencedColumnName="id", nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent"})
     * @Serializer\SerializedName("courseSection")
     */
    private $courseSection;

    /**
     * @ORM\ManyToOne(targetEntity="QuizBundle\Entity\Quiz", inversedBy="courseContents")
     * @ORM\JoinColumn(name="quiz_id", referencedColumnName="id", nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent","courseUser","courseContentShow"})
     * @Serializer\SerializedName("quiz")
     */
    private $quiz;


    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Course", inversedBy="courseContents")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=true)
     */
    private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="chamada", type="text",nullable=true)
     * @Serializer\Groups({"default","list"})
     */
    private $chamada;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string",nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent","courseUser"})
     */
    private $title;

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle(){
        return $this->title;
    }

    /**
     * @return string
     */
    public function getChamada()
    {
        return $this->chamada;
    }

    /**
     * @param string $chamada
     */
    public function setChamada($chamada)
    {
        $this->chamada = $chamada;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set delta.
     *
     * @param string $delta
     *
     * @return CourseContent
     */
    public function setDelta($delta)
    {
        $this->delta = $delta;

        return $this;
    }

    /**
     * Get delta.
     *
     * @return string
     */
    public function getDelta()
    {
        return $this->delta;
    }

    /**
     * Set content.
     *
     * @param \ContentBundle\Entity\Content|null $content
     *
     * @return CourseContent
     */
    public function setContent(\ContentBundle\Entity\Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return \ContentBundle\Entity\Content|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set course.
     *
     * @param \ContentBundle\Entity\Course|null $course
     *
     * @return CourseContent
     */
    public function setCourse(\ContentBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course.
     *
     * @return Course|null
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set quiz.
     *
     * @param \QuizBundle\Entity\Quiz|null $quiz
     *
     * @return CourseContent
     */
    public function setQuiz(\QuizBundle\Entity\Quiz $quiz = null)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Get quiz.
     *
     * @return \QuizBundle\Entity\Quiz|null
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * Add courseProgress.
     *
     * @param \ContentBundle\Entity\CourseProgress $courseProgress
     *
     * @return CourseContent
     */
    public function addCourseProgress(\ContentBundle\Entity\CourseProgress $courseProgress)
    {
        $this->courseProgresses[] = $courseProgress;

        return $this;
    }

    /**
     * Remove courseProgress.
     *
     * @param \ContentBundle\Entity\CourseProgress $courseProgress
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseProgress(\ContentBundle\Entity\CourseProgress $courseProgress)
    {
        return $this->courseProgresses->removeElement($courseProgress);
    }

    /**
     * Get courseProgresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseProgresses()
    {
        return $this->courseProgresses;
    }

    /**
     * @param User $user
     * @return string
     */
    public function getUserStatus(User $user){

        /** @var CourseProgress $courseProgress */
        foreach ($this->getCourseProgresses() as $courseProgress){
            if($courseProgress->getCreatedBy() and $courseProgress->getCreatedBy()->getId() == $user->getId()){
                return $courseProgress->getStatus();
            }
        }
    }

    /**
     * Obtem progresso do usuario para um dado modulo
     * @param User $user
     * @return CourseProgress
     */
    public function getCourseProgress(User $user = null){
        if(!$user instanceof User){
            return null;
        }

        /** @var CourseProgress $courseProgress */
        foreach ($this->getCourseProgresses() as $courseProgress){
            if($courseProgress->getCreatedBy() and $courseProgress->getCreatedBy()->getId() == $user->getId()){
                return $courseProgress;
            }
        }

        return null;
    }

    /**
     * Verifica se todos os quiz utilizados no curso foram concluidos
     * @param CourseProgress $courseProgress
     */
    public function allQuizFromCourseCompleted(CourseProgress $courseProgress){

        if(!$courseProgress->getCourseContent()->getQuiz()) return true;

        /** @var CourseContent $courseContent */
        foreach ($courseProgress->getCourseContent()->getCourse()->getCourseContents() as $courseContent){
            /** Se eh conteudo com quiz e eh o mesmo quiz usado no progresso */
            if($courseContent->getQuiz())
                if($courseContent->getQuiz()->getId() == $courseProgress->getCourseContent()->getQuiz()->getId()) {

                    if ($courseProgressRow = $courseContent->getCourseProgress($courseProgress->getCreatedBy())) {
                        if ($courseProgressRow->getStatus() != CourseProgress::STATUS_CONCLUIDO) return false;
                    }
            }
        }

        return true;
    }

    /**
     * Set courseSection.
     *
     * @param \ContentBundle\Entity\CourseSection|null $courseSection
     *
     * @return CourseContent
     */
    public function setCourseSection(\ContentBundle\Entity\CourseSection $courseSection = null)
    {
        $this->courseSection = $courseSection;

        return $this;
    }

    /**
     * Get courseSection.
     *
     * @return \ContentBundle\Entity\CourseSection|null
     */
    public function getCourseSection()
    {
        return $this->courseSection;
    }

    public function getNome(){
        return $this->getCourseSection()->getDelta().'.'.$this->getDelta() . '. ' . $this->getTitle();
    }

}
