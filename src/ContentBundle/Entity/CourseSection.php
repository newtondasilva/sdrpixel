<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ContentSection
 *
 * @ORM\Table(name="cont_course_section")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\CourseSectionRepository")
 */
class CourseSection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseContent","courseUser","courseSection"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Course", inversedBy="courseSections")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=true)
     * @Serializer\Groups({"default","list","courseContent"})
     */
    private $course;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseContent", mappedBy="courseSection", cascade={"persist","remove"})
     * @ORM\OrderBy({"delta" = "ASC"})
     * @Serializer\Groups({"default","list","show","courseUser","courseSection"})
     * @Serializer\SerializedName("courseContents")
     */
    private $courseContents;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Serializer\Groups({"default","list","show","courseUser","courseSection"})
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="delta", type="smallint")
     * @Serializer\Groups({"default","list","show","courseUser"})
     */
    private $delta;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return CourseSection
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set delta.
     *
     * @param int $delta
     *
     * @return CourseSection
     */
    public function setDelta($delta)
    {
        $this->delta = $delta;

        return $this;
    }

    /**
     * Get delta.
     *
     * @return int
     */
    public function getDelta()
    {
        return $this->delta;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->courseContents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set course.
     *
     * @param \ContentBundle\Entity\Course|null $course
     *
     * @return CourseSection
     */
    public function setCourse(\ContentBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course.
     *
     * @return \ContentBundle\Entity\Course|null
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Add courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent $courseContent
     *
     * @return CourseSection
     */
    public function addCourseContent(\ContentBundle\Entity\CourseContent $courseContent)
    {
        $this->courseContents[] = $courseContent;

        return $this;
    }

    /**
     * Remove courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent $courseContent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseContent(\ContentBundle\Entity\CourseContent $courseContent)
    {
        return $this->courseContents->removeElement($courseContent);
    }

    /**
     * Get courseContents.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseContents()
    {
        return $this->courseContents;
    }

    public function getNome(){
        return $this->getDelta() . '. ' . $this->getTitle();
    }
}
