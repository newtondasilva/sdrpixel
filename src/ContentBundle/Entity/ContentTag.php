<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * ContentTag
 *
 *
 * @ORM\Table(name="content_tag")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\ContentTagRepository")
 * @Vich\Uploadable()
 */
class ContentTag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Content", inversedBy="contentTags")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\Tag", inversedBy="contentTags")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     */
    private $tag;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param \ContentBundle\Entity\Content $content
     *
     * @return ContentTag
     */
    public function setContent(\ContentBundle\Entity\Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return \ContentBundle\Entity\Content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set tag
     *
     * @param \ContentBundle\Entity\Tag $tag
     *
     * @return ContentTag
     */
    public function setTag(\ContentBundle\Entity\Tag $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \ContentBundle\Entity\Tag
     */
    public function getTag()
    {
        return $this->tag;
    }
}
