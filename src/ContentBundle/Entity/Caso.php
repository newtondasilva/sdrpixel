<?php

namespace ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Caso
 *
 * @ORM\Table(name="cont_caso")
 * @ORM\Entity(repositoryClass="ContentBundle\Repository\CasoRepository")
 * @Vich\Uploadable()
 */
class Caso
{
    /**
     * @ORM\OneToOne(targetEntity="ContentBundle\Entity\Content", mappedBy="caso",cascade={"persist"})
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id",onDelete="SET NULL")
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="resume", type="string", length=255)
     */
    private $resume;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Caso
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set resume.
     *
     * @param string $resume
     *
     * @return Caso
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume.
     *
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set content.
     *
     * @param \ContentBundle\Entity\Content|null $content
     *
     * @return Caso
     */
    public function setContent(\ContentBundle\Entity\Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return \ContentBundle\Entity\Content|null
     */
    public function getContent()
    {
        return $this->content;
    }
}
