
    new Vue({
        el: '#app',
        watch: {
            bottom(bottom) {
                if (bottom) {
                    this.addcontent()
                }
            }
        },
        data() {
            return {
                bottom: false,
                step: 0,
                contents: []
            }
        },
        methods: {

            bottomVisible() {
                const scrollY = window.scrollY
                const visible = document.documentElement.clientHeight
                const pageHeight = document.documentElement.scrollHeight
                const bottomOfPage = visible + scrollY >= pageHeight
                return bottomOfPage || pageHeight < visible
            },
            addcontent() {
                axios.get('ajax/content/full', {
                    params: {
                        step: this.step,
                        type: 'aula',
                    }
                })
                    .then(response => {
                        this.step++;
                        this.contents.push(response.data)
                        if (this.bottomVisible()) {
                            this.addcontent()
                        }
                    })

            }
        },
        created() {
            window.addEventListener('scroll', () => {
                this.bottom = this.bottomVisible()
            })
            this.addcontent()
        }
    });;