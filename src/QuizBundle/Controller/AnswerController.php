<?php

namespace QuizBundle\Controller;

use AppBundle\Entity\User;
use QuizBundle\Entity\Answer;
use QuizBundle\Entity\AnswerItem;
use QuizBundle\Entity\Question;
use QuizBundle\Entity\QuestionItem;
use QuizBundle\Entity\Quiz;
use QuizBundle\Form\AnswerItemType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Answer controller.
 *
 * @Route("quiz/{quiz}/answer")
 */
class AnswerController extends Controller
{
    /**
     * Creates a new answer entity.
     *
     * @Route("/{answer}/questionItem/{questionItem}/ajax", name="ajax_answer_prequiz", options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function ajaxPreQuizAction(Request $request, QuestionItem $questionItem, Quiz $quiz, Answer $answer){

        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();


        // Localiza answerItem de prequiz
        $result = $em->getRepository('QuizBundle:AnswerItem')->createQueryBuilder('a')
            ->join('a.answer', 'd')
            ->join('a.questionItem','b')
            ->join('b.question', 'c')
            ->andWhere('c.id = :id')->setParameter('id',$questionItem->getQuestion()->getId())
            ->andWhere('d.user = :user')->setParameter('user',$user->getId())
            ->andWhere('d.id = :answer')->setParameter('answer',$answer->getId())
            ->andWhere('a.type = :type')->setParameter('type',$request->get('type'))
            ->getQuery()->getResult();


        if(!$result){
            $answerItem = new AnswerItem();
        } else {
            $answerItem = $result[0];
        }

        $answerItem->setCreated(new \DateTime());
        $answerItem->setTime(0);
        $answerItem->setType($request->get('type'));
        $answerItem->setQuestionItem($questionItem);
        $answerItem->setAnswer($answer);
        $em->persist($answerItem);
        $em->flush();


        return new JsonResponse(true);
    }

    /**
     * Creates a new answer entity.
     *
     * @Route("", name="quiz_answer_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request, Quiz $quiz){

        return $this->render('QuizBundle:Answer:index.html.twig',[
            'page_title' => 'Respostas',
            'quiz' => $quiz,
            'back' => $this->generateUrl('admin_quiz_index',['id' => $quiz->getId()])
        ]);
    }

    /**
     * STEP 0 - Orientacoes  e ponto de entrada
     *
     * @Route("/start", name="quiz_answer_start")
     * @Method({"GET", "POST"})
     */
    public function startAction(Request $request, Quiz $quiz)
    {
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $answer = new Answer();
            $answer->setCreated(new \DateTime());

            if($user = $this->getUser()){
                $answer->setUser($user);
            }

            $answer->setQuiz($quiz);
            $this->getDoctrine()->getManager()->persist($answer);
            $this->getDoctrine()->getManager()->flush();

            if ($quiz->getPreQuiz()) {
                return $this->redirectToRoute('quiz_answer_prequiz', [
                    'quiz' => $quiz->getId(),
                    'answer' => $answer->getId(),
                    'back' => $request->getRequestUri(),
                ]);
            } else {
                return $this->redirectToRoute('quiz_answer_content', [
                    'quiz' => $quiz->getId(),
                    'answer' => $answer->getId(),
                ]);
            }
        }

        return $this->render('Quiz/Answer/start.html.twig',[
            'page_title' => $quiz->getTitle() . ' - Orientações',
            'step' => 0,
            'form' => $form->createView(),
            'quiz' => $quiz,
            'back' => $this->generateUrl('home'),
        ]);
    }

    /**
     * STEP 1 - pre quiz
     *
     * @Route("/{answer}/prequiz", name="quiz_answer_prequiz")
     * @Method({"GET", "POST"})
     */
    public function preQuizAction(Request $request, Quiz $quiz, Answer $answer)
    {

        return $this->render('Quiz/Answer/prequiz.html.twig', [
            'page_title' => $quiz->getTitle() . ' - Questões pré-teste',
            'answer' => $answer,
            'step' => 1,
            'back' => $request->get('back'),
            'quiz' => $quiz,
        ]);
    }


    /**
     * STEP 2 - Conteudo
     *
     * @Route("/{answer}/content", name="quiz_answer_content")
     * @Method({"GET", "POST"})
     */
    public function contentAction(Request $request, Quiz $quiz, Answer $answer){
        $form = $this->createForm('QuizBundle\Form\AnswerType', $answer);
        $form->handleRequest($request);

        //Verifica se possui conteudo
        if(!$quiz->getContent()){
            return $this->redirectToRoute('quiz_answer_question', array(
                'quiz' => $quiz->getId(),
                'answer' => $answer->getId(),
                'question' => $answer->getNextQuestion()->getId(),
            ));
        }


        if ($form->isSubmitted() && $form->isValid()) {

            return $this->redirectToRoute('quiz_answer_question', array(
                'quiz' => $quiz->getId(),
                'answer' => $answer->getId(),
                'question' => $answer->getNextQuestion()->getId(),
            ));
        }

        $actions['edit'] =[
                'path' => $this->generateUrl('admin_quiz_edit',['id' => $quiz->getId()]),
                'label' => 'Editar',
                'role' => 'ROLE_ADMIN'
            ];

        if($quiz->getContent()){
            $actions['content'] =[
                'path' => $this->generateUrl('admin_content_edit',['id' => $quiz->getContent()->getId()]),
                'label' => 'Editar conteúdo',
                'role' => 'ROLE_ADMIN'
            ];
        }

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('home');
        }


        return $this->render('Quiz/Answer/content.html.twig', array(
            'page_title' => $quiz->getTitle(),
            'back' => $back,
            'actions' => $actions,
            'answer' => $answer,
            'quiz' => $quiz,
            'step' => 2,
            'form' => $form->createView(),
        ));
    }



    /**
     * STEP 3 - Questoes
     *
     * @Route("/{answer}/question/{question}", name="quiz_answer_question")
     * @Method({"GET","POST"})
     */
    public function questionAction(Request $request, Answer $answer, Quiz $quiz, \QuizBundle\Entity\Question $question)
    {
        $pos = $quiz->getQuestions()->indexOf($question) + 1;

        $answerItem = new AnswerItem();
        $answerItem->setAnswer($answer);

        $form = $this->createFormBuilder()
            ->add('answer',ChoiceType::class,[
                'label' => 'Resposta',
                'choices' => $question->getItens(),
                'choice_label' => 'text',
                'choice_value' => 'id',
                'expanded' => true,

            ])
            ->add('time',TextType::class)
            ->getForm();

        if($pos > 2){
            /** @var Question $question */
            $question = $quiz->getQuestions()->get($pos -1);
            $back = $this->generateUrl('quiz_answer_question',[
                'question' => $question->getId(),
                'answer' => $answer->getId(),
                'quiz' => $quiz->getId(),
            ]);
        } else {
            $back = $this->generateUrl('quiz_answer_content',[
                'quiz' => $quiz->getId(),
                'answer' => $answer->getId(),
            ]);
        }

        $form->handleRequest($request);

        if($form->isSubmitted()){
            $questionItemId = $request->get('form')['answer'];
            $time = $request->get('form')['time'];

            $questionItem = $this->getDoctrine()->getRepository('QuizBundle:QuestionItem')->find($questionItemId);
            $answerItem->setQuestionItem($questionItem);
            $answerItem->setCreated(new \DateTime());
            $answerItem->setType(AnswerItem::T_NORMAL);
            $answerItem->setTime($time);


            $this->getDoctrine()->getManager()->persist($answerItem);
            $this->getDoctrine()->getManager()->flush($answerItem);

            if($nextQuestion = $answer->getNextQuestion()){
                // Proxima questao
                return $this->redirectToRoute('quiz_answer_question', [
                    'answer'=> $answer->getId(),
                    'question' =>$nextQuestion->getId(),
                    'quiz' => $quiz->getId(),
                    'back' => $request->getRequestUri(),
                ]);
            } else {
                // Nao ha proxima questao
                $this->addFlash('success', 'Teste finalizado');
                //Exibe feedback
                if($quiz->getFeedback()){
                    return $this->redirectToRoute('quiz_answer_feedback',[
                        'quiz' => $quiz->getId(),
                        'answer' => $answer->getId(),
                    ]);
                } else {
                    return $this->redirectToRoute('home');
                }

            }
        }

        return $this->render('Quiz/Answer/question.html.twig', array(
            'page_title' => $quiz->getTitle() . ' - Questão ' . $pos . ' de ' . $quiz->getQuestions()->count(),
            'question' => $question,
            'questionNumber' => $pos,
            'step' => 3,
            'questionTotal' => $quiz->getQuestions()->count(),
            'quiz' => $quiz,
            'back' => $back,
            'form' => $form->createView(),
            'answer' => $answer,
        ));
    }

    /**
     * STEP 4 - Feedback
     *
     * @Route("/{answer}/feedback", name="quiz_answer_feedback")
     * @Method({"GET", "POST"})
     */
    public function feedbackAction(Request $request, Quiz $quiz,Answer $answer)
    {
        return $this->render('Quiz/Answer/feedback.html.twig', [
            'page_title' => $quiz->getTitle() . ' - Feedback',
            'step' => 4,
            "answer" => $answer,
            'back' => $request->get('back'),
            'quiz' => $quiz,
        ]);
    }

    /**
     * STEP 5 - Resultado
     *
     * @Route("/{answer}/finish", name="quiz_answer_finish")
     * @Method({"GET", "POST"})
     */
    public function finishAction(Request $request, Answer $answer, Quiz $quiz)
    {
        if(!$back = $request->get('back')){
            $back = $this->generateUrl('quiz_answer_start', [
                'quiz' => $answer->getQuiz()->getId()]);
        }


        return $this->render('Quiz/Answer/finish.html.twig', array(
            'page_title' => $quiz->getTitle() .' - Quiz Finalizado',
            'quiz' => $quiz,
            'actions' => [
                'delete' => [
                    'path' => $this->generateUrl('answer_delete',[
                        'id' => $answer->getId(), 'quiz' => $quiz->getId()]),
                    'label' => 'Excluir resposta',
                    'role' => 'ROLE_ADMIN'
                ],
            ],
            'step' => 5,
            'back' => $back,
            'answer' => $answer,
        ));
    }

    /**
     * Displays a form to edit an existing answer entity.
     *
     * @Route("/{id}/delete", name="answer_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Answer $answer)
    {
        $quiz = $answer->getQuiz();

        $this->getDoctrine()->getManager()->remove($answer);
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success', 'Exclusão efetuada com sucesso');

        return $this->redirectToRoute('admin_quiz_answers',[
            'id' => $quiz->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing answer entity.
     *
     * @Route("/{id}/edit", name="answer_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Answer $answer)
    {
        $deleteForm = $this->createDeleteForm($answer);
        $editForm = $this->createForm('QuizBundle\Form\AnswerType', $answer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('answer_edit', array('id' => $answer->getId()));
        }

        return $this->render('answer/edit.html.twig', array(
            'answer' => $answer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


}
