<?php

namespace QuizBundle\Controller;

use QuizBundle\Entity\Question;
use QuizBundle\Entity\Quiz;
use QuizBundle\Form\QuizType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Quiz controller.
 *
 * @Route("")
 */
class QuizController extends Controller
{
    /**
     * Lists all tag entities.
     *
     * @Route("/ajax/quiz", name="ajax_quiz", options={"expose=true"}, methods={"GET"})
     */
    public function ajaxQuizAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository(Quiz::class)->createQueryBuilder('a');
        $tags = $query->andWhere($query->expr()->like('a.title',':name'))
            ->setParameter('name','%'.$request->get('q').'%')
            ->getQuery()->getResult();

        $result = array();

        /** @var Quiz $tag */
        foreach ($tags as $tag){
            $result['results'][] = [
                'id' => $tag->getId(),
                'text' => $tag->getTitle(),
            ];
        }

        return new JsonResponse($result);
    }

    /**
     * Lists all quiz entities.
     *
     * @Route("/quiz", name="quiz_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $quizzes = $em->getRepository(Quiz::class)->findBy(['published' => true]);

        return $this->render('Quiz/Quiz/index.html.twig', array(
            'page_title' => 'Cursos Online',
            'actions' => [
                'new' => [
                    'label' => 'Administrar',
                    'path' => $this->generateUrl('admin_quiz_index'),
                    'role' => 'ROLE_ADMIN'
                ],
            ],
            'back' => $this->generateUrl('home'),
            'quizzes' => $quizzes,
        ));
    }
    /**
     * Lists all quiz entities.
     *
     * @Route("/admin/quiz", name="admin_quiz_index", methods={"GET"})
     */
    public function adminAction()
    {
        $em = $this->getDoctrine()->getManager();

        $quizzes = $em->getRepository(Quiz::class)->findAll();

        $rows = [
            Quiz::V_EDRPIXEL => ['name' => Quiz::V_EDRPIXEL_LABEL, 'quiz' => []],
            Quiz::V_ALL => ['name' => Quiz::V_ALL_LABEL, 'quiz' => []],
            Quiz::V_ALUNO_UNICAMP => ['name' => Quiz::V_ALUNO_UNICAMP_LABEL, 'quiz' => []],
        ];

        /** @var Quiz $quiz */
        foreach ($quizzes as $quiz){
            $rows[$quiz->getVisibility()]['quiz'][] = $quiz;
        }


        return $this->render('Quiz/Quiz/admin.html.twig', array(
            'page_title' => 'Quiz',
            'actions' => [
                'new' => [
                    'label' => 'Incluir quiz',
                    'path' => $this->generateUrl('admin_quiz_new'),
                ],
                'newedrpixel' => [
                    'label' => 'Incluir quiz E-drpixel',
                    'role' => 'ROLE_ADMIN',
                    'path' => $this->generateUrl('admin_quiz_new',['type' => 'e_drpixel']),
                ],
            ],
            'back' => $this->generateUrl('admin'),
            'rows' => $rows,
        ));
    }

    /**
     * Creates a new quiz entity.
     *
     * @Route("/admin/quiz/new", name="admin_quiz_new", methods={"GET","POST"})
     */
    public function newAction(Request $request)
    {
        $quiz = new Quiz();

        if($request->get('type') == Quiz::V_EDRPIXEL){
            $quiz->setVisibility(Quiz::V_EDRPIXEL);
            $quiz->setPreQuiz(false);
            $quiz->setFeedback(false);
            $quiz->setFixacao(false);
            $quiz->setPublished(false);
        }

        if($content = $request->get('content')){
            $quiz->setContent($quiz);
        }
        $form = $this->createForm(QuizType::class, $quiz, ['isNew' => true,]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $quiz->setPath(Quiz::url_slug($quiz->getTitle()));
            $quiz->setCreated(new \DateTime());
            $quiz->setCreatedBy($this->getUser());


            $em->persist($quiz);
            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_quiz_show', array('id' => $quiz->getId()));
        }

        return $this->render('Quiz/Quiz/edit.html.twig', array(
            'page_title' => 'Incluir Quiz',
            'back' => $this->generateUrl('admin_quiz_index'),
            'quiz' => $quiz,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a quiz entity.
     *
     * @Route("admin/quiz/{id}", name="admin_quiz_show", methods={"GET"})
     */
    public function showAction(Quiz $quiz, Request $request)
    {
        if(!$back = $request->get('back')){
            $back = $this->generateUrl('admin_quiz_index');
        }

        $actions = ['edit' => [
                'label' => 'Editar quiz',
                'path' => $this->generateUrl('admin_quiz_edit', [
                    'id' => $quiz->getId(),
                    'back' => $request->getRequestUri()
                ])
            ],
            'newquestion' => [
                'label' => 'Incluir pergunta',
                'path' => $this->generateUrl('admin_quiz_question_new', [
                    'quiz' => $quiz->getId(),
                    'type' => 0,
                ])
            ]
        ];


        //Se esta habilitado feedback
        if($quiz->getFeedback()){
            $actions['newquestion1'] = [
                'label' => 'Incluir pergunta de Feedback',
                'path' => $this->generateUrl('admin_quiz_question_new', [
                    'quiz' => $quiz->getId(),
                    'type' => 1,
                ])
            ];
        }

        return $this->render('Quiz/Quiz/admin.show.html.twig', array(
            'page_title' => 'Quiz: ' . $quiz->getTitle(),
            'actions' => $actions,
            'quiz' => $quiz,
            'back' => $back,

        ));
    }

    /**
     * Finds and displays a quiz entity.
     *
     * @Route("admin/quiz/{id}/answers", name="admin_quiz_answers", methods={"GET"})
     */
    public function answersAction(Quiz $quiz, Request $request)
    {
        return $this->render(':Quiz/Quiz:answers.html.twig',[
            'page_title' => $quiz->getTitle() . ' - Respsotas',
            'content' => $quiz->getContent(),
            'quiz' => $quiz,
            'back' => $this->generateUrl('admin_quiz_index')
        ]);
    }

    /**
     * Displays a form to edit an existing quiz entity.
     *
     * @Route("/admin/quiz/{id}/edit", name="admin_quiz_edit", methods={"GET","POST"})
     */
    public function editAction(Request $request, Quiz $quiz)
    {

        $editForm = $this->createForm('QuizBundle\Form\QuizType', $quiz);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','Alteração efetuada com sucesso.');
            return $this->redirectToRoute('admin_quiz_show', array('id' => $quiz->getId()));
        }

        if(!$back = $request->get('back')){
            $back = $this->generateUrl('admin_quiz_index');
        }

        return $this->render('Quiz/Quiz/edit.html.twig', array(
            'page_title' => 'Editar quiz: ' . $quiz->getTitle(),
            'back' => $back,
            'content' => $quiz->getContent(),
            'quiz' => $quiz,
            'form' => $editForm->createView(),

        ));
    }


}
