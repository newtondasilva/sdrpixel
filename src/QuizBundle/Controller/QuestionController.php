<?php

namespace QuizBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use QuizBundle\Entity\Question;
use QuizBundle\Entity\QuestionItem;
use QuizBundle\Entity\Quiz;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Question controller.
 *
 * @Route("")
 */
class QuestionController extends Controller
{

    /**
     * Creates a new question entity.
     *
     * @Route("/admin/quiz/{quiz}/question/new", name="admin_quiz_question_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request, Quiz $quiz)
    {
        $question = new Question();
        $form = $this->createForm('QuizBundle\Form\QuestionType', $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $question->setCreated(new \DateTime());

            if($request->get('type') == 0){
                $question->setQuiz($quiz);
            } else {
                $question->setQuizFeedback($quiz);
            }

            $em = $this->getDoctrine()->getManager();

            /** @var QuestionItem $iten */
            foreach ($question->getItens() as $iten){
                $question->addIten($iten);
                $iten->setQuestion($question);
                $em->persist($iten);
            }


            $em->persist($question);
            $em->flush();

            $this->addFlash('success','Inclusão efetuada com sucesso.');
            return $this->redirectToRoute('admin_quiz_show', array('id' => $quiz->getId()));
        }

        return $this->render('Quiz/Question/edit.html.twig', array(
            'page_title' => $quiz->getTitle() . ' - Incluir pergunta',
            'question' => $question,
            'back' => $this->generateUrl('admin_quiz_edit',['id' => $quiz->getId()]),
            'quiz' => $quiz,
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing question entity.
     *
     * @Route("/admin/question/{id}/edit", name="admin_question_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request, Question $question)
    {
        $editForm = $this->createForm('QuizBundle\Form\QuestionType', $question);

        $original = new ArrayCollection();
        foreach ($question->getItens() as $iten){
            $original->add($iten);
        }

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($original as &$row){
                if($question->getItens()->contains($row) == false){
                    $em->remove($row);
                    $em->flush();
                }
            }

            /** @var QuestionItem $iten */
            foreach ($question->getItens() as $iten){
                $question->addIten($iten);
                $iten->setQuestion($question);
                $em->persist($iten);
            }

            $em->persist($question);
            $em->flush();

            $this->addFlash('success','Alteração efetuada com sucesso.');
            return $this->redirectToRoute('admin_quiz_show', array('id' => $question->getQuiz()->getId()));
        }

        return $this->render('Quiz/Question/edit.html.twig', array(
            'page_title' => 'Editar questão: ' . substr(strip_tags($question->getText()),0,100),
            'question' => $question,
            'actions' => [
                'delete' => [
                    'label' => 'Excluir pergunta',
                    'path' => $this->generateUrl('admin_question_delete',['id' => $question->getId()])
                ],
            ],
            'form' => $editForm->createView(),
            'back' => $this->generateUrl('admin_quiz_show',['id' => $question->getQuiz()->getId()])
        ));
    }

    /**
     * Displays a form to edit an existing question entity.
     *
     * @Route("/admin/question/{id}/delete", name="admin_question_delete")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, Question $question)
    {
        $form = $this->createFormBuilder()->getForm();
        $quiz = $question->getQuiz();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->remove($question);
            $em->flush();
            $this->addFlash('success','Exclusão efetuada com sucesso');
            return $this->redirectToRoute('admin_quiz_show',['id' => $quiz->getId()]);
        }

        return $this->render(':default:delete.html.twig',[
            'page_title' => 'Confirma exclusão da questão',
            'form' => $form->createView(),
            'back' => $this->generateUrl('admin_question_edit',['id' => $question->getId()]),
        ]);
    }


}
