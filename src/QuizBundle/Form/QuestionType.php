<?php

namespace QuizBundle\Form;


use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text',CKEditorType::class,[
                'label' => 'Texto da pergunta'
            ])

            ->add('itens', CollectionType::class,[
                'label' => 'Respostas',
                'entry_type'   => QuestionItemType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype'    => true,
                'required'     => true,
                'by_reference' => true,
                'delete_empty' => true,
                    'attr' => array('class' => 'questao-items',)

            ])
            ->add('correctFeedback', CKEditorType::class, [
                'label' => 'Feedback questão correta',
                'required' => false,

            ])
            ->add('wrongFeedback', CKEditorType::class, [
                'label' => 'Feedback questão incorreta',
                'required' => false,
            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'QuizBundle\Entity\Question'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'QuestionType';
    }


}
