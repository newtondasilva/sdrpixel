<?php

namespace QuizBundle\Form;

use QuizBundle\Entity\Question;
use QuizBundle\Entity\QuestionItem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnswerItemType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Question $question */
        $question = $options['question'];

        $builder->add('questionItem', EntityType::class,[
            'class' => QuestionItem::class,
            'label' => 'Resposta',
            'choices' => $question->getItens(),
            'choice_label' => 'text',
            'expanded' => true,
            'label_format' => 'HTML',
        ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'QuizBundle\Entity\AnswerItem',
            'question' => array(),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'quizbundle_answer';
    }


}
