<?php

namespace QuizBundle\Form;

use ContentBundle\Entity\Content;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use QuizBundle\Entity\Quiz;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class QuizType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Quiz $quiz */
        $quiz = $builder->getData();

        $builder
            ->add('title', TextType::class, [
                'label' => 'Título'
            ])
            ->add('visibility', ChoiceType::class, [
                'label' => 'Visibilidade',
                'choices' => Quiz::getVisibilityChoices(),
            ])
            ;

        if($quiz->getVisibility() != Quiz::V_EDRPIXEL){
            $builder->add('path', TextType::class, [
                'label' => 'Endereço web(deixe em branco para geração automática)',
                'required' => false,
                'disabled' => $options['isNew']
            ])

                ->add('preQuiz', CheckboxType::class, [
                    'label' => 'Pré-Teste (verificar desempenho antes da realização do teste)',
                    'required' => false,
                ])
                ->add('feedback', CheckboxType::class, [
                    'label' => 'Feedback (Coletar feedback ao final do teste, necessário criar perguntas de feedback)',
                    'required' => false,
                ])
                ->add('fixacao', CheckboxType::class, [
                    'label' => 'Fixação (verificar desempenho após preenchimento do teste)',
                    'required' => false,
                ])
                ->add('resume', TextareaType::class, [
                    'label' => 'Texto de cahamda',
                ])
                ->add('body', CKEditorType::class, [
                    'label' => 'Texto de orientação do Teste', 'required' => false,
                ])
                ->add('term', CKEditorType::class, [
                    'label' => 'Termo de aceite do teste',
                    'required' => false,
                ])
                ->add('content', Select2EntityType::class,[
                    'label' => 'Conteúdo associado',
                    'class' => 'ContentBundle\Entity\Content',
                    'remote_route' => 'ajax_content',
                    'primary_key' => 'id',
                    'text_property' => 'titulo',
                    'minimum_input_length' => 2,
                    'page_limit' => 20,
                    'allow_clear' => true,
                    'required' => false,
                    'delay' => 250,
                    'cache' => true,
                    'cache_timeout' => 60000, // if 'cache' is true
                    'language' => 'pt-br',
                    'placeholder' => 'Informe o conteúdo',
                ])
                ->add('published', CheckboxType::class,[
                    'label' => 'Publicado? (teste pronto para preenchimento)',
                    'required' => false,
                ]);
        }

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'QuizBundle\Entity\Quiz',
            'isNew' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'quizbundle_quiz';
    }


}
