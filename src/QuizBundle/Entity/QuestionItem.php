<?php

namespace QuizBundle\Entity;

use ContentBundle\Entity\CourseContent;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * QuestionItem
 *
 * @ORM\Table(name="quiz_question_item")
 * @ORM\Entity(repositoryClass="QuizBundle\Repository\QuestionItemRepository")
 */
class QuestionItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="QuizBundle\Entity\Question", inversedBy="itens")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * @Serializer\Groups({"default","answerItem","courseProgress"})
     */
    private $question;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\AnswerItem", mappedBy="questionItem")
     */
    private $answerItems;

    public function __construct()
    {
        $this->answerItems = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     */
    private $text;

    /**
     * @var bool
     *
     * @ORM\Column(name="correct", type="boolean")
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     */
    private $correct;

    /**
     * @var string
     *
     * @ORM\Column(name="feedback", type="text", nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     */
    private $feedback;

    /**
     * @var int
     *
     * @ORM\Column(name="delta", type="smallint", nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent"})
     */
    private $delta;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return QuestionItem
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        // Remove <p> inicial
        $this->text = substr_replace($this->text,'',0,3);
        $this->text = substr_replace($this->text,'',-4);
        return $this->text;
    }

    /**
     * Set correct
     *
     * @param boolean $correct
     *
     * @return QuestionItem
     */
    public function setCorrect($correct)
    {
        $this->correct = $correct;

        return $this;
    }

    /**
     * Get correct
     *
     * @return bool
     */
    public function getCorrect()
    {
        return $this->correct;
    }

    /**
     * Set feedback
     *
     * @param string $feedback
     *
     * @return QuestionItem
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;

        return $this;
    }

    /**
     * Get feedback
     *
     * @return string
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Set delta
     *
     * @param integer $delta
     *
     * @return QuestionItem
     */
    public function setDelta($delta)
    {
        $this->delta = $delta;

        return $this;
    }

    /**
     * Get delta
     *
     * @return int
     */
    public function getDelta()
    {
        return $this->delta;
    }



    /**
     * Add answer
     *
     * @param \QuizBundle\Entity\Answer $answer
     *
     * @return QuestionItem
     */
    public function addAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $this->answerItems[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \QuizBundle\Entity\Answer $answer
     */
    public function removeAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $this->answerItems->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswerItems()
    {
        return $this->answerItems;
    }

    /**
     * Set question
     *
     * @param \QuizBundle\Entity\Question $question
     *
     * @return QuestionItem
     */
    public function setQuestion(\QuizBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \QuizBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Add answerItem
     *
     * @param \QuizBundle\Entity\Answer $answerItem
     *
     * @return QuestionItem
     */
    public function addAnswerItem(\QuizBundle\Entity\Answer $answerItem)
    {
        $this->answerItems[] = $answerItem;

        return $this;
    }

    /**
     * Remove answerItem
     *
     * @param \QuizBundle\Entity\Answer $answerItem
     */
    public function removeAnswerItem(\QuizBundle\Entity\Answer $answerItem)
    {
        $this->answerItems->removeElement($answerItem);
    }

    public function getAnswersItemByCourseProgress(CourseContent $courseContent){
        $answerItems = new ArrayCollection();

        /** @var AnswerItem $answerItem */
        foreach ($this->getAnswerItems() as $answerItem){
            if(!$answerItem->getAnswer()->getCourseProgress()) continue;
            if($answerItem->getAnswer()->getCourseProgress()->getCourseContent()->getId() == $courseContent->getId()){
                $answerItems->add(($answerItem));
            }
        }

        return $answerItems;
    }
}
