<?php

namespace QuizBundle\Entity;

use AppBundle\Entity\User;
use ContentBundle\Entity\Content;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Quiz
 *
 * @ORM\Table(name="quiz")
 * @ORM\Entity(repositoryClass="QuizBundle\Repository\QuizRepository")
 */
class Quiz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseContent"})
     */
    private $id;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->feedbacks = new ArrayCollection();
    }

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\Answer", mappedBy="quiz",cascade={"persist","remove"})
     */
    private $answers;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\Question", mappedBy="quiz",cascade={"persist","remove"})
     * @Serializer\Groups({"default","list","show","courseContent"})
     */
    private $questions;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\Question", mappedBy="quizFeedback",cascade={"remove","persist"})
     */
    private $feedbacks;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseContent", mappedBy="quiz",cascade={"remove","persist"})
     */
    private $courseContents;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="quizes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\OneToOne(targetEntity="ContentBundle\Entity\Content", inversedBy="quiz")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id",onDelete="SET NULL")
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="visibility", type="string", nullable=false)
     */
    private $visibility;


    const V_ALL = 'all';
    const V_ALUNO_UNICAMP = 'aluno_unicamp';
    const V_ALL_LABEL = 'Público';
    const V_ALUNO_UNICAMP_LABEL = 'Alunos da Unicamp';
    const V_EDRPIXEL = 'e_drpixel';
    const V_EDRPIXEL_LABEL = 'E-DrPixel';

    /**
     * @return array
     */
    public static function getVisibilityChoices(){
        return [
            Quiz::V_ALL_LABEL => Quiz::V_ALL,
            Quiz::V_ALUNO_UNICAMP_LABEL => Quiz::V_ALUNO_UNICAMP,
            Quiz::V_EDRPIXEL_LABEL => Quiz::V_EDRPIXEL,
        ];
    }


    /**
     * @var int
     *
     * @ORM\Column(name="type", type="boolean")
     */
    private $preQuiz;

    /**
     * @var int
     *
     * @ORM\Column(name="feedback", type="boolean")
     */
    private $feedback;

    /**
     * @var int
     *
     * @ORM\Column(name="fixacao", type="boolean")
     */
    private $fixacao;

    /**
     * @var string
     *
     * @ORM\Column(name="resume", type="string", length=1000, nullable=true)
     */
    private $resume;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     *
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="term", type="text", nullable=true)
     */
    private $term;

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param string $term
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=1000, unique=false)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=1000, unique=false)
     * @Serializer\Groups({"default","list","show","courseContent"})
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Quiz
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Quiz
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Quiz
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Quiz
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }



    /**
     * Set preQuiz
     *
     * @param boolean $preQuiz
     *
     * @return Quiz
     */
    public function setPreQuiz($preQuiz)
    {
        $this->preQuiz = $preQuiz;

        return $this;
    }

    /**
     * Get preQuiz
     *
     * @return boolean
     */
    public function getPreQuiz()
    {
        return $this->preQuiz;
    }

    /**
     * Set resume
     *
     * @param string $resume
     *
     * @return Quiz
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume
     *
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Quiz
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }


    /**
     *
     * @param string $str
     * @param array $options
     * @return string
     */
    public static function url_slug($str, $options = array()) {
	// Make sure string is in UTF-8 and strip invalid UTF-8 characters
	$str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

	$defaults = array(
		'delimiter' => '-',
		'limit' => null,
		'lowercase' => true,
		'replacements' => array(),
		'transliterate' => true,
	);

	// Merge options
	$options = array_merge($defaults, $options);

	$char_map = array(
		// Latin
		'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
		'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
		'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
		'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
		'ß' => 'ss', 'Ç' => 'c', 'ç' => 'c',
		'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
		'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
		'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
		'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
		'ÿ' => 'y',
	);

	// Make custom replacements
	$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

	// Transliterate characters to ASCII
	if ($options['transliterate']) {
		$str = str_replace(array_keys($char_map), $char_map, $str);
	}

	// Replace non-alphanumeric characters with our delimiter
	$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

	// Remove duplicate delimiters
	$str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

	// Truncate slug to max. characters
	$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

	// Remove delimiter from ends
	$str = trim($str, $options['delimiter']);

	return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }


    /**
     * Add question
     *
     * @param \QuizBundle\Entity\Question $question
     *
     * @return Quiz
     */
    public function addQuestion(\QuizBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \QuizBundle\Entity\Question $question
     */
    public function removeQuestion(\QuizBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Quiz
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Add answer
     *
     * @param \QuizBundle\Entity\Answer $answer
     *
     * @return Quiz
     */
    public function addAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \QuizBundle\Entity\Answer $answer
     */
    public function removeAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set content
     *
     * @param Content
     *
     * @return Quiz
     */
    public function setContent(Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return Content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isVisible(User $user = null){
        if(!$user and $this->getVisibility() != Quiz::V_ALL) return false;

        switch ($this->getVisibility()){
            case Quiz::V_ALL: return true; break;
            case Quiz::V_ALUNO_UNICAMP:

                if($user->getTipoInstituicao() == User::ALUNOUNICAMP){
                    return true;
                } else {
                    return false;
                }
        }
    }

    /**
     * @return int
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * @param int $feedback
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * @return int
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * @param int $visibility
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }

    /**
     * @return int
     */
    public function getFixacao()
    {
        return $this->fixacao;
    }

    /**
     * @param int $fixacao
     */
    public function setFixacao($fixacao)
    {
        $this->fixacao = $fixacao;
    }

    /**
     * Add feedback
     *
     * @param \QuizBundle\Entity\Question $feedback
     *
     * @return Quiz
     */
    public function addFeedback(\QuizBundle\Entity\Question $feedback)
    {
        $this->feedbacks[] = $feedback;

        return $this;
    }

    /**
     * Remove feedback
     *
     * @param \QuizBundle\Entity\Question $feedback
     */
    public function removeFeedback(\QuizBundle\Entity\Question $feedback)
    {
        $this->feedbacks->removeElement($feedback);
    }

    /**
     * Get feedbacks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeedbacks()
    {
        return $this->feedbacks;
    }

    /**
     * Add courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent $courseContent
     *
     * @return Quiz
     */
    public function addCourseContent(\ContentBundle\Entity\CourseContent $courseContent)
    {
        $this->courseContents[] = $courseContent;

        return $this;
    }

    /**
     * Remove courseContent.
     *
     * @param \ContentBundle\Entity\CourseContent $courseContent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseContent(\ContentBundle\Entity\CourseContent $courseContent)
    {
        return $this->courseContents->removeElement($courseContent);
    }

    /**
     * Get courseContents.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseContents()
    {
        return $this->courseContents;
    }

    /**
     *
     * Returns answer from an User
     * @param User $user
     * @return null|Answer
     */
    public function getAnswerUser(User $user){
        /** @var Answer $answer */
        foreach ($this->getAnswers() as $answer){
            if($answer->getUser()->getId() == $user->getId()){
                return $answer;
            }
        }

        return null;
    }
}
