<?php

namespace QuizBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Question
 *
 * @ORM\Table(name="quiz_question")
 * @ORM\Entity(repositoryClass="QuizBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     */
    private $id;

    public function __construct()
    {
        $this->itens = new ArrayCollection();
    }

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\QuestionItem", mappedBy="question",cascade={"persist","remove"})
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     * @Serializer\SerializedName("questionItems")
     */
    private $itens;

    /**
     * @ORM\ManyToOne(targetEntity="QuizBundle\Entity\Quiz", inversedBy="questions")
     * @ORM\JoinColumn(name="quiz_id", referencedColumnName="id")
     */
    private $quiz;

    /**
     * @ORM\ManyToOne(targetEntity="QuizBundle\Entity\Quiz", inversedBy="feedbacks")
     * @ORM\JoinColumn(name="quiz_feedback_id", referencedColumnName="id")
     */
    private $quizFeedback;


    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="correctText", type="text",nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent"})
     * @Serializer\SerializedName("correctFeedback")
     */
    private $correctFeedback;

    /**
     * @var string
     *
     * @ORM\Column(name="wrongText", type="text", nullable=true)
     * @Serializer\Groups({"default","list","show","courseContent"})
     * @Serializer\SerializedName("wrongFeedback")
     */
    private $wrongFeedback;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getType(){
        if($this->getQuiz()) return 0;
        return 1;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        // Remove <p> inicial
        $this->text = substr_replace($this->text,'',0,3);
        $this->text = substr_replace($this->text,'',-4);
        return $this->text;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Question
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set correctText
     *
     * @param string $correctFeedback
     *
     * @return Question
     */
    public function setCorrectFeedback($correctFeedback)
    {
        $this->correctFeedback = $correctFeedback;

        return $this;
    }

    /**
     * Get correctText
     *
     * @return string
     */
    public function getCorrectFeedback()
    {
        return $this->correctFeedback;
    }

    /**
     * Set wrongText
     *
     * @param string $wrongFeedback
     *
     * @return Question
     */
    public function setWrongFeedback($wrongFeedback)
    {
        $this->wrongFeedback = $wrongFeedback;

        return $this;
    }

    /**
     * Get wrongText
     *
     * @return string
     */
    public function getWrongFeedback()
    {
        return $this->wrongFeedback;
    }

    /**
     * Add iten
     *
     * @param \QuizBundle\Entity\QuestionItem $iten
     *
     * @return Question
     */
    public function addIten(\QuizBundle\Entity\QuestionItem $iten)
    {
        $this->itens[] = $iten;

        return $this;
    }

    /**
     * Remove iten
     *
     * @param \QuizBundle\Entity\QuestionItem $iten
     */
    public function removeIten(\QuizBundle\Entity\QuestionItem $iten)
    {
        $this->itens->removeElement($iten);
    }

    /**
     * Get itens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItens()
    {
        return $this->itens;
    }

    /**
     * Set quiz
     *
     * @param \QuizBundle\Entity\Quiz $quiz
     *
     * @return Question
     */
    public function setQuiz(\QuizBundle\Entity\Quiz $quiz = null)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Get quiz
     *
     * @return \QuizBundle\Entity\Quiz
     */
    public function getQuiz()
    {
        return $this->quiz;
    }



    /**
     * Set quizFeedback
     *
     * @param \QuizBundle\Entity\Quiz $quizFeedback
     *
     * @return Question
     */
    public function setQuizFeedback(\QuizBundle\Entity\Quiz $quizFeedback = null)
    {
        $this->quizFeedback = $quizFeedback;

        return $this;
    }

    /**
     * Get quizFeedback
     *
     * @return \QuizBundle\Entity\Quiz
     */
    public function getQuizFeedback()
    {
        return $this->quizFeedback;
    }
}
