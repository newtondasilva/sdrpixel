<?php

namespace QuizBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Answer
 *
 * @ORM\Table(name="quiz_answer")
 * @ORM\Entity(repositoryClass="QuizBundle\Repository\AnswerRepository")
 */
class Answer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     */
    private $id;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\AnswerItem", mappedBy="answer",cascade={"persist","remove"})
     * @Serializer\Groups({"default","list","show","courseContent"})
     * @Serializer\SerializedName("answerItems")
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="QuizBundle\Entity\Quiz", inversedBy="answers")
     * @ORM\JoinColumn(name="quiz_id", referencedColumnName="id")
     */
    private $quiz;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="answers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="ContentBundle\Entity\CourseProgress", inversedBy="answers")
     * @ORM\JoinColumn(name="course_progress_id", referencedColumnName="id")
     */
    private $courseProgress;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Serializer\Groups({"default","list","show","courseContent"})
     */
    private $created;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Answer
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set questionItem
     *
     * @param \QuizBundle\Entity\QuestionItem $questionItem
     *
     * @return Answer
     */
    public function setQuestionItem(\QuizBundle\Entity\QuestionItem $questionItem = null)
    {
        $this->questionItem = $questionItem;

        return $this;
    }

    /**
     * Get questionItem
     *
     * @return \QuizBundle\Entity\QuestionItem
     */
    public function getQuestionItem()
    {
        return $this->questionItem;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Answer
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add item
     *
     * @param \QuizBundle\Entity\AnswerItem $item
     *
     * @return Answer
     */
    public function addItem(\QuizBundle\Entity\AnswerItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \QuizBundle\Entity\AnswerItem $item
     */
    public function removeItem(\QuizBundle\Entity\AnswerItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems($type = null)
    {
        if(is_numeric($type)){
            $result = new ArrayCollection();

            /** @var AnswerItem $item */
            foreach ($this->items as $item){

                if($item->getType() == $type){
                    $result->add($item);
                }
            }

            return $result;
        }
        return $this->items;
    }

    /**
     * Set quiz
     *
     * @param \QuizBundle\Entity\Quiz $quiz
     *
     * @return Answer
     */
    public function setQuiz(\QuizBundle\Entity\Quiz $quiz = null)
    {
        $this->quiz = $quiz;

        return $this;
    }

    /**
     * Get quiz
     *
     * @return \QuizBundle\Entity\Quiz
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     * Returns next answered question or null when all questions was answered
     * @return Question | null
     *
     */
    public function getNextQuestion($type = 1){
        $answered = new ArrayCollection();

        /** @var AnswerItem $item */
        foreach ($this->getItems() as $item){

            // Pesquisa somente questoes do mesmo tipo (Ex. Pre, Normal ou fixacao)
            if($item->getType() == $type){
                $answered->add($item->getQuestionItem()->getQuestion());
            }

        }

        foreach ($this->getQuiz()->getQuestions() as $question){
            // Nao contem resposta para a questao
            if($answered->contains($question) === false){
                return $question;
            }
        }

        return null;
    }

    /**
     * Obtem numero de respostas corretas
     * @return int
     */
    public function getCorrects($type = null){
        $result = 0;

        /** @var AnswerItem $item */
        foreach ($this->getItems($type) as $item){
            if($item->getQuestionItem()->getCorrect()){
                $result++;
            }
        }

        return $result;
    }


    /**
     * Obtem numero de respostas incorretas
     * @return int
     */
    public function getIncorrects($type = null){
        $result = 0;

        /** @var AnswerItem $item */
        foreach ($this->getItems($type) as $item){
            if(!$item->getQuestionItem()->getCorrect()){
                $result++;
            }
        }

        return $result;
    }

    /**
     * Retorna valor de acertos
     * @param $type
     * @return int
     */
    public function getPercent($type){
        $correct = $this->getCorrects($type);

        $incorrect = $this->getIncorrects($type);

        $total = count($this->getItems($type));

        if(!$total) return 0;

        if($correct == 0 and $incorrect > 0) return 0;
        if($incorrect == 0 and $correct > 0) return 100;
        if($incorrect == 0 and $correct == 0) return 0;



        if($incorrect > 0 and $correct > 0) return ($correct / $total) * 100;

    }

    /**
     *
     * @param Question $question
     * @return null|AnswerItem
     */
    public function getAnswerItem(Question $question, $answerType = 1){
        /** @var AnswerItem $answerItem */
        foreach ($this->getItems() as $answerItem){
            if($answerItem->getQuestionItem()->getQuestion()->getId() == $question->getId() and $answerItem->getType() == $answerType){
                return $answerItem;
            }
        }

        return null;
    }

    public function hasFeedback(){
        /** @var AnswerItem $answerItem */
        foreach ($this->getItems() as $answerItem){
            if($answerItem->getQuestionItem()->getQuestion()->getType() == 1){
                return true;
            }
        }
        return false;
    }


    /**
     * Set courseProgress.
     *
     * @param \ContentBundle\Entity\CourseProgress|null $courseProgress
     *
     * @return Answer
     */
    public function setCourseProgress(\ContentBundle\Entity\CourseProgress $courseProgress = null)
    {
        $this->courseProgress = $courseProgress;

        return $this;
    }

    /**
     * Get courseProgress.
     *
     * @return \ContentBundle\Entity\CourseProgress|null
     */
    public function getCourseProgress()
    {
        return $this->courseProgress;
    }
}
