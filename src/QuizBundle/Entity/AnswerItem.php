<?php

namespace QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * AnswerItem
 *
 * @ORM\Table(name="quiz_answer_item")
 * @ORM\Entity(repositoryClass="QuizBundle\Repository\AnswerItemRepository")
 */
class AnswerItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="QuizBundle\Entity\Answer", inversedBy="items")
     * @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     * @Serializer\Groups({"default","answerItem"})
     */
    private $answer;

    /**
     * @ORM\ManyToOne(targetEntity="QuizBundle\Entity\QuestionItem", inversedBy="answerItems")
     * @ORM\JoinColumn(name="question_item_id", referencedColumnName="id")
     * @Serializer\Groups({"default","list","show","courseContent","answerItem"})
     * @Serializer\SerializedName("questionItem")
     */
    private $questionItem;

    /**
     * @var int
     *
     * @ORM\Column(name="time", type="smallint")
     */
    private $time;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Serializer\Groups({"default","answerItem"})
     *
     */
    private $created;

    /**
     * Define se qual eh pre, questionario ou fixacao
     * @var
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
     * @Serializer\Groups({"default","list","show","courseContent"})
     */
    private $type;

    const T_PRE = 0;
    const T_NORMAL = 1;
    const T_FEEDBACK = 2;
    const T_FIXACAO = 3;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param \stdClass $answer
     *
     * @return AnswerItem
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return Answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return AnswerItem
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return AnswerItem
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set questionItem
     *
     * @param \QuizBundle\Entity\QuestionItem $questionItem
     *
     * @return AnswerItem
     */
    public function setQuestionItem(\QuizBundle\Entity\QuestionItem $questionItem = null)
    {
        $this->questionItem = $questionItem;

        return $this;
    }

    /**
     * Get questionItem
     *
     * @return \QuizBundle\Entity\QuestionItem
     */
    public function getQuestionItem()
    {
        return $this->questionItem;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
