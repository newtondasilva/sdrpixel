$('.ajax_answer_prequiz').on('click',function (element) {
    $questionItem = $(this).attr('questionItem');
    $quiz = $(this).attr('quiz');
    $answer = $(this).attr('answer');
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: Routing.generate('ajax_answer_prequiz', {
            'questionItem' : $questionItem,
            'quiz' : $quiz,
            'type' : $(this).attr('tipo'),
            'answer' : $answer,
        }),
        async: false //you won't need that if nothing in your following code is dependend of the result
    })
        .done(function(response){
            var result = '#ajax_answer_prequiz_' + $questionItem;
            $(result).html(response); //Change the html of the div with the id = "your_div"

        })
        .fail(function(jqXHR, textStatus, errorThrown){
            alert('Error : ' + errorThrown);
        });
})

