<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nome',
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
                'disabled' => true,
            ])
            ->add('nomeCitacao',TextType::class, [
                'label' => 'Nome para citação',
                'required' => false,
            ])
            ->add('formacao', ChoiceType::class,[
                'label' => 'Formação',
                'choices' => User::getFormacaoChoices(),
                'expanded' => true,
                'disabled' => false,
                'multiple' => false,
            ])
            ->add('tipoInstituicao', ChoiceType::class,[
                'label' => 'Instituição',
                'choices' => User::getTipoInstituicaoChoices(),
                'expanded' => true,
                'placeholder' => 'Nenhuma',
                'required' => false,
            ])
            ->add('area',TextType::class,['label' => "Área",'required' => false])
            ->add('instituicao', TextType::class, [
                'label' => 'Instituição',
                'required' => false,
                'attr' => ['instituicao']
            ])

            ->add('ra', TextType::class, [
                'label' => 'Se aluno da UNICAMP informe seu RA',
                'required' => false,
            ])
            ->add('newsletter', CheckboxType::class, [
                'label' => 'Receber newsletter do Dr.Pixel',
                'required' => false,
            ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}