<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Role
 *
 * @ORM\Table(name="app_role")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleRepository")
 */
class Role
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show"})
     *
     */
    private $id;

    private function __construct()
    {
        $this->itens = new ArrayCollection();
    }

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="role")
     */
    private $users;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=false)
     * @Serializer\Groups({"default","list","show"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sufix", type="string", length=255, unique=false)
     */
    private $sufix;

    /**
     * @return string
     */
    public function getSufix()
    {
        return $this->sufix;
    }

    /**
     * @param string $sufix
     */
    public function setSufix($sufix)
    {
        $this->sufix = $sufix;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Role
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
