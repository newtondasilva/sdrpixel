<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use QuizBundle\Entity\Answer;
use QuizBundle\Entity\Quiz;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="app_user")
 * @ORM\Entity()
 *
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"default","list","show","courseUser","courseContent"})
     */
    private $id;

    /**
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\Answer", mappedBy="user")
     */
    private $answers;

    /**
     *
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Course", mappedBy="createdBy")
     */
    private $courses;

    /**
     *
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseProgress", mappedBy="createdBy")
     */
    private $courseProgresses;

    /**
     *
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\CourseUser", mappedBy="user")
     * @Serializer\Groups({"default","show"})
     * @Serializer\SerializedName("courseUsers")
     */
    private $courseUsers;

    /**
     *
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Evento", mappedBy="createdBy")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Rate", mappedBy="user", cascade={"persist","remove"})
     */
    private $rates;


    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="QuizBundle\Entity\Quiz", mappedBy="createdBy")
     */
    private $quizes;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="ContentBundle\Entity\Content", mappedBy="createdBy")
     */
    private $contents;

    const FORMACAOGRADUACAO = 1;
    const FORMACAORESRAD = 2;
    //const FORMACAORESOUTRA = 3;
    const FORMACAORADIOLOGISTA = 4;
    const FORMACAOMEDICO = 5;
    const FORMACAO0 = 0;

    const FORMACAOGRADUACAOLABEL = 'Aluno(a) de graduação em medicina';
    const FORMACAORESRADLABEL = 'Residente';
    //const FORMACAORESOUTRALABEL = 'Residente de outra especialidade';
    const FORMACAORADIOLOGISTALABEL = 'Radiologista';
    const FORMACAOMEDICOLABEL = 'Médico de outra especialidade';
    const FORMACAO0LABEL = 'Nenhuma das anteriores';

    const ALUNONENHUMA = null;
    const ALUNOUNICAMP = 1;
    const ALUNOOUTRAINSTITUICAO = 2;

    const ALUNONENHUMALABEL = 'Nenhuma';
    const ALUNOUNICAMPLABEL = 'Aluno Unicamp';
    const ALUNOOUTRAINSTITUICAOLABEL = 'Aluno de outra instituição';

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Serializer\Groups({"default","show"})
     */
    private $tipoInstituicao;


    /**
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     * @Serializer\Groups({"default","list","show"})
     * @Serializer\SerializedName("googleId")
     */
    private $googleId;

    /**
     * @ORM\Column(type="string", length=1000, unique=false, nullable=true)
     * @Serializer\Groups({"default","list","show"})
     */
    private $picture;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nomeCitacao;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public $password;

    private $roles;

    /**
     * @ORM\Column(type="string", length=1000, unique=false)
     * @Serializer\Groups({"default","list","show"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=1000, unique=false)
     * @Serializer\Groups({"default","list","show"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1000, unique=false)
     * @Serializer\Groups({"default","list","show"})
     * @Serializer\SerializedName("firstName")
     *
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=1000, unique=false, nullable=true)
     * @Serializer\Groups({"default","list","show"})
     * @Serializer\SerializedName("lastName")
     */
    private $lastName;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Serializer\Groups({"default","show"})
     */
    private $formacao;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     * @Serializer\Groups({"default","show"})
     */
    private $instituicao;

    /**
     * @ORM\Column(name="area", type="string", length=1000, nullable=true)
     */
    private $area;

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @ORM\Column(type="boolean")
     */
    private $newsletter = true;

    /**
     * @return mixed
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @param mixed $newsletter
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ra;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created;

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getLastAccess()
    {
        return $this->lastAccess;
    }

    /**
     * @param mixed $lastAccess
     */
    public function setLastAccess($lastAccess)
    {
        $this->lastAccess = $lastAccess;
    }

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $lastAccess;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Role", inversedBy="users")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * @Serializer\Groups({"default","list","show"})
     */
    private $role;

    /**
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param Role $role
     */
    public function setRole(Role $role)
    {
        $this->role = $role;
    }


    public function getRoles()
    {
        $roles['ROLE_USER'] = 'ROLE_USER';

        if($this->role){
            $roles['ROLE_'.$this->getRole()->getSufix()] = 'ROLE_' . $this->getRole()->getSufix();
        }

        return $roles;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->email,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->name,
            $this->email,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }

    public function getPassword()
    {
        return null;
        return $this->password;
    }

    public function encodePassword($pass)
    {
        return sha1($pass);
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @param mixed $googleId
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add answer
     *
     * @param \QuizBundle\Entity\Answer $answer
     *
     * @return User
     */
    public function addAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \QuizBundle\Entity\Answer $answer
     */
    public function removeAnswer(\QuizBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @return mixed
     */
    public function getFormacao()
    {
        return $this->formacao;
    }

    /**
     * @param mixed $formacao
     */
    public function setFormacao($formacao)
    {
        $this->formacao = $formacao;
    }

    /**
     * @return mixed
     */
    public function getInstituicao()
    {
        return $this->instituicao;
    }

    /**
     * @param mixed $instituicao
     */
    public function setInstituicao($instituicao)
    {
        $this->instituicao = $instituicao;
    }

    /**
     * @return mixed
     */
    public function getRa()
    {
        return $this->ra;
    }

    /**
     * @param mixed $ra
     */
    public function setRa($ra)
    {
        $this->ra = $ra;
    }

    public static function getFormacaoChoices(){
        return [
            User::FORMACAOGRADUACAOLABEL => User::FORMACAOGRADUACAO,
            User::FORMACAORESRADLABEL => User::FORMACAORESRAD,

            User::FORMACAORADIOLOGISTALABEL => User::FORMACAORADIOLOGISTA,
            User::FORMACAOMEDICOLABEL => User::FORMACAOMEDICO,
            User::FORMACAO0LABEL => User::FORMACAO0,
        ];
    }

    public function getFormacaoLabel(){
        switch ($this->getFormacao()){
            case 0; return User::FORMACAO0LABEL; break;
            case 1; return User::FORMACAOGRADUACAOLABEL; break;
            case 2; return User::FORMACAORESRADLABEL; break;

            case 4; return User::FORMACAORADIOLOGISTALABEL; break;
            case 5; return User::FORMACAOMEDICO; break;
        }
    }

    public static function getTipoInstituicaoChoices(){
        return [
            User::ALUNOOUTRAINSTITUICAOLABEL => User::ALUNOOUTRAINSTITUICAO,
            User::ALUNOUNICAMPLABEL => User::ALUNOUNICAMP,
        ];
    }

    public function getTipoInstituicaoLabel(){
        switch ($this->getFormacao()){
            case 0; return User::ALUNONENHUMALABEL; break;
            case 1; return User::ALUNOUNICAMPLABEL; break;
            case 2; return User::ALUNOOUTRAINSTITUICAOLABEL; break;
        }
    }



    /**
     * Set unicamp
     *
     * @param boolean $tipoInstituicao
     *
     * @return User
     */
    public function setTipoInstituicao($tipoInstituicao)
    {
        $this->tipoInstituicao = $tipoInstituicao;

        return $this;
    }

    /**
     * Get unicamp
     *
     * @return boolean
     */
    public function getTipoInstituicao()
    {
        return $this->tipoInstituicao;
    }

    /**
     * Add quize
     *
     * @param \QuizBundle\Entity\Quiz $quize
     *
     * @return User
     */
    public function addQuize(\QuizBundle\Entity\Quiz $quize)
    {
        $this->quizes[] = $quize;

        return $this;
    }

    /**
     * Remove quize
     *
     * @param \QuizBundle\Entity\Quiz $quize
     */
    public function removeQuize(\QuizBundle\Entity\Quiz $quize)
    {
        $this->quizes->removeElement($quize);
    }

    /**
     * Get quizes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuizes()
    {
        return $this->quizes;
    }

    public function isQuizAnswered(Quiz $quiz){
        /** @var Answer $answer */
        foreach ($this->getAnswers() as $answer){
            if($answer->getQuiz()->getId() == $quiz->getId()) return true;
        }
        return false;
    }

    /**
     * Add event.
     *
     * @param \ContentBundle\Entity\Evento $event
     *
     * @return User
     */
    public function addEvent(\ContentBundle\Entity\Evento $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event.
     *
     * @param \ContentBundle\Entity\Evento $event
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEvent(\ContentBundle\Entity\Evento $event)
    {
        return $this->events->removeElement($event);
    }

    /**
     * Get events.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add rate.
     *
     * @param \ContentBundle\Entity\Rate $rate
     *
     * @return User
     */
    public function addRate(\ContentBundle\Entity\Rate $rate)
    {
        $this->rates[] = $rate;

        return $this;
    }

    /**
     * Remove rate.
     *
     * @param \ContentBundle\Entity\Rate $rate
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRate(\ContentBundle\Entity\Rate $rate)
    {
        return $this->rates->removeElement($rate);
    }

    /**
     * Get rates.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * Add content.
     *
     * @param \ContentBundle\Entity\Content $content
     *
     * @return User
     */
    public function addContent(\ContentBundle\Entity\Content $content)
    {
        $this->contents[] = $content;

        return $this;
    }

    /**
     * Remove content.
     *
     * @param \ContentBundle\Entity\Content $content
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContent(\ContentBundle\Entity\Content $content)
    {
        return $this->contents->removeElement($content);
    }

    /**
     * Get contents.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * @return mixed
     */
    public function getNomeCitacao()
    {
        return $this->nomeCitacao;
    }

    /**
     * @param mixed $nomeCitacao
     */
    public function setNomeCitacao($nomeCitacao)
    {
        $this->nomeCitacao = $nomeCitacao;
    }

    /**
     * Add course.
     *
     * @param \ContentBundle\Entity\Course $course
     *
     * @return User
     */
    public function addCourse(\ContentBundle\Entity\Course $course)
    {
        $this->courses[] = $course;

        return $this;
    }

    /**
     * Remove course.
     *
     * @param \ContentBundle\Entity\Course $course
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourse(\ContentBundle\Entity\Course $course)
    {
        return $this->courses->removeElement($course);
    }

    /**
     * Get courses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * Add courseProgress.
     *
     * @param \ContentBundle\Entity\CourseProgress $courseProgress
     *
     * @return User
     */
    public function addCourseProgress(\ContentBundle\Entity\CourseProgress $courseProgress)
    {
        $this->courseProgresses[] = $courseProgress;

        return $this;
    }

    /**
     * Remove courseProgress.
     *
     * @param \ContentBundle\Entity\CourseProgress $courseProgress
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseProgress(\ContentBundle\Entity\CourseProgress $courseProgress)
    {
        return $this->courseProgresses->removeElement($courseProgress);
    }

    /**
     * Get courseProgresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseProgresses()
    {
        return $this->courseProgresses;
    }

    /**
     * Add courseUser.
     *
     * @param \ContentBundle\Entity\CourseUser $courseUser
     *
     * @return User
     */
    public function addCourseUser(\ContentBundle\Entity\CourseUser $courseUser)
    {
        $this->courseUsers[] = $courseUser;

        return $this;
    }

    /**
     * Remove courseUser.
     *
     * @param \ContentBundle\Entity\CourseUser $courseUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCourseUser(\ContentBundle\Entity\CourseUser $courseUser)
    {
        return $this->courseUsers->removeElement($courseUser);
    }

    /**
     * Get courseUsers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseUsers()
    {
        return $this->courseUsers;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $this->encodePassword($password);
    }

    public function checkPassword($password)
    {
        if($this->encodePassword($password) == $this->password) return true;
        return false;
    }
}
