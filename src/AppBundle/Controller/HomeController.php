<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use ContentBundle\Entity\Campaign;
use ContentBundle\Entity\Content;
use ContentBundle\Entity\Counter;
use ContentBundle\Entity\Course;
use ContentBundle\Entity\Evento;
use ContentBundle\Entity\Tag;
use ContentBundle\Entity\TagType;
use Doctrine\Common\Collections\ArrayCollection;
use QuizBundle\Entity\Quiz;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    /**
     * @Route("/pt-br", name="home_ptbr", options={"sitemap" = true})
     */
    public function homePTBRAction()
    {
       return $this->redirectToRoute('home');
    }

    /**
     * @Route("/", name="home", options={"sitemap" = true})
     */
    public function home2Action()
    {
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();


        $eventos = $this->getDoctrine()->getManager()
            ->getRepository(Evento::class)
            ->createQueryBuilder('a')
            ->andWhere('a.dataFim >= :today')
            ->andWhere('a.published = true')
            ->setParameter('today', new \DateTime())
            ->orderBy('a.id','DESC')
            ->getQuery()->getResult();
        ;



        $quizesR = $this->getDoctrine()
            ->getRepository(Quiz::class)->findBy(['published' => true],['created' => 'DESC'],5);

        $quizes = array();
        foreach ($quizesR as $quiz){
            if($quiz->isVisible($user)) $quizes[] = $quiz;
        }


        $exclude = array();

        $recentContents = $em->getRepository(Content::class)->getRecentContent(5);
        $this->check($exclude,$recentContents);

        $neocasos = $em->getRepository(Content::class)->getNeoNatologia();
        $this->check($exclude,$neocasos);

        $graduacao = $em->getRepository(Content::class)->getGraduacao(3);
        $this->check($exclude,$graduacao);

        $topContents = $this->getDoctrine()->getRepository(Counter::class)->getTopContent(4);
        $this->check($exclude,$topContents);

        $casos = $em->getRepository(Content::class)->getRecentContent(3,Content::TYPE_DISCUSSAOCASO);
        $this->check($exclude,$casos);


        $topTags['total'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(2,null,1);
        $topTags['year'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(2,null,2, $topTags['total']);
        $topTags['month'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(2,null,3,array_merge($topTags['total'],$topTags['year']));
        $topTags['week'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(2,null,4,
                array_merge($topTags['total'],$topTags['year'],$topTags['month']));

        $courses = $this->getDoctrine()->getRepository(Course::class)
            ->findBy(['published' => true,'visibility' => Course::VIS_EDRPIXEL]);

        $especialidades = $em->getRepository(Tag::class)->findBy(['tagType' => 3]);
        shuffle($especialidades);
        $especialidades = array_slice($especialidades,0,3);

        $metodos = $em->getRepository(Tag::class)->findBy(['tagType' => 4]);
        shuffle($metodos);
        $metodos = array_slice($metodos,0,3);

        $laudos = $em->getRepository(Content::class)->getByTag(10187,2);
        $this->check($exclude,$laudos);

        $slideshows = $em->getRepository(Campaign::class)->createQueryBuilder('c')
            ->andWhere('c.published = true')
            ->andWhere('c.slideshowName is not null')
            ->orderBy('c.id','DESC')
            ->getQuery()->getResult();

        return $this->render('AppBundle:Home2:home.html.twig',[
            'page_id' => 'home',
            'latest' => $recentContents,
            'slideshows' => $slideshows,
            'quizes' => $quizes,
            'neocasos' => $neocasos,
            'laudos' => $laudos,
            'especialidades' => $especialidades,
            'metodos' => $metodos,
            'graduacao' => $graduacao,
            'courses' => $courses,
            'casos' => $casos,
            'topTags' => $topTags,
            'topContents' => $topContents,
            'eventos' => $eventos,
        ]);
    }



    /**
     * @deprecated
     * @Route("/homeold", name="home_old", options={"sitemap" = true})
     */
    public function homeNewAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $quizesR = $this->getDoctrine()
            ->getRepository(Quiz::class)->findBy(['published' => true],['created' => 'DESC'],5);

        $quizes = array();
        foreach ($quizesR as $quiz){
            if($quiz->isVisible($user)) $quizes[] = $quiz;
        }

        $eventos = $this->getDoctrine()->getManager()->getRepository(Evento::class)->createQueryBuilder('a')
            ->andWhere('a.dataFim >= :today')
            ->andWhere('a.published = true')
            ->setParameter('today', new \DateTime())
            ->orderBy('a.id','DESC')
            ->getQuery()->getResult();
        ;

        $exclude = array();

        $casodasemana = $em->getRepository(Content::class)->getRecentCases(1);
        $this->check($exclude,$casodasemana);

        $recentContents = $em->getRepository(Content::class)->getRecentContent(5);
        $this->check($exclude,$recentContents);

        $neocasos = $em->getRepository(Content::class)->getNeoNatologia();
        $this->check($exclude,$neocasos);

        $graduacao = $em->getRepository(Content::class)->getGraduacao(5);
        $this->check($exclude,$graduacao);

        $topContents = $this->getDoctrine()->getRepository(Counter::class)->getTopContent(4);
        $this->check($exclude,$topContents);

        $casos = $em->getRepository(Content::class)->getRecentContent(5,Content::TYPE_DISCUSSAOCASO);
        $this->check($exclude,$casos);




        $topTags['total'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(1,null,1);
        $topTags['year'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(1,null,2, $topTags['total']);
        $topTags['month'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(1,null,3,array_merge($topTags['total'],$topTags['year']));
        $topTags['week'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(1,null,4,
                array_merge($topTags['total'],$topTags['year'],$topTags['month']));

        $courses = $this->getDoctrine()->getRepository(Course::class)
            ->findBy(['published' => true,'visibility' => Course::VIS_EDRPIXEL]);

        $especialidades = $em->getRepository(Tag::class)->findBy(['tagType' => 3]);
        shuffle($especialidades);
        $especialidades = array_slice($especialidades,0,3);

        $metodos = $em->getRepository(Tag::class)->findBy(['tagType' => 4]);
        shuffle($metodos);
        $metodos = array_slice($metodos,0,3);

        return $this->render('AppBundle:HomeNew:home.html.twig',[
            'page_id' => 'home',
            'latest' => $recentContents,
            'quizes' => $quizes,
            'neocasos' => $neocasos,
            'especialidades' => $especialidades,
            'metodos' => $metodos,
            'graduacao' => $graduacao,
            'courses' => $courses,
            'casos' => $casos,
            'casodasemana' => $casodasemana,
            'topTags' => $topTags,
            'laudos' => $laudos,
            'topContents' => $topContents,
            'eventos' => $eventos,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function errorAction(Request $request){
        return $this->redirectToRoute('busca',['q' => $request->getRequestUri()]);
    }


    /**
     * @Route("home_old", name="home_old", options={"sitemap" = true})
     */
    public function homeAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $quizesR = $this->getDoctrine()
            ->getRepository(Quiz::class)->findBy(['published' => true],['created' => 'DESC'],5);

        $quizes = array();
        foreach ($quizesR as $quiz){
            if($quiz->isVisible($user)) $quizes[] = $quiz;
        }

        $eventos = $this->getDoctrine()->getManager()->getRepository(Evento::class)->createQueryBuilder('a')
            ->andWhere('a.dataFim >= :today')
            ->andWhere('a.published = true')
            ->setParameter('today', new \DateTime())
            ->orderBy('a.id','DESC')
            ->getQuery()->getResult();
        ;

        $exclude = array();

        $casodasemana = $em->getRepository(Content::class)->getRecentCases(1);
        $this->check($exclude,$casodasemana);

        $recentContents = $em->getRepository(Content::class)->getRecentContent(5);
        $this->check($exclude,$recentContents);

        $neocasos = $em->getRepository(Content::class)->getNeoNatologia();
        $this->check($exclude,$neocasos);

        $topContents = $this->getDoctrine()->getRepository(Counter::class)->getTopContent(5);
        $this->check($exclude,$topContents);

        $casos = $em->getRepository(Content::class)->getRecentContent(5,Content::TYPE_DISCUSSAOCASO);
        $this->check($exclude,$casos);


        $topTags['total'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(1,null,1);
        $topTags['year'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(1,null,2, $topTags['total']);
        $topTags['month'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(1,null,3,array_merge($topTags['total'],$topTags['year']));
        $topTags['week'] = $this->getDoctrine()->getRepository(Counter::class)
            ->getTopTag(1,null,4,
                array_merge($topTags['total'],$topTags['year'],$topTags['month']));

        $courses = $this->getDoctrine()->getRepository(Course::class)->findBy([
            'published' => true, 'visibility' => Course::VIS_PUBLICO]);

        return $this->render('AppBundle:Home:home.html.twig',[
            'quizes' => $quizes,
            'page_id' => 'home',
            'neocasos' => $neocasos,
            'recentContents' => $recentContents,
            'courses' => $courses,
            'casos' => $casos,
            'casodasemana' => $casodasemana,
            'topTags' => $topTags,
            'topContents' => $topContents,
            'eventos' => $eventos,
        ]);
    }

    public function checkTag($topTags){

    }

    public function check(&$exclude, ArrayCollection &$array){
        /** @var Content $item */
        foreach ($array as $item){
            if(isset($exclude[$item->getId()])){
                $array->removeElement($item);
            }

            $exclude[$item->getId()] = $item->getId();
        }

    }

    /**
     * @Route("busca", name="busca", options={"sitemap" = true})
     */
    public function searchAction(Request $request)
    {
        $search = $request->get('q');


        if(substr($search,-4,1) == '.' or substr($search,-3,1) == '.'){
            return new JsonResponse(null);
        }

        return $this->render('AppBundle:Other:busca.html.twig',[
            'value' => $search,
            'page_title' => $search ? 'Pesquisa por ' . $search : 'Pesquisa',
            'back' =>$this->generateUrl('home'),
        ]);
    }
}
