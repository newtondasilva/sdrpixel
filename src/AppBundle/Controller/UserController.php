<?php
namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Doctrine\DBAL\Types\TextType;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use ReCaptcha\ReCaptcha;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Validator\Constraints\DateTime;

class UserController extends AbstractController
{
    public $encoder;

    public function __construct(UserPasswordEncoderInterface  $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @Route("login", name="login", methods={"GET", "POST"})
     */
    public function loginAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class,[
                'label' => 'E-mail',
            ])
            ->add('password',PasswordType::class,[
                'label' => 'Senha',
            ])
            ->add('masqueradeUser', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                'required' => false,
            ])
            ->add('captcha',EWZRecaptchaType::class)
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            return $this->redirectToRoute('user_profile');
        }

        return $this->render('AppBundle:User:login.html.twig',[
            'form' => $form->createView(),
            'env' => $this->getParameter('env'),
        ]);
    }

    /**
     * @Route("user/profile", name="user_profile")
     */
    public function profileAction(Request $request)
    {
        return $this->redirectToRoute('user_show',['id' => $this->getUser()->getId()]);
    }

    /**
     * @Route("resetpass", name="resetpass", methods={"GET", "POST"})
     */
    public function resetAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class,[
                'label' => 'E-mail',
            ])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            if($user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email'=> $form->getNormData()['email']])){
                if($user->getGoogleId()){
                    $this->addFlash('info','Recupere a senha da sua conta google');
                    return $this->redirectToRoute('resetpass');
                }
                $message = (new \Swift_Message('[DrPixel] Contato'))
                    ->setFrom($this->getParameter('from'))
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView('Emails/resetpass.html.twig',[
                            'token' => $this->generateUrl('user_new',['token' => $user->getPassword()])
                        ]),'text/html'
                    );

                $this->get('mailer')->send($message);

                $this->addFlash('success','Um e-mail foi enviado com link para recuperação da senha foi enviado');
            } else {
                $this->addFlash('info','E-mail não encontrado');
            }

        }

        return $this->render('AppBundle:User:resetpass.html.twig',[
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/user/new", name="user_new")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class,[
                'label' => 'E-mail',
            ])
            ->add('password',RepeatedType::class,[
                'type' => PasswordType::class,
                'invalid_message' => 'Senha não confere.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Senha'],
                'second_options' => ['label' => 'Confirmação da senha'],
            ])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** User @var $user */
            if($user = $em->getRepository(User::class)
                ->findOneBy(['email' => $form->get('email')->getViewData()])){
                $this->addFlash('info','Conta já cadastrada');
                return $this->redirectToRoute('login');
            }

            $password = $form->get('password')->getViewData()['first'];

            $user = new User();
            $user->setEmail($form->get('email')->getViewData());
            $user->setCreated(new \DateTime());
            $user->setGoogleId($form->get('email')->getViewData());
            $user->setLastAccess(new \DateTime());
            $user->setName($form->get('email')->getViewData());
            $user->setFirstName($user->getName());

            $user->setPassword($password);
            $em->persist($user);
            $em->flush();

            $this->addFlash('success','Conta criada com sucesso, realize o login');
            if($goto = $request->get('redirect')){
                return new RedirectResponse($goto);
            } else {
                return $this->redirectToRoute('login',[]);
            }
        }
        return $this->render('AppBundle:User:new.html.twig',[
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/user/{id}/edit", name="user_edit")
     * @Security("has_role('ROLE_USER')")
     */
    public function editAction(Request $request, User $user)
    {
        if(!$user->getFormacao()){
            $user->setNewsletter(true);
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($user->getTipoInstituicao() == User::ALUNOUNICAMP and !$user->getRa() ){
                $this->addFlash('error', 'Insira seu RA');
            } else {

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', 'Alterado com sucesso');
                return $this->redirectToRoute('user_show',['id' => $user->getId()]);
            }


        }

        return $this->render('AppBundle:User:edit.html.twig', array(
            'page_title' => 'Editar perfil ' . $user->getName(),
            'back' => $this->generateUrl('user_profile'),
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request)
    {
        return $this->redirectToRoute('home');
    }

    /**
     * Lists all user entities.
     *
     * @Route("/admin/user", name="user_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->findAll();

        return $this->render('@App/User/index.html.twig', array(
            'page_title' => 'Usuários',
            'actions' => [
                0 => [
                    'label' => 'Incluir usuário',
                    'url' => $this->generateUrl('user_new'),
                ],
            ],
            'back' => $this->generateUrl('admin'),
            'users' => $users,
        ));
    }


    /**
     * Finds and displays a user entity.
     *
     * @Route("admin/user/{id}", name="user_show")
     */
    public function showAction(Request $request, User $user)
    {
        if(!$back = $request->get('back')){
            $back = $this->generateUrl('home');
        }

        return $this->render('AppBundle:User:profile.html.twig', array(
            'page_title' => $user->getName(),
            'actions' => [
                'edit' => [
                    'path' => $this->generateUrl('user_edit',['id' => $user->getId()]),
                    'label' => 'Editar perfil'
                ],
            ],
            'user' => $user,
            'back' => $back,
        ));
    }
}
