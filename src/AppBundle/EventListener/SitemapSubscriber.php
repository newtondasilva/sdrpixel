<?php

namespace AppBundle\EventListener;


use ContentBundle\Entity\Content;
use ContentBundle\Entity\Redirect;
use ContentBundle\Entity\Tag;
use ContentBundle\Entity\TagType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;

class SitemapSubscriber implements EventSubscriberInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    private $em;

    /**
     * @param UrlGeneratorInterface $urlGenerator
     * @param $doctrine
     */
    public function __construct(UrlGeneratorInterface $urlGenerator,
                                EntityManagerInterface $doctrine)
    {
        $this->urlGenerator = $urlGenerator;
        $this->em = $doctrine;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            SitemapPopulateEvent::ON_SITEMAP_POPULATE => 'populate',
        ];
    }

    /**
     * @param SitemapPopulateEvent $event
     */
    public function populate(SitemapPopulateEvent $event)
    {
        $this->registerUrls($event->getUrlContainer());
    }

    /**
     * @param UrlContainerInterface $urls
     */
    public function registerUrls(UrlContainerInterface $urls)
    {
        $contents = $this->em->getRepository(Content::class)->findAll();

        /** @var Content $post */
        foreach ($contents as $post) {
            $urls->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'content_path',
                        ['path' => $post->getPath()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ,null,UrlConcrete::CHANGEFREQ_MONTHLY),
                'content'
            );
        }

        $this->registerRedirects($urls);
        $this->registerEspecialidades($urls);
        $this->registerTag($urls);
    }

    public function registerRedirects($urls){
        $redirects  = $this->em->getRepository(Redirect::class)->findAll();

        /** @var Redirect $item */
        foreach ($redirects as $item) {
            $args = $item->splitTarget();
            $urls->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'match2args',[
                            'arg1' => $args[1], 'arg2' => $args[2]
                    ],UrlGeneratorInterface::ABSOLUTE_URL
                    )
                    ,null,UrlConcrete::CHANGEFREQ_WEEKLY),
                'redirects'
            );
        }
    }

    public function registerEspecialidades($urls){
        $contents = $this->em->getRepository(TagType::class)->findOneBy(['name' => 'Especialidades']);

        /** @var Tag $item */
        foreach ($contents->getTags() as $item) {
            $urls->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'especialidade_show',
                        ['path' => $item->getPath()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                    ,null,UrlConcrete::CHANGEFREQ_YEARLY),
                'especialidade'
            );
        }
    }

    public function registerTag($urls){
        $contents = $this->em->getRepository(Tag::class)->findAll();

        /** @var Tag $item */
        foreach ($contents as $item) {
            $urls->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'tag_path',
                        ['path' => $item->getPath()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                    ,null,UrlConcrete::CHANGEFREQ_YEARLY),
                'tag'
            );
        }
    }
}