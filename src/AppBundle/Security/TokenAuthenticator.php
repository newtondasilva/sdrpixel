<?php

namespace AppBundle\Security;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use IA\ContaBundle\Service\UsuarioTasker;
use IA\UserBundle\Entity\User;
use LdapTools\LdapManager;
use SensioLabs\Security\SecurityChecker;
use Stevenmaguire\OAuth2\Client\Provider\Keycloak;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Provider\UserAuthenticationProvider;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;


class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /** @var EntityManager */
    private $em;

    protected $user;

    /** @var  LdapManager */
    protected $ldalManager;

    protected $router;

    /** @var TokenStorage */
    protected $tokenStorage;

    protected $token;

    protected $encoder;

    protected $storage;

    public function __construct(EntityManagerInterface $entityManager,
                                RouterInterface $router,
                                \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $token,
                                UserPasswordEncoderInterface $encoder)
    {
        $this->em = $entityManager;
        $this->router = $router;
        $this->token = $token;
        $this->encoder = $encoder;
    }

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     */
    public function getCredentials(Request $request)
    {

        if($form = $request->get('form') and isset($form['password'])){
            return [
                'masquerade' => isset($form['masqueradeUser']) ? $form['masqueradeUser'] : null,
                'username' => $form['email'],
                'password' => $form['password']
            ];
        }
        return false;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = null;

        if($credentials['masquerade']){
            $credentials['username'] = $credentials['masquerade'];
        }

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $credentials['username']]);

        return $user;
    }


    /**
     * Verifica a senha somente para autenticacao.
     * Masquerade e usernameSISE (keycloak) passam direto
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {

        // Masquerade nao verifica senha
        if($credentials['masquerade']) return true;

        if($user->checkPassword($credentials['password'])) return true;

        if($this->encoder->isPasswordValid($user, $credentials['password'])) return true;

        return false;

    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {

        if($redirect = $request->get('redirectTo')){
            return new RedirectResponse($request->getBaseUrl(). $redirect);
        } else {
            return new RedirectResponse($this->router->generate('user_profile'));
        }
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        $request->getSession()->getFlashBag()->add('error', 'Verifique seu usuário e senha e tente novamente');

        $url = $this->router->generate('login');

        return new RedirectResponse($url);
    }



    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {


        if($request->getPathInfo() == "/"){
            $url = $this->router->generate('login');
        } else {
            $url = $this->router->generate('login',['redirectTo' => $request->getPathInfo()]);
        }


        return new RedirectResponse($url);
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supportsRememberMe()
    {
        return false;
    }

    public function supports(Request $request)
    {
        return ('login' === $request->attributes->get('_route') && $request->isMethod('POST'));
    }

}
