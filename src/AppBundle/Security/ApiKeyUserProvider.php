<?php

namespace AppBundle\Security;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Acme\DemoBundle\Entity\User;
use Acme\DemoBundle\Provider\OAuthUser;

class ApiKeyUserProvider extends OAuthUserProvider
{
    /** @var EntityManager */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @inheritdoc
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response){
        $user = $this->em->getRepository(User::class)->findOneBy(['googleId' => $response->getUsername()]);

        if(!$user){
            $user = new \AppBundle\Entity\User();
            $user->setGoogleId($response->getUsername());
            $user->setFirstName($response->getFirstName());
            $user->setLastName($response->getLastName());
            $user->setEmail($response->getEmail());
            $user->setName($response->getRealName());
            $user->setCreated(new \DateTime());
        }

        $user->setPicture($response->getProfilePicture());
        $user->setLastAccess(new \DateTime());

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

}
