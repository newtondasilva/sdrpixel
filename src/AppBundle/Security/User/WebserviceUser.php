<?php
namespace AppBundle\Security\User;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class WebserviceUser implements OAuthAwareUserProviderInterface
{

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @inheritdoc
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response){
        $user = $this->em->getRepository(User::class)->findOneBy(['googleId' => $response->getUsername()]);
        if(!$user){
            $user = new User();
            $user->setGoogleId($response->getUsername());
            $user->setFirstName($response->getFirstName());
            $user->setLastName($response->getLastName());
            $user->setEmail($response->getEmail());
            $user->setName($response->getRealName());
            $user->setCreated(new \DateTime());
        }

        $user->setPicture($response->getProfilePicture());
        $user->setLastAccess(new \DateTime());

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }


}
