<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use ContentBundle\Entity\CourseContent;
use ContentBundle\Entity\CourseProgress;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

class CourseContentController extends AbstractFOSRestController
{

    /**
     * @Rest\Get("courseContent/{courseContent}/user/{user}/courseProgress")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, CourseContent $courseContent,User $user){
        $em = $this->getDoctrine()->getManager();

        $courseUser = $this->getDoctrine()->getRepository('ContentBundle:CourseUser')
            ->findOneBy(['user' => $user,'course' => $courseContent->getCourseSection()->getCourse()->getId()]);

        $courseProgress = $this->getDoctrine()->getRepository('ContentBundle:CourseProgress')
            ->findOneBy(['courseUser' => $courseUser->getId(),'courseContent' => $courseContent->getId()]);

        if(!$courseProgress){
            $courseProgress = new CourseProgress();
            $courseProgress->setCourseContent($courseContent);
            $courseProgress->setCreated(new \DateTime());
            $courseProgress->setCourseUser($courseUser);
            $courseProgress->setStatus(CourseProgress::STATUS_INICIADO);
            $em->persist($courseProgress);
            $em->flush();
        }

        // define conteudo como lido
        if($status = $request->get('status')){
           $courseProgress->setStatus($status);
            $em->persist($courseProgress);
            $em->flush();
        }
        $view = $this->view($courseProgress,200);
        $view->getContext()->addGroups(['courseUser','courseProgress','courseContent']);

        return $this->handleView($view);
    }
}
