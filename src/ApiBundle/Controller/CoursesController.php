<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use ContentBundle\Entity\Course;
use ContentBundle\Entity\CourseProgress;
use ContentBundle\Entity\CourseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CoursesController extends AbstractFOSRestController
{
    protected function context(Request $request, View &$view){
        $context = new Context();
        if(!$group = $request->get('group')){
            $group = 'list';
        }

        $context->addGroup($group);
        $view->setContext($context);

    }

    // "get_users"            [GET] /courses
    public function getCoursesAction(Request $request){
        $courses = [];

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if($userId = $request->get('userId')){

            $courseUsers = $em->getRepository('ContentBundle:CourseUser')
                ->createQueryBuilder('course_user')
                ->andWhere('course_user.user = :user')->setParameter('user', $userId)
                ->leftJoin('course_user.course','course')
                ->andWhere('course.visibility = :visibility')
                ->setParameter('visibility', Course::VIS_EDRPIXEL)
                ->getQuery()->getResult();

            /** @var CourseUser $courseUser */
            foreach ($courseUsers as $courseUser){
                $courses[$courseUser->getCourse()->getId()] = $courseUser->getCourse();
            }
        }
        $allCourses = $this->getDoctrine()->getRepository(Course::class)
            ->findBy(['visibility' => Course::VIS_EDRPIXEL, 'published' => true]);

        /** @var Course $course */
        foreach ($allCourses as $course){
            $courses[$course->getId()] = $course;
        }

        $coursesF = [];
        foreach ($courses as $course){
            $coursesF[] = $course;
        }

        $view = new View($coursesF);
        $this->context($request,$view);

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCourseAction(Request $request, $id){
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);

        $view = $this->view($course,200);
        $this->context($request,$view);


        return $this->handleView($view);



    }
}
