<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use ContentBundle\Entity\CourseContent;
use ContentBundle\Entity\CourseProgress;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use QuizBundle\Entity\Answer;
use QuizBundle\Entity\AnswerItem;
use QuizBundle\Entity\QuestionItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

class CourseProgressController extends AbstractFOSRestController
{
    protected function context(Request $request, View &$view){
        $context = new Context();
        if(!$group = $request->get('group')){
            $group = 'list';
        }

        $context->addGroup($group);
        $view->setContext($context);

    }

    /**
     * @Rest\Get("courseProgress/{courseProgress}/answer")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAnswerItemAction(Request $request, CourseProgress $courseProgress){
        $em = $this->getDoctrine()->getManager();

        // limpa respostas anteriores
        foreach ($courseProgress->getAnswers() as $answer){
            $em->remove($answer);
            $em->flush($answer);
        }

        $courseProgress->getAnswers()->clear();

        $answer = new Answer();
        $answer->setQuiz($courseProgress->getCourseContent()->getQuiz());
        $answer->setCreated(new \DateTime());
        $answer->setUser($courseProgress->getCourseUser()->getUser());
        $answer->setCourseProgress($courseProgress);
        $em->persist($answer);
        $em->flush();


        $questionItemsIds = json_decode($request->get('answerItems'));

        foreach ($questionItemsIds as $item){
            $questionItem = $em->getRepository('QuizBundle:QuestionItem')->find($item);
            // Verifica se possui answerItem
            if(!$answerItem = $em->getRepository('QuizBundle:AnswerItem')->findOneBy([
                'answer' => $answer->getId(),'questionItem' => $questionItem->getId()])) {
                $answerItem = new AnswerItem();
                $answerItem->setAnswer($answer);
                $answerItem->setTime(0);
            }

            $answerItem->setQuestionItem($questionItem);
            $answerItem->setCreated(new \DateTime());
            $answerItem->setType(AnswerItem::T_NORMAL);
            $answer->addItem($answerItem);
            $em->persist($answerItem);
        }

        $em->flush();
        $courseProgress->addAnswer($answer);
        $view = $this->view($courseProgress,200);
        $this->context($request,$view);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get("courseProgress/{courseProgress}/resetAnswer")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetAnswerAction(Request $request, CourseProgress $courseProgress){
        $em = $this->getDoctrine()->getManager();

        foreach ($courseProgress->getAnswers() as &$answer){
            $em->remove($answer);
            $em->flush();
        }

        $view = $this->view($courseProgress,200);
        $this->context($request,$view);

        return $this->handleView($view);
    }
}
