<?php

namespace ApiBundle\Controller;

use ContentBundle\Entity\Course;
use ContentBundle\Entity\CourseUser;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CourseUserController extends AbstractFOSRestController
{
    protected function context(Request $request, View &$view){
        $context = new Context();
        if(!$group = $request->get('group')){
            $group = 'courseUser';
        }

        $context->addGroup($group);
        $view->setContext($context);

    }

    /**
     * @Rest\Get("/courseUser")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexCourseUser(Request $request){

        $courseUser = $this->getDoctrine()->getRepository('ContentBundle:CourseUser')
            ->findOneBy(['user' => $request->get('userId'), 'course' => $request->get('courseId')]);

        if(!$courseUser){
            $courseUser = new CourseUser();
            $course = $this->getDoctrine()->getRepository(Course::class)->find($request->get('courseId'));
            $user = $this->getDoctrine()->getRepository(User::class)->find($request->get('userId'));
            $courseUser->setCourse($course);
            $courseUser->setUser($user);
            $courseUser->setCreated(new \DateTime());
            $this->getDoctrine()->getManager()->persist($courseUser);
            $this->getDoctrine()->getManager()->flush();
        }

        $view = new View($courseUser);
        $this->context($request,$view);

        return $this->handleView($view);
    }



}
