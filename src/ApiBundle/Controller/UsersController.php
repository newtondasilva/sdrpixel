<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class UsersController extends AbstractFOSRestController
{
    protected function context(Request $request, View &$view){
        $context = new Context();
        if(!$group = $request->get('group')){
            $group = 'list';
        }

        $context->addGroup($group);
        $view->setContext($context);

    }

    private $encoder;

    /**
     * @Rest\Get("users")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUsersAction(Request $request){
        $this->encoder = $this->get('security.password_encoder');
        $user = null;

        if($googleId = $request->get('googleId')){
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['googleId' => $googleId]);

            // caso nao encontrado cria
            if(!$user){
                $user = new User();
                $user->setName($request->get('name'));
                $user->setFirstName($request->get('firstName'));
                $user->setLastName($request->get('lastName'));
                $user->setGoogleId($request->get('googleId'));
                $user->setPicture($request->get('picture'));
                $user->setCreated(new \DateTime());
                $user->setEmail($request->get('email'));
                $user->setNewsletter(0);
                $user->setLastAccess(new \DateTime());
                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();
            }
        } else {
            // autenticacao por senha
            if($email = $request->get('email')){
                $user = $this->getDoctrine()->getRepository(User::class)
                    ->findOneBy(['email' => $email]);

                if($user){
                    $valid = $this->encoder->isPasswordValid($user, $request->get('password'));
                    if(!$valid){
//                        $user = null;
                    }
                }
            }
        }



        $view = new View($user);
        $this->context($request,$view);

        return $this->handleView($view);
    }

    public function getUserAction(Request $request, $id){
        $course = $this->getDoctrine()->getRepository(Course::class)->find($id);

        $view = $this->view($course,200);
        $this->context($request,$view);

        return $this->handleView($view);
    }
}

